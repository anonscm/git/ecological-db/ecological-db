-- Fichier contenant un extrait de crebas (4 tables de fin de script) et sur lequel on peut exécuter un programme permettant la duplication / personnalisation

/*====================================================================================*/
/* Table : t_mesure_mobj                                           					  */
/*		Cette table devra être renommée voir dupliquée pour prendre en compte autant  */
/*		d'objets métiers à mesurer que nécessaire. Par exemple une table mesure_arb   */
/*		pour contenir les mesure concernant des arbres.								  */
/*		Les tables satellites devront elles aussi être renommées voir dupliquées pour */
/*		répondre au besoin.(il s'agit notamment des 3 tables suivantes :			  */
/*			celle qui associe la mesure à un opérateur et un rôle.					  */
/*			celle qui associe éventuellement la mesure à 1 ou plusieurs reroupements  */
/*			celle qui associe la mesure à une autre mesure considérée comme métadonnée*/
/*====================================================================================*/
create table t_mesure_mobj (
   mobj_id               serial               not null,
   mobj_obj_id			 bigint				  not null, 
   mobj_typm_id          integer              null,
   mobj_var_id           integer              not null,
   mobj_fic_id           integer              null,
   mobj_prot_id          integer              null,
   mobj_norm_id          integer              null,
   mobj_uni_id           integer              null,
   mobj_valeur_num       float8               null,
   mobj_valeur_text      varchar(1)           null,
   mobj_valeur_date      date                 null,
   mobj_date_mesure      date                 null,
   mobj_precision_date_mesure float8          null,
   mobj_precision_mesure 	 float8          null,
   mobj_remarque         varchar              null,
   constraint t_mesure_mobj_pkey primary key (mobj_id),
   constraint c_uni_var_datemesure_mobj unique(mobj_var_id,mobj_date_mesure), -- Cette clé 'humaine' pourra être revisitée lors de la spécialisation pour les mesures de différents objets
   constraint c_fk_obj_mobj foreign key (mobj_obj_id)
		references public.t_objet_obj(obj_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_typm_mobj foreign key (mobj_typm_id)
		references public.tr_type_mesure_typm(typm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_var_mobj foreign key (mobj_var_id)
    	references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_fic_mobj foreign key (mobj_fic_id)
		references public.tr_fichier_fic(fic_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_prot_mobj foreign key (mobj_prot_id)
		references public.tr_protocole_prot(prot_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_norm_mobj foreign key (mobj_norm_id)
		references public.tr_norme_norm(norm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_uni_mobj foreign key (mobj_uni_id)
		references public.tr_unite_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table t_mesure_mobj is 'Table des mesures de variables pour un type d''objet spécifique';
comment on column t_mesure_mobj.mobj_id is 'Identifiant automatique numérique de la mesure';
comment on column t_mesure_mobj.mobj_obj_id is 'Identifiant automatique numérique de l''objet associé';
comment on column t_mesure_mobj.mobj_typm_id is 'Identifiant automatique numérique du type de mesure associé';
comment on column t_mesure_mobj.mobj_var_id is 'Identifiant unique numérique de la variable associée';
comment on column t_mesure_mobj.mobj_fic_id is 'Identifiant automatique numérique du fichier associé';
comment on column t_mesure_mobj.mobj_prot_id is 'Identifiant unique numérique du protocole associé';
comment on column t_mesure_mobj.mobj_norm_id is 'Identifiant numérique automatique de la norme associée';
comment on column t_mesure_mobj.mobj_uni_id is 'Identifiant unique numérique de l''unité de mesure associée';
comment on column t_mesure_mobj.mobj_valeur_num is 'valeur de la mesure si celle-ci est de type numérique';
comment on column t_mesure_mobj.mobj_valeur_text is 'valeur de la mesure si celle-ci est de type texte';
comment on column t_mesure_mobj.mobj_valeur_date is 'valeur de la mesure si celle-ci est de type date';
comment on column t_mesure_mobj.mobj_date_mesure is 'date de la mesure';
comment on column t_mesure_mobj.mobj_precision_date_mesure is 'Précision concernant la date de la mesure';
comment on column t_mesure_mobj.mobj_precision_mesure is 'Précision de la valeur de la mesure';
comment on column t_mesure_mobj.mobj_remarque is 'Information complémentaire associée à la mesure';
comment on constraint c_uni_var_datemesure_mobj on public.t_mesure_mobj is 'Le couple identifiant de la variable / date de la mesure est unique';

comment on constraint c_fk_obj_mobj on public.t_mesure_mobj is 'une mesure doit être rattachée à un et un seul objet';
comment on constraint c_fk_typm_mobj on public.t_mesure_mobj is 'une mesure peut être rattachée à O ou 1 type de mesure';
comment on constraint c_fk_var_mobj on public.t_mesure_mobj is 'une mesure doit être rattachée à une et une seule variable';
comment on constraint c_fk_fic_mobj on public.t_mesure_mobj is 'une mesure peut être rattachée à O ou 1 fichier';
comment on constraint c_fk_prot_mobj on public.t_mesure_mobj is 'une mesure peut être rattachée à O ou 1 protocole';
comment on constraint c_fk_norm_mobj on public.t_mesure_mobj is 'une mesure peut être rattachée à O ou 1 norme';
comment on constraint c_fk_uni_mobj on public.t_mesure_mobj is 'une mesure peut être rattachée à O ou 1 unité';

-- Index pour les clés etrangères
create index x_btr_fkey_obj_mobj on public.t_mesure_mobj
  using btree (mobj_obj_id);
comment on index public.x_btr_fkey_obj_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_typm_mobj on public.t_mesure_mobj
  using btree (mobj_typm_id);
comment on index public.x_btr_fkey_typm_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_var_mobj on public.t_mesure_mobj
  using btree (mobj_var_id);
comment on index public.x_btr_fkey_var_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_fic_mobj on public.t_mesure_mobj
  using btree (mobj_fic_id);
comment on index public.x_btr_fkey_fic_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_prot_mobj on public.t_mesure_mobj
  using btree (mobj_prot_id);
comment on index public.x_btr_fkey_prot_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_norm_mobj on public.t_mesure_mobj
  using btree (mobj_norm_id);
comment on index public.x_btr_fkey_norm_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_uni_mobj on public.t_mesure_mobj
  using btree (mobj_uni_id);
comment on index public.x_btr_fkey_uni_mobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : tj_role_ope_mobj_rom                                    */
/*			Table associant une mesure d'objet à un opérateur   */
/*			et un rôle										    */
/*==============================================================*/
create table tj_role_ope_mobj_rom (
   rom_mobj_id           integer              not null,
   rom_ope_id           integer              not null,
   rom_rol_id			integer				 not null,
   constraint tj_role_ope_mobj_rom_pkey primary key (rom_mobj_id, rom_ope_id,rom_rol_id),
   constraint c_fk_mobj_rom foreign key (rom_mobj_id)
		references public.t_mesure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_rom foreign key (rom_ope_id)
		references public.tr_operateur_ope(ope_id)
		on delete no action
		on update no action
		not deferrable, 		
   constraint c_fk_rol_rom foreign key (rom_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_role_ope_mobj_rom is 'Table de jointure associant un regroupement à un opérateur et à un rôle pour cet opérateur';
comment on column tj_role_ope_mobj_rom.rom_mobj_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_role_ope_mobj_rom.rom_ope_id is 'Identifiant automatique numérique de l''opérateur associé';
comment on column tj_role_ope_mobj_rom.rom_rol_id is 'Identifiant automatique numérique du rôle associé';
comment on constraint c_fk_mobj_rom on public.tj_role_ope_mobj_rom is 'Le champ rom_mobj_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_ope_rom on public.tj_role_ope_mobj_rom is 'Le champ rom_ope_id correspond obligatoirement à un opérateur existant';
comment on constraint c_fk_rol_rom on public.tj_role_ope_mobj_rom is 'Le champ rom_rol_id correspond obligatoirement à un rôle existant';

-- Index pour les clés etrangères
create index x_btr_mobj_rom on public.tj_role_ope_mobj_rom
  using btree (rom_mobj_id);
comment on index public.x_btr_mobj_rom is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_ope_rom on public.tj_role_ope_mobj_rom
  using btree (rom_ope_id);
comment on index public.x_btr_ope_rom is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rol_rom on public.tj_role_ope_mobj_rom
  using btree (rom_rol_id);
comment on index public.x_btr_rol_rom is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*=================================================================*/
/* Table : tj_rgp_mobj_gmo                                         */
/*			Table associant une mesure d'objet à un regroupement   */
/*=================================================================*/
create table tj_rgp_mobj_gmo (
   gmo_mobj_id          integer              not null,
   gmo_rgp_id           integer              not null,
   constraint tj_rgp_mobj_gmo_pkey primary key (gmo_mobj_id, gmo_rgp_id),
   constraint c_fk_mobj_gmo foreign key (gmo_mobj_id)
		references public.t_mesure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rgp_gmo foreign key (gmo_rgp_id)
		references public.t_regroupement_rgp(rgp_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_rgp_mobj_gmo is 'Table de jointure associant une mesure à un regroupement.';
comment on column tj_rgp_mobj_gmo.gmo_mobj_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_rgp_mobj_gmo.gmo_rgp_id is 'Identifiant automatique numérique du regroupement associé';
comment on constraint c_fk_mobj_gmo on public.tj_rgp_mobj_gmo is 'Le champ gmo_mobj_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_rgp_gmo on public.tj_rgp_mobj_gmo is 'Le champ gmo_rgp_id correspond obligatoirement à un regroupememnt existant';

-- Index pour les clés etrangères
create index x_btr_mobj_gmo on public.tj_rgp_mobj_gmo
  using btree (gmo_mobj_id);
comment on index public.x_btr_mobj_gmo is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rgp_gmo on public.tj_rgp_mobj_gmo
  using btree (gmo_rgp_id);
comment on index public.x_btr_rgp_gmo is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*=================================================================*/
/* Table : tj_mobj_mobj_tobj                                       */
/*			Table associant une mesure d'objet à une autre mesure  */
/*			d'objet, la seconde étant complémentaire (métadonnée)  */
/*			ou nécessaire à l'interprétation de la première		   */
/*=================================================================*/
create table tj_mobj_mobj_tobj (
   tobj_ref_mobj_id          integer              not null,
   tobj_meta_mobj_id         integer              not null,
   tobj_statut				 varchar(32)		  null,
   constraint tj_mobj_mobj_tobj_pkey primary key (tobj_ref_mobj_id, tobj_meta_mobj_id),
   constraint c_fk_ref_mobj_tobj foreign key (tobj_ref_mobj_id)
		references public.t_mesure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_meta_mobj_tobj foreign key (tobj_meta_mobj_id)
		references public.t_mesure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_mobj_mobj_tobj is 'Table de jointure associant une mesure à une autre mesure, la seconde étant complémentaire (métadonnée) ou nécessaire à l''interprétation de la première.';
comment on column tj_mobj_mobj_tobj.tobj_ref_mobj_id is 'Identifiant automatique numérique de la mesure référence';
comment on column tj_mobj_mobj_tobj.tobj_meta_mobj_id is 'Identifiant automatique numérique de la mesure considérée comme métadonnée de la mesure référence';
comment on column tj_mobj_mobj_tobj.tobj_statut is 'Indicateur précisant le statut de la métadonnée par rapport à la mesure de référence. Par exemple le caractère obligatoire ou recommandé pour l''interprétation.';

comment on constraint c_fk_ref_mobj_tobj on public.tj_mobj_mobj_tobj is 'Le champ tobj_ref_mobj_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_meta_mobj_tobj on public.tj_mobj_mobj_tobj is 'Le champ tobj_meta_mobj_id correspond obligatoirement à une mesure existante';

-- Index pour les clés etrangères
create index x_btr_mobj_tobj on public.tj_mobj_mobj_tobj
  using btree (tobj_ref_mobj_id);
comment on index public.x_btr_mobj_tobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rgp_tobj on public.tj_mobj_mobj_tobj
  using btree (tobj_meta_mobj_id);
comment on index public.x_btr_rgp_tobj is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
