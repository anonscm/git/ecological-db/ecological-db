---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- Date création :	19/07/2022
-- dernière modificcation	:	19/07/2022
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour créer la structure (Tables, contraintes, index ...). 
--					
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer sur une base précedemment créée à l'aide du script suivant :
--				https://git.renater.fr/anonscm/gitweb?p=script/script.git;a=blob_plain;f=serveurs+virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des index et contraintes ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f crebas_schema_application_fr.sql -d nombase
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 			
-- MODIFICATION :
\set proprietaire 'ecological_db'
\set grpconnect 'ecological_db_connect'
\set grpecriture 'ecological_db_ecriture'
\set grplecture 'ecological_db_lecture'
\set grpapplication 'ecological_db_application'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

START TRANSACTION;

create schema application;
alter schema application OWNER to :proprietaire;
grant usage on schema application to :grpapplication;
grant usage on schema application to :grplecture; -- pour les formulaires access

/*==============================================================*/
/* Table : application.configuration formulaires access         */
/*==============================================================*/
-- Cette table n'est utile que si une application basée sur les formulaires Access standardisés du Plateau SIG-BD de l'unité SILVA sera développée.

create table application.t_formulaire_access_configuration_faconf (
	faconf_id               serial			not null,
	faconf_sgbd             varchar(100)	not null,
	faconf_version			varchar(4)		null,
	faconf_odbc				boolean         not null,
	faconf_deconnecte		boolean         not null,
	faconf_num_instance		integer			null,
	faconf_database			varchar(100)	not null,
	faconf_role_ecriture	varchar(100)	null,
	faconf_tag				varchar(255)	null,
	constraint t_formulaire_access_configuration_faconf_pkey primary key (faconf_id)
);

comment on table application.t_formulaire_access_configuration_faconf is
'Table contenant une partie de la configuration de l''application access.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_id is
'Identifiant de la configuration.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_sgbd is
'Nom du sgbd utilisé.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_version is
'Version du sgbd utilisé.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_odbc is
'Booléan indiquant si l''application est connectée à la base en mode connecté (tables liées).';
comment on column application.t_formulaire_access_configuration_faconf.faconf_deconnecte is
'Booléan indiquant si l''application est connectée à la base en mode déconnecté.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_num_instance is
'Chiffre indiquant le numéro de l''instance, information utilisée en mode déconnecté pour la saisie à plusieurs.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_database is
'Nom de la base de donnée utilsée.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_role_ecriture is
'Nom du rôle ayant les droits en écriture.';
comment on column application.t_formulaire_access_configuration_faconf.faconf_tag is
'Tag permettant d''effectuer un traitement particulier selon la valeur de ce dernier.';

alter table application.t_formulaire_access_configuration_faconf owner to :proprietaire;

grant select on application.t_formulaire_access_configuration_faconf to :grplecture;
grant select on application.t_formulaire_access_configuration_faconf to :grpecriture;
grant select on application.t_formulaire_access_configuration_faconf to :grpapplication;

-- ******************************************************************************************
-- ***** Gestion des droits par défauts lors de la création de nouveaux objets postgres *****
-- ******************************************************************************************

alter default privileges
	in schema application
	grant all
	on sequences
	TO :proprietaire, :grpapplication;

-- *********************************************************************
-- ***** Création des tables permettant la gestion des utilisateurs ****
-- *********************************************************************
-- Ces tables sont utiles uniquement pour une application basée sur les formulaires PHP standardisés du Plateau SIG-BD de l'unité SILVA.

/*==============================================================*/
/* Table : application.t_organisation_org                       */
/*==============================================================*/
create table application.t_organisation_org (
	org_id			serial			not null,
	org_ldap_uid	varchar(100)	null,
	org_code		varchar(100)    not null,
	org_libelle		varchar(255)	not null,
	constraint c_pk_t_organisation_org primary key(org_id),
	constraint c_uni_code_org unique (org_code)
);
comment on table application.t_organisation_org is
'Table contenant les organisations (Centre INRAe pour le LDAP INRAe).';
comment on column application.t_organisation_org.org_id is
'Identifiant de l''organisation (postgres).';
comment on column application.t_organisation_org.org_ldap_uid is
'Identifiant LDAP de l''organisation.';
comment on column application.t_organisation_org.org_code is
'Code de l''organisation.';
comment on column application.t_organisation_org.org_libelle is
'Libellé de l''organisation.';
comment on constraint c_uni_code_org on application.t_organisation_org is
'Contrainte d''unicité sur le code de l''organisation.';

alter table application.t_organisation_org owner to :proprietaire;

grant select on application.t_organisation_org to :grpapplication;
grant insert on application.t_organisation_org to :grpapplication;
grant update on application.t_organisation_org to :grpapplication;
grant delete on application.t_organisation_org to :grpapplication;

/*==============================================================*/
/* Table : application.t_service_serv							*/
/*==============================================================*/
create table application.t_service_serv (
	serv_id			serial			not null,
	serv_ldap_uid	varchar(100)	null,
	serv_code		varchar(100)	not null,
	serv_libelle	varchar(255)	not null,
	constraint c_pk_t_service_serv primary key(serv_id),
	constraint c_uni_code_serv unique (serv_code)
);
comment on table application.t_service_serv is
'Table contenant les différents services (Unité pour le LDAP INRAe).';
comment on column application.t_service_serv.serv_id is
'Identifiant du service (postgres).';
comment on column application.t_service_serv.serv_ldap_uid is
'Identifiant du service au sein du LDAP.';
comment on column application.t_service_serv.serv_code is
'Code du service.';
comment on column application.t_service_serv.serv_libelle is
'Libellé du service.';
comment on constraint c_uni_code_serv on application.t_service_serv is
'Contrainte d''unicité sur le code du service.';

alter table application.t_service_serv owner to :proprietaire;

grant select on application.t_service_serv to :grpapplication;
grant insert on application.t_service_serv to :grpapplication;
grant update on application.t_service_serv to :grpapplication;
grant delete on application.t_service_serv to :grpapplication;

/*==============================================================*/
/* Table : application.t_utilisateur_usr                        */
/*==============================================================*/

create table application.t_utilisateur_usr (
	usr_id						serial						not null,
	usr_org_id					int4						null,
	usr_serv_id					int4						null,
	usr_login					varchar(255)				null,
	usr_password				varchar(255)				null,
	usr_nom						varchar(100)				null,
	usr_prenom					varchar(100)				null,
	usr_mail					varchar(255)				not null,
	usr_activation_code			varchar(255)				null,
	usr_code_creation			timestamp with time zone	null,
	usr_code_expiration			timestamp with time zone	null,
	usr_ldap_nom           		varchar(20)					null,
	usr_ldap_uid            	varchar(100)    			null,
	usr_oauth2_provider			varchar(15)					null,
	usr_oauth2_provider_id  	varchar(100)    			null,
	usr_refresh_token			varchar(255)				null,
	usr_blocked					boolean						not null default false,
	usr_creation_compte			date						null,
	usr_derniere_connexion		date						null,
	usr_validite_compte			date						null,
	usr_licence_signature		date						null,
	usr_licence_version			varchar(4)					null,
	usr_totp_secret				varchar(50)					null,
	usr_totp_code_recuperation	varchar(100)				null,
	constraint c_pk_t_utilisateur_usr primary key(usr_id),
	constraint c_uni_login_mail_usr unique (usr_login, usr_mail),
	constraint c_uni_login_ldap_mail_usr unique (usr_login, usr_ldap_nom, usr_mail),
	constraint c_uni_login_provider_mail_usr unique (usr_login, usr_oauth2_provider, usr_mail),
	constraint c_uni_mail_usr unique (usr_mail),
	constraint c_fk_usr_org foreign key (usr_org_id)
		references application.t_organisation_org(org_id)
		on delete no action
		on update no action
		not deferrable,
	constraint c_fk_usr_serv foreign key (usr_serv_id)
		references application.t_service_serv(serv_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table application.t_utilisateur_usr is
'Table contenant les comptes utilisateurs.';
comment on column application.t_utilisateur_usr.usr_id is
'Identifiant de l''utilisateur (postgres).';
comment on column application.t_utilisateur_usr.usr_org_id is
'Rattachement d''une organisation à un utilisateur.';
comment on column application.t_utilisateur_usr.usr_serv_id is
'Rattachement d''un service à un utilisateur.';
comment on column application.t_utilisateur_usr.usr_login is
'Login de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_password is
'Mot de passe de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_nom is
'Nom de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_prenom is
'Prénom de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_mail is
'Email de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_activation_code is
'Code d''activation généré lors de la demande de compte ou lors de la demande de ré-initialisation de mot de passe.';
comment on column application.t_utilisateur_usr.usr_code_creation is
'Date et heure de création du code temporaire permettant la création de compte.';
comment on column application.t_utilisateur_usr.usr_code_expiration is
'Date et heure d''expiration du code temporaire permettant la création de compte.';
comment on column application.t_utilisateur_usr.usr_ldap_nom is
'Nom du LDAP que l''utilisateur utilise pour se connecter.';
comment on column application.t_utilisateur_usr.usr_ldap_uid is
'Identifiant de l''utilisateur dans le LDAP qu''il utilise pour se connecter.';
comment on column application.t_utilisateur_usr.usr_oauth2_provider is
'Fournisseur de jeton utilisé par l''utilisateur pour se connecter.';
comment on column application.t_utilisateur_usr.usr_oauth2_provider_id is
'Identifiant de l''utilisateur dans le fournisseur de jeton utilisé par l''utilisateur pour se connecter.';
comment on column application.t_utilisateur_usr.usr_refresh_token is
'Jeton de rafraichissement.';
comment on column application.t_utilisateur_usr.usr_blocked is
'Boolean indiquant si le compte de l''utilisateur est bloqué ou pas.';
comment on column application.t_utilisateur_usr.usr_creation_compte is
'Date à laquelle le compte de l''utilisateur a été créé.';
comment on column application.t_utilisateur_usr.usr_derniere_connexion is
'Date de la dernière connexion de l''utilisateur.';
comment on column application.t_utilisateur_usr.usr_validite_compte is
'Date à laquelle le compte de l''utilisateur expire.';
comment on column application.t_utilisateur_usr.usr_licence_signature is
'Date à laquelle l''utilisateur a accépté la licence.';
comment on column application.t_utilisateur_usr.usr_licence_version is
'Numéro de version de la licence que l''utilisateur a accépté.';
comment on column application.t_utilisateur_usr.usr_totp_secret is
'Code secret généré automatique lors de l''activation de l''authentification à 2 facteurs.';
comment on column application.t_utilisateur_usr.usr_totp_code_recuperation is
'Code de récupération généré automatiquement lors de l''activation de l''authentification à 2 facteurs.';
comment on constraint c_uni_login_mail_usr on application.t_utilisateur_usr is
'Unicité sur le login et le mail de l''utilisateur unicité necessaire pour les comptes sauvegardés en base de données.';
comment on constraint c_uni_login_ldap_mail_usr on application.t_utilisateur_usr is
'Unicité sur le login, le nom du ldap et le mail de l''utilisateur, unicité necessaire pour identifier un compte de façon unique lors de connexion via un ldap.';
comment on constraint c_uni_login_provider_mail_usr on application.t_utilisateur_usr is
'Unicité sur le login, le nom du fournisseur d''identité et le mail de l''utilisateur, unicité necessaire pour identifier un compte de façon unique lors de connexion via le protocole OAuth2.';
comment on constraint c_uni_mail_usr on application.t_utilisateur_usr is
'Unicité sur l''email de l''utilisateur.';
comment on constraint c_fk_usr_org on application.t_utilisateur_usr is
'Rattachement d''une organisation à un utilisateur.';
comment on constraint c_fk_usr_serv on application.t_utilisateur_usr is
'Rattachement d''un service à un utilisateur.';

-- création de l'Index : x_btr_usr_org_id
create index x_btr_usr_org_id on application.t_utilisateur_usr using btree (usr_org_id);
comment on index application.x_btr_usr_org_id is
'Index sur la rattachement d''une organisation à un utilisateur.';

-- création de l'Index : application.x_btr_usr_serv_id
create index x_btr_usr_serv_id on application.t_utilisateur_usr using btree (usr_serv_id);
comment on index application.x_btr_usr_serv_id is
'Index sur la rattachement d''un service à un utilisateur.';

alter table application.t_utilisateur_usr owner to :proprietaire;

grant select on application.t_utilisateur_usr to :grpapplication;
grant insert on application.t_utilisateur_usr to :grpapplication;
grant update on application.t_utilisateur_usr to :grpapplication;
grant delete on application.t_utilisateur_usr to :grpapplication;

/*==============================================================*/
/* Table : application.tr_role_rol                              */
/*==============================================================*/

create table application.tr_role_rol (
	rol_id			serial			not null,
	rol_libelle		varchar(25)		not null,
	rol_description	varchar(255)	null,
	constraint c_pk_tr_role_rol primary key(rol_id),
	constraint c_uni_libelle_rol unique (rol_libelle)
);
comment on table application.tr_role_rol is
'Table contenant les différents rôles pouvant être affectés aux utilisateurs.';
comment on column application.tr_role_rol.rol_id is
'Identifant (postgres) du rôle.';
comment on column application.tr_role_rol.rol_libelle is
'Libellé du rôle.';
comment on column application.tr_role_rol.rol_description is
'Descritpion du rôle.';
comment on constraint c_uni_libelle_rol on application.tr_role_rol is
'Unicité sur le libellé du rôle.';

alter table application.tr_role_rol owner to :proprietaire;

grant select on application.tr_role_rol to :grpapplication;
grant insert on application.tr_role_rol to :grpapplication;
grant update on application.tr_role_rol to :grpapplication;
grant delete on application.tr_role_rol to :grpapplication;

/*==============================================================*/
/* Table : application.tr_permission_perm                       */
/*==============================================================*/

create table application.tr_permission_perm (
	perm_id				serial			not null,
	perm_libelle		varchar(100)	not null,
	perm_description	varchar(255)	not null,
	constraint c_pk_tr_permission_perm primary key(perm_id),
	constraint c_uni_libelle_perm unique (perm_libelle)
);
comment on table application.tr_permission_perm is
'Table contenant les différentes permissions pouvant être affectées aux rôles.';
comment on column application.tr_permission_perm.perm_id is
'Identifant (postgres) de la permission.';
comment on column application.tr_permission_perm.perm_libelle is
'Libellé unique de la permission.';
comment on column application.tr_permission_perm.perm_description is
'Description des droits donnés par la permission.';
comment on constraint c_uni_libelle_perm on application.tr_permission_perm is
'Unicité sur le libellé de la permission.';

alter table application.tr_permission_perm owner to :proprietaire;

grant select on application.tr_permission_perm to :grpapplication;
grant insert on application.tr_permission_perm to :grpapplication;
grant update on application.tr_permission_perm to :grpapplication;
grant delete on application.tr_permission_perm to :grpapplication;

/*==============================================================*/
/* Table : application.tj_usr_rol_arol                          */
/*         Table associant un compte utilisateur à un rôle      */
/*==============================================================*/

create table application.tj_usr_rol_arol (
	arol_usr_id		int4	not null,
	arol_rol_id		int4	not null,
	constraint c_pk_tj_usr_rol_arol primary key (arol_usr_id, arol_rol_id),
	constraint c_fk_usr_arol foreign key (arol_usr_id)
		references application.t_utilisateur_usr(usr_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_rol_arol foreign key (arol_rol_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_usr_rol_arol is
'Table attribuant un rôle à compte un utilisateur.';
comment on column application.tj_usr_rol_arol.arol_usr_id is
'Identifiant (postgres) d''un utilisateur.';
comment on column application.tj_usr_rol_arol.arol_rol_id is
'Identifiant (postgres) d''un rôle.';
comment on constraint c_fk_usr_arol on application.tj_usr_rol_arol is
'Association d''un compte utilisateur à un rôle.';
comment on constraint c_fk_rol_arol on application.tj_usr_rol_arol is
'Association d''un rôle à un compte utilisateur.';

alter table application.tj_usr_rol_arol owner to :proprietaire;

grant select on application.tj_usr_rol_arol to :grpapplication;
grant insert on application.tj_usr_rol_arol to :grpapplication;
grant update on application.tj_usr_rol_arol to :grpapplication;
grant delete on application.tj_usr_rol_arol to :grpapplication;

-- Création de l'index
create index x_btr_usr_id_arol on application.tj_usr_rol_arol using btree (arol_usr_id);
comment on index application.x_btr_usr_id_arol is 
'Index sur le rattachement à un compte utilisateur afin d''optimiser le temps des requètes.';

-- Création de l'index
create index x_btr_rol_id_arol on application.tj_usr_rol_arol using btree (arol_rol_id);
comment on index application.x_btr_rol_id_arol is 
'Index sur le rattachement à un rôle afin d''optimiser le temps des requètes.';

/*==============================================================*/
/* Table : application.tj_rol_rol_rolp                          */
/*         Table associant un rôle parent à un rôle             */
/*==============================================================*/

create table application.tj_rol_rol_rolp (
	rolp_role_parent_id		int4	not null,
	rolp_role_enfant_id		int4	not null,
	constraint c_pk_tj_rol_rol_rolp primary key (rolp_role_parent_id, rolp_role_enfant_id),
	constraint c_fk_role_parent_rolp foreign key (rolp_role_parent_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_role_enfant_rolp foreign key (rolp_role_enfant_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_rol_rol_rolp is
'Table associant un rôle parent à un rôle enfant.';
comment on column application.tj_rol_rol_rolp.rolp_role_parent_id is
'Identifiant (postgres) du rôle parent.';
comment on column application.tj_rol_rol_rolp.rolp_role_enfant_id is
'Identifiant (postgres) du rôle enfant.';
comment on constraint c_fk_role_parent_rolp on application.tj_rol_rol_rolp is
'Association d''un rôle parent.';
comment on constraint c_fk_role_enfant_rolp on application.tj_rol_rol_rolp is
'Association d''un rôle enfant.';

alter table application.tj_rol_rol_rolp owner to :proprietaire;

grant select on application.tj_rol_rol_rolp to :grpapplication;
grant insert on application.tj_rol_rol_rolp to :grpapplication;
grant update on application.tj_rol_rol_rolp to :grpapplication;
grant delete on application.tj_rol_rol_rolp to :grpapplication;

-- Création de l'index
create index x_btr_role_parent_id_rolp on application.tj_rol_rol_rolp using btree (rolp_role_parent_id);
comment on index application.x_btr_role_parent_id_rolp is 
'Index sur le rattachement à un rôle parent afin d''optimiser le temps des requètes.';

-- Création de l'index
create index x_btr_role_enfant_id_rolp on application.tj_rol_rol_rolp using btree (rolp_role_enfant_id);
comment on index application.x_btr_role_enfant_id_rolp is 
'Index sur le rattachement à un rôle enfant afin d''optimiser le temps des requètes.';

/*==============================================================*/
/* Table : application.tj_rol_perm_aperm                        */
/*         Table associant une permission à un rôle             */
/*==============================================================*/

create table application.tj_rol_perm_aperm (
	aperm_rol_id		int4	not null,
	aperm_perm_id		int4	not null,
	constraint c_pk_tj_rol_perm_aperm primary key (aperm_rol_id, aperm_perm_id),
	constraint c_fk_rol_aperm foreign key (aperm_rol_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_perm_aperm foreign key (aperm_perm_id)
		references application.tr_permission_perm(perm_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_rol_perm_aperm is
'Table attribuant une permission à un rôle.';
comment on column application.tj_rol_perm_aperm.aperm_rol_id is
'Identifiant (postgres) d''un rôle.';
comment on column application.tj_rol_perm_aperm.aperm_perm_id is
'Identifiant (postgres) d''une permission.';
comment on constraint c_fk_rol_aperm on application.tj_rol_perm_aperm is
'Association d''un rôle à une permission.';
comment on constraint c_fk_perm_aperm on application.tj_rol_perm_aperm is
'Association d''une permission à un rôle.';

alter table application.tj_rol_perm_aperm owner to :proprietaire;

grant select on application.tj_rol_perm_aperm to :grpapplication;
grant insert on application.tj_rol_perm_aperm to :grpapplication;
grant update on application.tj_rol_perm_aperm to :grpapplication;
grant delete on application.tj_rol_perm_aperm to :grpapplication;

-- Création de l'index
create index x_btr_rol_id_aperm on application.tj_rol_perm_aperm using btree (aperm_rol_id);
comment on index application.x_btr_rol_id_aperm is 
'Index sur le rattachement à un rôle afin d''optimiser le temps des requètes.';

-- Création de l'index
create index x_btr_perm_id_aperm on application.tj_rol_perm_aperm using btree (aperm_perm_id);
comment on index application.x_btr_perm_id_aperm is 
'Index sur le rattachement à une permission afin d''optimiser le temps des requètes.';

/*==============================================================*/
/* Table : application.tr_type_rgpd_typrgpd                     */
/*         Table contenant les types de demandes RGPD           */
/*==============================================================*/
create table application.tr_type_rgpd_typrgpd (
	typrgpd_id				serial				not null,
	typrgpd_code			varchar				not null,
	typrgpd_libelle			varchar(255)		not null,
	constraint c_pk_tr_type_rgpd_typrgpd primary key (typrgpd_id)
);
comment on table application.tr_type_rgpd_typrgpd is
'Table contenant les types de demandes relatives au RGPD.';
comment on column application.tr_type_rgpd_typrgpd.typrgpd_id is
'Identifiant du type de la demande.';
comment on column application.tr_type_rgpd_typrgpd.typrgpd_code is
'Code du type de la demande.';
comment on column application.tr_type_rgpd_typrgpd.typrgpd_libelle is
'Libellé du type de demande.';

alter table application.tr_type_rgpd_typrgpd owner to :proprietaire;

grant select on application.tr_type_rgpd_typrgpd to :grpapplication;
grant insert on application.tr_type_rgpd_typrgpd to :grpapplication;
grant update on application.tr_type_rgpd_typrgpd to :grpapplication;
grant delete on application.tr_type_rgpd_typrgpd to :grpapplication;

/*==============================================================*/
/* Table : application.t_demande_rgpd                           */
/*         Table contenant les demandes RGPD                    */
/*==============================================================*/
create table application.t_demande_rgpd (
	rgpd_id					serial							not null,
	rgpd_usr_id				int4							not null,
	rgpd_typrgpd_id			int4							not null,
	rgpd_date_demande		timestamp without time zone		not null 	default now(),
	rgpd_message			text							null,
	rgpd_date_traitement	timestamp without time zone		null,
	rgpd_effectuee			bool							not null	default false,
	rgpd_invalide			bool							not null	default false,
	rgpd_raisons			varchar(255)					null,
	constraint c_pk_t_demande_rgpd primary key (rgpd_id),
	constraint c_fk_usr_rgpd foreign key (rgpd_usr_id)
		references application.t_utilisateur_usr(usr_id)
		on delete no action
		on update no action
		not deferrable,
	constraint c_fk_rgpd_typrgpd foreign key (rgpd_typrgpd_id)
		references application.tr_type_rgpd_typrgpd(typrgpd_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table application.t_demande_rgpd is
'Table contenant les demandes relatives au RGPD.';
comment on column application.t_demande_rgpd.rgpd_id is
'Identifiant d''une demande.';
comment on column application.t_demande_rgpd.rgpd_usr_id is
'Rattachement d''un utilisateur à la demande.';
comment on column application.t_demande_rgpd.rgpd_typrgpd_id is
'Rattachement du type de la demande.';
comment on column application.t_demande_rgpd.rgpd_date_demande is
'Date de la demande de traitement par l''utilisateur.';
comment on column application.t_demande_rgpd.rgpd_message is
'Message de l''utilisateur concernant la demande.';
comment on column application.t_demande_rgpd.rgpd_date_traitement is
'Date à laquelle le traitement de la demande a été effectué.';
comment on column application.t_demande_rgpd.rgpd_effectuee is
'Boolean indiquant si la demande a été traitée.';
comment on column application.t_demande_rgpd.rgpd_invalide is
'Boolean indiquant si la demande est valide.';
comment on column application.t_demande_rgpd.rgpd_raisons is
'Raison de l''invalidité de la demande.';

-- création de l'Index : x_btr_rgpd_usr_id
create index x_btr_rgpd_usr_id on application.t_demande_rgpd using btree (rgpd_usr_id);
comment on index application.x_btr_rgpd_usr_id is
'Index sur la rattachement d''un utilisateur à une demande relative au RGPD.';

-- création de l'Index : application.x_btr_rgpd_typrgpd_id
create index x_btr_rgpd_typrgpd_id on application.t_demande_rgpd using btree (rgpd_typrgpd_id);
comment on index application.x_btr_rgpd_typrgpd_id is
'Index sur la rattachement d''un droit conféré par les RGPD à la demande.';

alter table application.t_demande_rgpd owner to :proprietaire;

grant select on application.t_demande_rgpd to :grpapplication;
grant insert on application.t_demande_rgpd to :grpapplication;
grant update on application.t_demande_rgpd to :grpapplication;
grant delete on application.t_demande_rgpd to :grpapplication;

-- ***** Création des procédures et vues utilisées éventuellement par l'application PHP
/*==============================================================*/
/* Procedure : public.ps_inventaire                             */
/*                                                              */
/*==============================================================*/


CREATE OR REPLACE FUNCTION public.ps_inventaire (
  out v_schema varchar,
  out v_table varchar,
  out nb_enreg bigint
)
RETURNS SETOF record AS
$body$
DECLARE
  curs_table 			refcursor; 		-- cuirseur recevant la liste des tables
  rec_table  			record;			-- Enregistrement contenant la ligne en cours de traitement
  query_liste_table		text;
BEGIN
  --query_liste_table= "SELECT table_schema,table_name , table_type FROM information_schema.tables where table_schema in (SELECT schema_name FROM information_schema.schemata where schema_name not like 'pg%' and schema_name not IN('information_schema','topology')) and table_type in ('BASE TABLE') and table_name not in ('spatial_ref_sys') order by table_schema, table_name;";
  open curs_table FOR SELECT table_schema,table_name , table_type FROM information_schema.tables where table_schema in (SELECT schema_name FROM information_schema.schemata where schema_name not like 'pg%' and schema_name not IN('information_schema','topology')) and table_type in ('BASE TABLE') and table_name not in ('spatial_ref_sys') order by table_schema, table_name; 
  LOOP
  BEGIN
  	FETCH curs_table into rec_table;
  	if found then
		v_schema = rec_table.table_schema;
        v_table = rec_table.table_name;
        execute 'select count(*) from ' || v_schema || '.' || quote_ident(v_table) ||';' into nb_enreg;
		
        return next;
	ELSE
    	EXIT;  
    end if;
    EXCEPTION  
    WHEN OTHERS THEN
    	raise notice 'Erreur boucle : schema : %, table : %',  v_schema,v_table;
  END;
  end loop;
  
    
EXCEPTION
WHEN OTHERS THEN
	raise notice 'Erreur generale : % - schema : %, table : %',  sqlerrm,v_schema,v_table;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100 ROWS 1000;

COMMENT ON FUNCTION public.ps_inventaire(out v_schema varchar, out v_table varchar, out nb_enreg bigint)
IS 'Fonction permettant de renvoyer le nombre de lignes de chaque table excepté pour les schémas pg*, ''information_schema'' et ''topology''. La table spatial_ref_sys est également exclue de l''inventare.';
ALTER FUNCTION public.ps_inventaire (out v_schema varchar, out v_table varchar, out nb_enreg bigint)
  OWNER TO postgres;

/*==============================================================*/
/* Vue : application.v_volumetrie_brute                         */
/*                                                              */
/*==============================================================*/ 
-- *** Volumétrie brute *** :
--     L'ajout d'une vue permettant d'afficher la volumétrie des tables peut trouver sa place dans les données
--     descriptives que sert l'application basée sur les formulaires générique.
	CREATE VIEW application.v_volumetrie_brute (
		"table",
		"nb_lignes")
	AS
	SELECT ps_inventaire.v_table AS "table",
		ps_inventaire.nb_enreg AS "nb_lignes"
	FROM ps_inventaire() ps_inventaire(v_schema, v_table, nb_enreg)
	WHERE ps_inventaire.v_schema::text = 'public'::text AND ps_inventaire.v_table::text <> 'pgmfavorites'::text;

	COMMENT ON VIEW application.v_volumetrie_brute IS 'Vue utilisée par l''application basée sur le modèle ecological_db permettant d''afficher le nombre de lignes des différentes tables.';

	GRANT SELECT ON application.v_volumetrie_brute TO :grplecture;
	GRANT ALL ON application.v_volumetrie_brute TO :grpecriture;

	ALTER VIEW application.v_volumetrie_brute  OWNER TO :grpapplication;

-- *** Métadonnées de zone *** :
--     Le modèle ecological_db comporte une vue permettant d'extraire les métadonnées de zone en incluant les
--     métadonnées des parents (au sens du champ zon_tree de type ltree). Cet export brut peut-être carossé par
--     une vue qui sélectionnera certains champs, n'affichant que des données utiles et rendant la lecture plus
--     adaptée à une présentation dans les vues statistiques d'une application basée sur le modèle ecological_db
--     Sous réserve de la présence du schéma application et de la vue metadonnees_zone le code 
--     ci-dessous va créer une vue qui pourra être utilisée par le formulaire stats.global.php
	CREATE VIEW application.v_metadonnees_zone_appli (
		hierarchie,
		hierarchie_parent,
		est_heritee,
		date_mesure,
		variable_nom,
		code,
		valeur,
		unite,
		unite_symbole,
		protocole,
		id_zone,
		id_zone_parent)
	AS
	SELECT v_metadonnees_zone.ltree_enfant AS hierarchie,
			CASE
				WHEN v_metadonnees_zone.est_heritee THEN v_metadonnees_zone.ltree_parent
				ELSE NULL::ltree
			END AS hierarchie_parent,
		v_metadonnees_zone.est_heritee AS est_heritee,
		v_metadonnees_zone.date_mesure,
		v_metadonnees_zone.var_nom AS variable_nom,
		v_metadonnees_zone.var_code AS code,
		v_metadonnees_zone.valeur,
		v_metadonnees_zone.uni_nom AS unite,
		v_metadonnees_zone.uni_symbole AS unite_symbole,
		v_metadonnees_zone.prot_description AS protocole,
		v_metadonnees_zone.id_enfant AS id_zone,
			CASE
				WHEN v_metadonnees_zone.id_parent <> v_metadonnees_zone.id_enfant THEN v_metadonnees_zone.id_parent
				ELSE NULL::integer
			END AS id_zone_parent
	FROM v_metadonnees_zone as v_metadonnees_zone
	ORDER BY v_metadonnees_zone.ltree_enfant, v_metadonnees_zone.est_heritee, v_metadonnees_zone.ltree_parent;

	GRANT SELECT ON application.v_metadonnees_zone_appli TO :grplecture;
	GRANT ALL ON application.v_metadonnees_zone_appli TO :grpecriture;

	COMMENT ON VIEW application.v_metadonnees_zone_appli IS 'vue permettant un affichage travaillé des métadonnées de zones d''étude.';

	ALTER VIEW application.v_metadonnees_zone_appli  OWNER TO :grpapplication;


COMMIT