-- Fichier contenant un extrait de crebas (5 tables de fin de script) et sur lequel on peut exécuter un programme permettant la duplication / personnalisation
/*======================================================================*/
/* Table : objet                                                 		*/
/*		Cette table n'a d'intérêt que pour la maquette générique		*/
/*	Dans un véritable projet elle sera remplacée par une table   		*/
/*  représentant un objet (par exemple arbre).							*/
/*  Elle est toutefois utile ici car elle représente tous les liens que */
/*  doit / peut posséder une table objet notamment avec la table des    */
/*  mesure d'objet ci-après où elle est référencée.						*/
/*======================================================================*/
create table t_objet_obj (
   obj_id               serial               not null,
   obj_etiquette        varchar(64)          not null,
   constraint t_objet_obj_pkey primary key (obj_id),
   constraint c_uni_etiquette_obj unique(obj_etiquette)
);

comment on table t_objet_obj is 'Table des objets ...';
comment on column t_objet_obj.obj_id is 'Identifiant automatique numérique de l''objet';
comment on column t_objet_obj.obj_etiquette is 'Identifiant humain unique de l''objet - pourrait être constitué de plusieurs champs';
comment on constraint c_uni_etiquette_obj on public.t_objet_obj is 'L''étiquette de l''objet est unique';



/*====================================================================================*/
/* Table : mesure_obj                                           					  */
/*		Cette table devra être renommée voir dupliquée pour prendre en compte autant  */
/*		d'objets métiers à mesurer que nécessaire. Par exemple une table mesure_arb   */
/*		pour contenir les mesure concernant des arbres.								  */
/*		Les tables satellites devront elles aussi être renommées voir dupliquées pour */
/*		répondre au besoin.(il s'agit notamment des 3 tables suivantes :			  */
/*			celle qui associe la mesure à un opérateur et un rôle.					  */
/*			celle qui associe éventuellement la mesure à 1 ou plusieurs reroupements  */
/*			celle qui associe la mesure à une autre mesure considérée comme métadonnée*/
/*====================================================================================*/
create table t_mesure_marb (
   marb_id               serial               not null,
   marb_arb_id			 bigint				  not null, 
   marb_typm_id          integer              null,
   marb_var_id           integer              not null,
   marb_fic_id           integer              null,
   marb_prot_id          integer              null,
   marb_norm_id          integer              null,
   marb_uni_id           integer              null,
   marb_valeur_num       float8               null,
   marb_valeur_text      varchar(1)           null,
   marb_valeur_date      date                 null,
   marb_date_mesure      date                 null,
   marb_precision_date_mesure float8          null,
   marb_precision_mesure 	 float8          null,
   marb_remarque         varchar              null,
   constraint t_mesure_marb_pkey primary key (marb_id),
   constraint c_uni_var_datemesure_marb unique(marb_var_id,marb_date_mesure), -- Cette clé 'humaine' pourra être revisitée lors de la spécialisation pour les mesures de différents objets
   constraint c_fk_arb_marb foreign key (marb_arb_id)
		references public.t_arbre_arb(arb_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_typm_marb foreign key (marb_typm_id)
		references public.tr_type_mesure_typm(typm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_var_marb foreign key (marb_var_id)
    	references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_fic_marb foreign key (marb_fic_id)
		references public.tr_fichier_fic(fic_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_prot_marb foreign key (marb_prot_id)
		references public.tr_protocole_prot(prot_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_norm_marb foreign key (marb_norm_id)
		references public.tr_norme_norm(norm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_uni_marb foreign key (marb_uni_id)
		references public.tr_unite_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table t_mesure_marb is 'Table des mesures de variables pour un type d''objet spécifique';
comment on column t_mesure_marb.marb_id is 'Identifiant automatique numérique de la mesure';
comment on column t_mesure_marb.marb_arb_id is 'Identifiant automatique numérique de l''objet associé';
comment on column t_mesure_marb.marb_typm_id is 'Identifiant automatique numérique du type de mesure associé';
comment on column t_mesure_marb.marb_var_id is 'Identifiant unique numérique de la variable associée';
comment on column t_mesure_marb.marb_fic_id is 'Identifiant automatique numérique du fichier associé';
comment on column t_mesure_marb.marb_prot_id is 'Identifiant unique numérique du protocole associé';
comment on column t_mesure_marb.marb_norm_id is 'Identifiant numérique automatique de la norme associée';
comment on column t_mesure_marb.marb_uni_id is 'Identifiant unique numérique de l''unité de mesure associée';
comment on column t_mesure_marb.marb_valeur_num is 'valeur de la mesure si celle-ci est de type numérique';
comment on column t_mesure_marb.marb_valeur_text is 'valeur de la mesure si celle-ci est de type texte';
comment on column t_mesure_marb.marb_valeur_date is 'valeur de la mesure si celle-ci est de type date';
comment on column t_mesure_marb.marb_date_mesure is 'date de la mesure';
comment on column t_mesure_marb.marb_precision_date_mesure is 'Précision concernant la date de la mesure';
comment on column t_mesure_marb.marb_precision_mesure is 'Précision de la valeur de la mesure';
comment on column t_mesure_marb.marb_remarque is 'Information complémentaire associée à la mesure';
comment on constraint c_uni_var_datemesure_marb on public.t_mesure_marb is 'Le couple identifiant de la variable / date de la mesure est unique';

comment on constraint c_fk_arb_marb on public.t_mesure_marb is 'une mesure doit être rattachée à un et un seul objet';
comment on constraint c_fk_typm_marb on public.t_mesure_marb is 'une mesure peut être rattachée à O ou 1 type de mesure';
comment on constraint c_fk_var_marb on public.t_mesure_marb is 'une mesure doit être rattachée à une et une seule variable';
comment on constraint c_fk_fic_marb on public.t_mesure_marb is 'une mesure peut être rattachée à O ou 1 fichier';
comment on constraint c_fk_prot_marb on public.t_mesure_marb is 'une mesure peut être rattachée à O ou 1 protocole';
comment on constraint c_fk_norm_marb on public.t_mesure_marb is 'une mesure peut être rattachée à O ou 1 norme';
comment on constraint c_fk_uni_marb on public.t_mesure_marb is 'une mesure peut être rattachée à O ou 1 unité';

-- Index pour les clés etrangères
create index x_btr_fkey_arb_marb on public.t_mesure_marb
  using btree (marb_arb_id);
comment on index public.x_btr_fkey_arb_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_typm_marb on public.t_mesure_marb
  using btree (marb_typm_id);
comment on index public.x_btr_fkey_typm_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_var_marb on public.t_mesure_marb
  using btree (marb_var_id);
comment on index public.x_btr_fkey_var_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_fic_marb on public.t_mesure_marb
  using btree (marb_fic_id);
comment on index public.x_btr_fkey_fic_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_prot_marb on public.t_mesure_marb
  using btree (marb_prot_id);
comment on index public.x_btr_fkey_prot_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_norm_marb on public.t_mesure_marb
  using btree (marb_norm_id);
comment on index public.x_btr_fkey_norm_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_uni_marb on public.t_mesure_marb
  using btree (marb_uni_id);
comment on index public.x_btr_fkey_uni_marb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : tj_role_ope_marb_roa                                    */
/*			Table associant une mesure d'objet à un opérateur   */
/*			et un rôle										    */
/*==============================================================*/
create table tj_role_ope_marb_roa (
   roa_marb_id           integer              not null,
   roa_ope_id           integer              not null,
   roa_rol_id			integer				 not null,
   constraint tj_role_ope_marb_roa_pkey primary key (roa_marb_id, roa_ope_id,roa_rol_id),
   constraint c_fk_marb_roa foreign key (roa_marb_id)
		references public.t_mesure_marb(marb_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_roa foreign key (roa_ope_id)
		references public.tr_operateur_ope(ope_id)
		on delete no action
		on update no action
		not deferrable, 		
   constraint c_fk_rol_roa foreign key (roa_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_role_ope_marb_roa is 'Table de jointure associant un regroupement à un opérateur et à un rôle pour cet opérateur';
comment on column tj_role_ope_marb_roa.roa_marb_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_role_ope_marb_roa.roa_ope_id is 'Identifiant automatique numérique de l''opérateur associé';
comment on column tj_role_ope_marb_roa.roa_rol_id is 'Identifiant automatique numérique du rôle associé';
comment on constraint c_fk_marb_roa on public.tj_role_ope_marb_roa is 'Le champ roa_marb_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_ope_roa on public.tj_role_ope_marb_roa is 'Le champ roa_ope_id correspond obligatoirement à un opérateur existant';
comment on constraint c_fk_rol_roa on public.tj_role_ope_marb_roa is 'Le champ roa_rol_id correspond obligatoirement à un rôle existant';

-- Index pour les clés etrangères
create index x_btr_marb_roa on public.tj_role_ope_marb_roa
  using btree (roa_marb_id);
comment on index public.x_btr_marb_roa is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_ope_roa on public.tj_role_ope_marb_roa
  using btree (roa_ope_id);
comment on index public.x_btr_ope_roa is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rol_roa on public.tj_role_ope_marb_roa
  using btree (roa_rol_id);
comment on index public.x_btr_rol_roa is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*=================================================================*/
/* Table : tj_rgp_marb_gma                                         */
/*			Table associant une mesure d'objet à un regroupement   */
/*=================================================================*/
create table tj_rgp_marb_gma (
   gma_marb_id          integer              not null,
   gma_rgp_id           integer              not null,
   constraint tj_rgp_marb_gma_pkey primary key (gma_marb_id, gma_rgp_id),
   constraint c_fk_marb_gma foreign key (gma_marb_id)
		references public.t_mesure_marb(marb_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rgp_gma foreign key (gma_rgp_id)
		references public.t_regroupement_rgp(rgp_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_rgp_marb_gma is 'Table de jointure associant une mesure à un regroupement.';
comment on column tj_rgp_marb_gma.gma_marb_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_rgp_marb_gma.gma_rgp_id is 'Identifiant automatique numérique du regroupement associé';
comment on constraint c_fk_marb_gma on public.tj_rgp_marb_gma is 'Le champ gma_marb_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_rgp_gma on public.tj_rgp_marb_gma is 'Le champ gma_rgp_id correspond obligatoirement à un regroupememnt existant';

-- Index pour les clés etrangères
create index x_btr_marb_gma on public.tj_rgp_marb_gma
  using btree (gma_marb_id);
comment on index public.x_btr_marb_gma is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rgp_gma on public.tj_rgp_marb_gma
  using btree (gma_rgp_id);
comment on index public.x_btr_rgp_gma is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*=================================================================*/
/* Table : tj_marb_marb_tarb                                       */
/*			Table associant une mesure d'objet à une autre mesure  */
/*			d'objet, la seconde étant complémentaire (métadonnée)  */
/*			ou nécessaire à l'interprétation de la première		   */
/*=================================================================*/
create table tj_marb_marb_tarb (
   tarb_ref_marb_id          integer              not null,
   tarb_meta_marb_id         integer              not null,
   tarb_statut				 varchar(32)		  null,
   constraint tj_marb_marb_tarb_pkey primary key (tarb_ref_marb_id, tarb_meta_marb_id),
   constraint c_fk_ref_marb_tarb foreign key (tarb_ref_marb_id)
		references public.t_mesure_marb(marb_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_meta_marb_tarb foreign key (tarb_meta_marb_id)
		references public.t_mesure_marb(marb_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_marb_marb_tarb is 'Table de jointure associant une mesure à une autre mesure, la seconde étant complémentaire (métadonnée) ou nécessaire à l''interprétation de la première.';
comment on column tj_marb_marb_tarb.tarb_ref_marb_id is 'Identifiant automatique numérique de la mesure référence';
comment on column tj_marb_marb_tarb.tarb_meta_marb_id is 'Identifiant automatique numérique de la mesure considérée comme métadonnée de la mesure référence';
comment on column tj_marb_marb_tarb.tarb_statut is 'Indicateur précisant le statut de la métadonnée par rapport à la mesure de référence. Par exemple le caractère obligatoire ou recommandé pour l''interprétation.';

comment on constraint c_fk_ref_marb_tarb on public.tj_marb_marb_tarb is 'Le champ tarb_ref_marb_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_meta_marb_tarb on public.tj_marb_marb_tarb is 'Le champ tarb_meta_marb_id correspond obligatoirement à une mesure existante';

-- Index pour les clés etrangères
create index x_btr_marb_tarb on public.tj_marb_marb_tarb
  using btree (tarb_ref_marb_id);
comment on index public.x_btr_marb_tarb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rgp_tarb on public.tj_marb_marb_tarb
  using btree (tarb_meta_marb_id);
comment on index public.x_btr_rgp_tarb is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
