---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- Creation date :	19/07/2022
-- Last update	:	19/07/2022
---------------------------------------------------------------------------------
-- Description / objectif : 	This SQL script performs a transaction on the current database
--                              - to create the structure (Tables, constraints, index ...). 
--					
--					The file of this script is encoded in UTF8 
-- Usage : 	*	This script should be run on a previously created database using the following scrip :
--				https://git.renater.fr/anonscm/gitweb?p=script/script.git;a=blob_plain;f=serveurs+virtuels/pggeodb/exploitation/creer_base.sh
--			* 	The account to use must have sufficient privileges to create tables, indexes and constraints ...
--			*	The use of an interpolated psql variable in the sql (\ set) code only works with the psql tool that should be used
--          *   to run this script (otherwise the script must be modified beforehand to comply with the Classic SQL.
--			* SYNTAXE : psql -f crebas_schema_application_en.sql nombase
-- PARAMETERS :
--		entrance :
-- 			nothing
--		output :
-- 			
-- UPDATE :
\set owner 'ecological_db'
\set grpconnect 'ecological_db_connect'
\set grpwriting 'ecological_db_writing'
\set grpreading 'ecological_db_reading'
\set grpapplication 'ecological_db_application'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

START TRANSACTION;

create schema application;
alter schema application OWNER to :owner;
grant usage on schema application to :grpapplication;
grant usage on schema application to :grpreading; -- pour les formulaires access

/*==============================================================*/
/* Table : application.t_access_configuration_form_acf          */
/*==============================================================*/
-- This table is only useful if an application based on the standardized Access forms of the SIG-BD Platform of the SILVA unit will be developed.

create table application.t_access_configuration_form_acf (
	acf_id				serial			not null,
	acf_sgbd			varchar(100)	not null,
	acf_version			varchar(4)		null,
	acf_odbc			boolean         not null,
	acf_disconnected	boolean         not null,
	acf_instance_num	integer			null,
	acf_database		varchar(100)	not null,
	acf_role_writing	varchar(100)	null,
	acf_tag				varchar(255)	null,
	constraint t_access_configuration_form_acf_pkey primary key (acf_id)
);

comment on table application.t_access_configuration_form_acf is
'Table containing part of the access application configuration.';
comment on column application.t_access_configuration_form_acf.acf_id is
'Configuration identifier.';
comment on column application.t_access_configuration_form_acf.acf_sgbd is
'Name of the sgbd used.';
comment on column application.t_access_configuration_form_acf.acf_version is
'Version of sgbd used.';
comment on column application.t_access_configuration_form_acf.acf_odbc is
'Boolean indicating whether the application is connected to the database in connected mode (linked tables).';
comment on column application.t_access_configuration_form_acf.acf_disconnected is
'Boolean indicating whether the application is connected to the database in disconnected mode.';
comment on column application.t_access_configuration_form_acf.acf_instance_num is
'Number indicating the number of the instance, information used in offline mode for multiple entry.';
comment on column application.t_access_configuration_form_acf.acf_database is
'Name of the database used.';
comment on column application.t_access_configuration_form_acf.acf_role_writing is
'Name of the role with write rights.';
comment on column application.t_access_configuration_form_acf.acf_tag is
'Tag allowing to carry out a particular treatment according to the value of the latter.';

alter table application.t_access_configuration_form_acf owner to :owner;

grant select on application.t_access_configuration_form_acf to :grpreading;
grant select on application.t_access_configuration_form_acf to :grpwriting;
grant select on application.t_access_configuration_form_acf to :grpapplication;

-- ******************************************************************************************
-- ***** Gestion des droits par défauts lors de la création de nouveaux objets postgres *****
-- ******************************************************************************************

alter default privileges
	in schema application
	grant all
	on sequences
	TO :owner, :grpapplication;

-- *******************************************************
-- ***** Creation of tables allowing user management  ****
-- *******************************************************
-- These tables are only useful for an application based on the standardized PHP forms of the SIG-BD Platform of the SILVA unit.

/*==============================================================*/
/* Table : application.t_organization_org                       */
/*==============================================================*/
create table application.t_organization_org (
	org_id			serial			not null,
	org_ldap_uid	varchar(100)	null,
	org_code		varchar(100)    not null,
	org_label		varchar(255)	not null,
	constraint c_pk_t_organization_org primary key(org_id),
	constraint c_uni_code_org unique (org_code)
);
comment on table application.t_organization_org is
'Table containing the organizations (INRAe Center for INRAe LDAP).';
comment on column application.t_organization_org.org_id is
'Organization identifier (postgres).';
comment on column application.t_organization_org.org_ldap_uid is
'LDAP identifier of the organization.';
comment on column application.t_organization_org.org_code is
'Organization code.';
comment on column application.t_organization_org.org_label is
'Organization label.';
comment on constraint c_uni_code_org on application.t_organization_org is
'Uniqueness constraint on the organization code.';

alter table application.t_organization_org owner to :owner;

grant select on application.t_organization_org to :grpapplication;
grant insert on application.t_organization_org to :grpapplication;
grant update on application.t_organization_org to :grpapplication;
grant delete on application.t_organization_org to :grpapplication;

/*==============================================================*/
/* Table : application.t_service_serv							*/
/*==============================================================*/
create table application.t_service_serv (
	serv_id			serial			not null,
	serv_ldap_uid	varchar(100)	null,
	serv_code		varchar(100)	not null,
	serv_label		varchar(255)	not null,
	constraint c_pk_t_service_serv primary key(serv_id),
	constraint c_uni_code_serv unique (serv_code)
);
comment on table application.t_service_serv is
'Table containing the different services (Unit for INRAe LDAP).';
comment on column application.t_service_serv.serv_id is
'Service ID (postgres).';
comment on column application.t_service_serv.serv_ldap_uid is
'Identifier of the service within the LDAP.';
comment on column application.t_service_serv.serv_code is
'Service code.';
comment on column application.t_service_serv.serv_label is
'Service label.';
comment on constraint c_uni_code_serv on application.t_service_serv is
'Uniqueness constraint on the service code.';

alter table application.t_service_serv owner to :owner;

grant select on application.t_service_serv to :grpapplication;
grant insert on application.t_service_serv to :grpapplication;
grant update on application.t_service_serv to :grpapplication;
grant delete on application.t_service_serv to :grpapplication;

/*==============================================================*/
/* Table : application.t_user_usr                               */
/*==============================================================*/

create table application.t_user_usr (
	usr_id						serial						not null,
	usr_org_id					int4						null,
	usr_serv_id					int4						null,
	usr_login					varchar(255)				null,
	usr_password				varchar(255)				null,
	usr_last_name				varchar(100)				null,
	usr_first_name				varchar(100)				null,
	usr_mail					varchar(255)				not null,
	usr_activation_code			varchar(255)				null,
	usr_code_creation			timestamp with time zone	null,
	usr_code_expiration			timestamp with time zone	null,
	usr_ldap_name          		varchar(20)					null,
	usr_ldap_uid            	varchar(100)    			null,
	usr_oauth2_provider			varchar(15)					null,
	usr_oauth2_provider_id  	varchar(100)    			null,
	usr_refresh_token			varchar(255)				null,
	usr_blocked					boolean						not null default false,
	usr_account_creation		date						null,
	usr_last_login				date						null,
	usr_account_validity		date						null,
	usr_license_signature		date						null,
	usr_license_version			varchar(4)					null,
	usr_totp_secret				varchar(50)					null,
	usr_totp_recovery_code		varchar(100)				null,
	constraint c_pk_t_user_usr primary key(usr_id),
	constraint c_uni_login_mail_usr unique (usr_login, usr_mail),
	constraint c_uni_login_ldap_mail_usr unique (usr_login, usr_ldap_name, usr_mail),
	constraint c_uni_login_provider_mail_usr unique (usr_login, usr_oauth2_provider, usr_mail),
	constraint c_uni_mail_usr unique (usr_mail),
	constraint c_fk_usr_org foreign key (usr_org_id)
		references application.t_organization_org(org_id)
		on delete no action
		on update no action
		not deferrable,
	constraint c_fk_usr_serv foreign key (usr_serv_id)
		references application.t_service_serv(serv_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table application.t_user_usr is
'Table containing user accounts.';
comment on column application.t_user_usr.usr_id is
'User ID (postgres).';
comment on column application.t_user_usr.usr_org_id is
'Attachment of an organization to a user.';
comment on column application.t_user_usr.usr_serv_id is
'Attachment of a service to a user.';
comment on column application.t_user_usr.usr_login is
'User login.';
comment on column application.t_user_usr.usr_password is
'User password.';
comment on column application.t_user_usr.usr_last_name is
'Username.';
comment on column application.t_user_usr.usr_first_name is
'User''s first name.';
comment on column application.t_user_usr.usr_mail is
'User email.';
comment on column application.t_user_usr.usr_activation_code is
'Activation code generated during the account request or during the password reset request.';
comment on column application.t_user_usr.usr_code_creation is
'Date and time of creation of the temporary code allowing the creation of an account.';
comment on column application.t_user_usr.usr_code_expiration is
'Expiry date and time of the temporary code allowing account creation.';
comment on column application.t_user_usr.usr_ldap_name is
'LDAP name that the user uses to connect.';
comment on column application.t_user_usr.usr_ldap_uid is
'Identifier of the user in the LDAP that he uses to connect.';
comment on column application.t_user_usr.usr_oauth2_provider is
'Token provider used by the user to login.';
comment on column application.t_user_usr.usr_oauth2_provider_id is
'User ID in the token provider used by the user to login.';
comment on column application.t_user_usr.usr_refresh_token is
'Refresh token.';
comment on column application.t_user_usr.usr_blocked is
'Boolean indicating whether the user''s account is blocked or not.';
comment on column application.t_user_usr.usr_account_creation is
'Date on which the user''s account was created.';
comment on column application.t_user_usr.usr_last_login is
'Date of the last login with this user account.';
comment on column application.t_user_usr.usr_account_validity is
'Date on which the user''s account expires.';
comment on column application.t_user_usr.usr_license_signature is
'Date on which the user accepted the license.';
comment on column application.t_user_usr.usr_license_version is
'Version number of the license that the user has accepted.';
comment on column application.t_user_usr.usr_totp_secret is
'Automatically generated secret code when activating 2-factor authentication.';
comment on column application.t_user_usr.usr_totp_recovery_code is
'Recovery code automatically generated when activating 2-factor authentication.';
comment on constraint c_uni_login_mail_usr on application.t_user_usr is
'Uniqueness on the user''s login and email uniqueness required for accounts saved in the database.';
comment on constraint c_uni_login_ldap_mail_usr on application.t_user_usr is
'Uniqueness on the login, the name of the ldap and the email of the user, uniqueness necessary to uniquely identify an account when connecting via an ldap.';
comment on constraint c_uni_login_provider_mail_usr on application.t_user_usr is
'Uniqueness on the login, the name of the identity provider and the user''s email, uniqueness necessary to uniquely identify an account when connecting via the OAuth2 protocol.';
comment on constraint c_uni_mail_usr on application.t_user_usr is
'Uniqueness on the user''s email.';
comment on constraint c_fk_usr_org on application.t_user_usr is
'Attachment of an organization to a user.';
comment on constraint c_fk_usr_serv on application.t_user_usr is
'Attachment of a service to a user.';

-- index creation : application.x_btr_usr_org_id
create index x_btr_usr_org_id on application.t_user_usr using btree (usr_org_id);
comment on index application.x_btr_usr_org_id is
'Index on the attachment of an organization to a user.';

-- index creation : application.x_btr_usr_serv_id
create index x_btr_usr_serv_id on application.t_user_usr using btree (usr_serv_id);
comment on index application.x_btr_usr_serv_id is
'Index on the attachment of a service to a user.';

alter table application.t_user_usr owner to :owner;

grant select on application.t_user_usr to :grpapplication;
grant insert on application.t_user_usr to :grpapplication;
grant update on application.t_user_usr to :grpapplication;
grant delete on application.t_user_usr to :grpapplication;

/*==============================================================*/
/* Table : application.tr_role_rol                              */
/*==============================================================*/

create table application.tr_role_rol (
	rol_id			serial			not null,
	rol_label		varchar(25)		not null,
	rol_description	varchar(255)	null,
	constraint c_pk_tr_role_rol primary key(rol_id),
	constraint c_uni_label_rol unique (rol_label)
);
comment on table application.tr_role_rol is
'Table containing the different roles that can be assigned to users.';
comment on column application.tr_role_rol.rol_id is
'Identifier (postgres) of the role.';
comment on column application.tr_role_rol.rol_label is
'Label of the role.';
comment on column application.tr_role_rol.rol_description is
'Descritpion of the role.';
comment on constraint c_uni_label_rol on application.tr_role_rol is
'Uniqueness on the label of the role.';

alter table application.tr_role_rol owner to :owner;

grant select on application.tr_role_rol to :grpapplication;
grant insert on application.tr_role_rol to :grpapplication;
grant update on application.tr_role_rol to :grpapplication;
grant delete on application.tr_role_rol to :grpapplication;

/*==============================================================*/
/* Table : application.tr_permission_perm                       */
/*==============================================================*/

create table application.tr_permission_perm (
	perm_id				serial			not null,
	perm_label			varchar(100)	not null,
	perm_description	varchar(255)	not null,
	constraint c_pk_tr_permission_perm primary key(perm_id),
	constraint c_uni_label_perm unique (perm_label)
);
comment on table application.tr_permission_perm is
'Table containing the different permissions that can be assigned to roles.';
comment on column application.tr_permission_perm.perm_id is
'Identifier (postgres) of the permission.';
comment on column application.tr_permission_perm.perm_label is
'Label of the permission.';
comment on column application.tr_permission_perm.perm_description is
'Description of the rights given by the permission.';
comment on constraint c_uni_label_perm on application.tr_permission_perm is
'Uniqueness on the label of the permission.';

alter table application.tr_permission_perm owner to :owner;

grant select on application.tr_permission_perm to :grpapplication;
grant insert on application.tr_permission_perm to :grpapplication;
grant update on application.tr_permission_perm to :grpapplication;
grant delete on application.tr_permission_perm to :grpapplication;

/*==============================================================*/
/* Table : application.tj_usr_rol_arol                          */
/*         Table associating a user account with a role         */
/*==============================================================*/

create table application.tj_usr_rol_arol (
	arol_usr_id		int4	not null,
	arol_rol_id		int4	not null,
	constraint c_pk_tj_usr_rol_arol primary key (arol_usr_id, arol_rol_id),
	constraint c_fk_usr_arol foreign key (arol_usr_id)
		references application.t_user_usr(usr_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_rol_arol foreign key (arol_rol_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_usr_rol_arol is
'Table assigning a role to a user''s account.';
comment on column application.tj_usr_rol_arol.arol_usr_id is
'Identifier (postgres) of a user.';
comment on column application.tj_usr_rol_arol.arol_rol_id is
'Identifier (postgres) of a role.';
comment on constraint c_fk_usr_arol on application.tj_usr_rol_arol is
'Association of a user account with a role.';
comment on constraint c_fk_rol_arol on application.tj_usr_rol_arol is
'Association of a role with a user account.';

alter table application.tj_usr_rol_arol owner to :owner;

grant select on application.tj_usr_rol_arol to :grpapplication;
grant insert on application.tj_usr_rol_arol to :grpapplication;
grant update on application.tj_usr_rol_arol to :grpapplication;
grant delete on application.tj_usr_rol_arol to :grpapplication;

-- Index creation
create index x_btr_usr_id_arol on application.tj_usr_rol_arol using btree (arol_usr_id);
comment on index application.x_btr_usr_id_arol is 
'Index on the attachment to a user account in order to optimize the time of requests.';

-- Index creation
create index x_btr_rol_id_arol on application.tj_usr_rol_arol using btree (arol_rol_id);
comment on index application.x_btr_rol_id_arol is 
'Index on the attachment to a role in order to optimize the time of requests.';

/*==============================================================*/
/* Table : application.tj_rol_rol_rolp                          */
/*         Table associating a parent role with a role          */
/*==============================================================*/

create table application.tj_rol_rol_rolp (
	rolp_role_parent_id		int4	not null,
	rolp_role_child_id		int4	not null,
	constraint c_pk_tj_rol_rol_rolp primary key (rolp_role_parent_id, rolp_role_child_id),
	constraint c_fk_role_parent_rolp foreign key (rolp_role_parent_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_role_child_rolp foreign key (rolp_role_child_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_rol_rol_rolp is
'Table associating a parent role with a child role.';
comment on column application.tj_rol_rol_rolp.rolp_role_parent_id is
'Identifier (postgres) of the parent role.';
comment on column application.tj_rol_rol_rolp.rolp_role_child_id is
'Identifier (postgres) of the child role.';
comment on constraint c_fk_role_parent_rolp on application.tj_rol_rol_rolp is
'Association of a parent role.';
comment on constraint c_fk_role_child_rolp on application.tj_rol_rol_rolp is
'Association of a child role.';

alter table application.tj_rol_rol_rolp owner to :owner;

grant select on application.tj_rol_rol_rolp to :grpapplication;
grant insert on application.tj_rol_rol_rolp to :grpapplication;
grant update on application.tj_rol_rol_rolp to :grpapplication;
grant delete on application.tj_rol_rol_rolp to :grpapplication;

-- Index creation
create index x_btr_role_parent_id_rolp on application.tj_rol_rol_rolp using btree (rolp_role_parent_id);
comment on index application.x_btr_role_parent_id_rolp is 
'Index on the attachment to a parent role in order to optimize the time of requests.';

-- Index creation
create index x_btr_role_child_id_rolp on application.tj_rol_rol_rolp using btree (rolp_role_child_id);
comment on index application.x_btr_role_child_id_rolp is 
'Index on the attachment to a child role in order to optimize the time of requests.';

/*==============================================================*/
/* Table : application.tj_rol_perm_aperm                        */
/*         Table associating a permission to a role             */
/*==============================================================*/

create table application.tj_rol_perm_aperm (
	aperm_rol_id		int4	not null,
	aperm_perm_id		int4	not null,
	constraint c_pk_tj_rol_perm_aperm primary key (aperm_rol_id, aperm_perm_id),
	constraint c_fk_rol_aperm foreign key (aperm_rol_id)
		references application.tr_role_rol(rol_id)
		on delete cascade
		on update no action
		not deferrable,
	constraint c_fk_perm_aperm foreign key (aperm_perm_id)
		references application.tr_permission_perm(perm_id)
		on delete cascade
		on update no action
		not deferrable
);
comment on table application.tj_rol_perm_aperm is
'Table assigning a permission to a role.';
comment on column application.tj_rol_perm_aperm.aperm_rol_id is
'Identifier (postgres) of a role.';
comment on column application.tj_rol_perm_aperm.aperm_perm_id is
'Identifier (postgres) of a permission.';
comment on constraint c_fk_rol_aperm on application.tj_rol_perm_aperm is
'Association of a role with a permission.';
comment on constraint c_fk_perm_aperm on application.tj_rol_perm_aperm is
'Association of a permission to a role.';

alter table application.tj_rol_perm_aperm owner to :owner;

grant select on application.tj_rol_perm_aperm to :grpapplication;
grant insert on application.tj_rol_perm_aperm to :grpapplication;
grant update on application.tj_rol_perm_aperm to :grpapplication;
grant delete on application.tj_rol_perm_aperm to :grpapplication;

-- Index creation
create index x_btr_rol_id_aperm on application.tj_rol_perm_aperm using btree (aperm_rol_id);
comment on index application.x_btr_rol_id_aperm is 
'Index on the attachment to a role in order to optimize the time of requests.';

-- Index creation
create index x_btr_perm_id_aperm on application.tj_rol_perm_aperm using btree (aperm_perm_id);
comment on index application.x_btr_perm_id_aperm is 
'Index on the attachment to a permission in order to optimize the time of requests';

/*==============================================================*/
/* Table : application.tr_type_gdpr_typgdpr                     */
/*         Table containing types of GDPR requests              */
/*==============================================================*/
create table application.tr_type_gdpr_typgdpr (
	typgdpr_id				serial				not null,
	typgdpr_code			varchar				not null,
	typgdpr_label			varchar(255)		not null,
	constraint c_pk_tr_type_gdpr_typgdpr primary key (typgdpr_id)
);
comment on table application.tr_type_gdpr_typgdpr is
'Table containing types of GDPR-related requests.';
comment on column application.tr_type_gdpr_typgdpr.typgdpr_id is
'Identifier of the request type (postgres).';
comment on column application.tr_type_gdpr_typgdpr.typgdpr_code is
'Request type code.';
comment on column application.tr_type_gdpr_typgdpr.typgdpr_label is
'Label of the type of request.';

alter table application.tr_type_gdpr_typgdpr owner to :owner;

grant select on application.tr_type_gdpr_typgdpr to :grpapplication;
grant insert on application.tr_type_gdpr_typgdpr to :grpapplication;
grant update on application.tr_type_gdpr_typgdpr to :grpapplication;
grant delete on application.tr_type_gdpr_typgdpr to :grpapplication;

/*==============================================================*/
/* Table : application.t_request_gdpr                           */
/*         Table containing GDPR requests                       */
/*==============================================================*/
create table application.t_request_gdpr (
	gdpr_id					serial							not null,
	gdpr_usr_id				int4							not null,
	gdpr_typgdpr_id			int4							not null,
	gdpr_request_date		timestamp without time zone		not null 	default now(),
	gdpr_message			text							null,
	gdpr_treatment_date		timestamp without time zone		null,
	gdpr_executed			bool							not null	default false,
	gdpr_invalid			bool							not null	default false,
	gdpr_reasons			varchar(255)					null,
	constraint c_pk_t_request_gdpr primary key (gdpr_id),
	constraint c_fk_usr_gdpr foreign key (gdpr_usr_id)
		references application.t_user_usr(usr_id)
		on delete no action
		on update no action
		not deferrable,
	constraint c_fk_gdpr_typgdpr foreign key (gdpr_typgdpr_id)
		references application.tr_type_gdpr_typgdpr(typgdpr_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table application.t_request_gdpr is
'Table containing GDPR-related requests.';
comment on column application.t_request_gdpr.gdpr_id is
'Identifier of a request (postgres).';
comment on column application.t_request_gdpr.gdpr_usr_id is
'Attachment of a user to the request.';
comment on column application.t_request_gdpr.gdpr_typgdpr_id is
'Attachment of the request type.';
comment on column application.t_request_gdpr.gdpr_request_date is
'Date of the processing request by the user.';
comment on column application.t_request_gdpr.gdpr_message is
'Message from the user regarding the request.';
comment on column application.t_request_gdpr.gdpr_treatment_date is
'Date the request was processed.';
comment on column application.t_request_gdpr.gdpr_executed is
'Boolean indicating whether the request has been processed.';
comment on column application.t_request_gdpr.gdpr_invalid is
'Boolean indicating whether the request is valid.';
comment on column application.t_request_gdpr.gdpr_reasons is
'Reason for the invalidity of the request.';

-- Index creation : application.x_btr_gdpr_usr_id
create index x_btr_gdpr_usr_id on application.t_request_gdpr using btree (gdpr_usr_id);
comment on index application.x_btr_gdpr_usr_id is
'Index on the connection of a user to a request relating to the GDPR.';

-- Index creation : application.x_btr_gdpr_typgdpr_id
create index x_btr_gdpr_typgdpr_id on application.t_request_gdpr using btree (gdpr_typgdpr_id);
comment on index application.x_btr_gdpr_typgdpr_id is
'Index on the attachment of a right conferred by the GDPR to the request.';

alter table application.t_request_gdpr owner to :owner;

grant select on application.t_request_gdpr to :grpapplication;
grant insert on application.t_request_gdpr to :grpapplication;
grant update on application.t_request_gdpr to :grpapplication;
grant delete on application.t_request_gdpr to :grpapplication;

-- ***** Création des procédures et vues utilisées éventuellement par l'application PHP
/*==============================================================*/
/* Procedure : public.ps_inventaire                             */
/*                                                              */
/*==============================================================*/

CREATE OR REPLACE FUNCTION public.ps_inventaire (
  out v_schema varchar,
  out v_table varchar,
  out nb_enreg bigint
)
RETURNS SETOF record AS
$body$
DECLARE
  curs_table 			refcursor; 		-- cuirseur recevant la liste des tables
  rec_table  			record;			-- Enregistrement contenant la ligne en cours de traitement
  query_liste_table		text;
BEGIN
  --query_liste_table= "SELECT table_schema,table_name , table_type FROM information_schema.tables where table_schema in (SELECT schema_name FROM information_schema.schemata where schema_name not like 'pg%' and schema_name not IN('information_schema','topology')) and table_type in ('BASE TABLE') and table_name not in ('spatial_ref_sys') order by table_schema, table_name;";
  open curs_table FOR SELECT table_schema,table_name , table_type FROM information_schema.tables where table_schema in (SELECT schema_name FROM information_schema.schemata where schema_name not like 'pg%' and schema_name not IN('information_schema','topology')) and table_type in ('BASE TABLE') and table_name not in ('spatial_ref_sys') order by table_schema, table_name; 
  LOOP
  BEGIN
  	FETCH curs_table into rec_table;
  	if found then
		v_schema = rec_table.table_schema;
        v_table = rec_table.table_name;
        execute 'select count(*) from ' || v_schema || '.' || quote_ident(v_table) ||';' into nb_enreg;
		
        return next;
	ELSE
    	EXIT;  
    end if;
    EXCEPTION  
    WHEN OTHERS THEN
    	raise notice 'Erreur boucle : schema : %, table : %',  v_schema,v_table;
  END;
  end loop;
  
    
EXCEPTION
WHEN OTHERS THEN
	raise notice 'Erreur generale : % - schema : %, table : %',  sqlerrm,v_schema,v_table;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
PARALLEL UNSAFE
COST 100 ROWS 1000;

COMMENT ON FUNCTION public.ps_inventaire(out v_schema varchar, out v_table varchar, out nb_enreg bigint)
IS 'Fonction permettant de renvoyer le nombre de lignes de chaque table excepté pour les schémas pg*, ''information_schema'' et ''topology''. La table spatial_ref_sys est également exclue de l''inventare.';
ALTER FUNCTION public.ps_inventaire (out v_schema varchar, out v_table varchar, out nb_enreg bigint)
  OWNER TO postgres;
 
/*==============================================================*/
/* Vue : application.v_volumetrie_brute                         */
/*                                                              */
/*==============================================================*/ 
-- *** Volumétrie brute *** :
--     L'ajout d'une vue permettant d'afficher la volumétrie des tables peut trouver sa place dans les données
--     descriptives que sert l'application basée sur les formulaires générique.
	CREATE VIEW application.v_volumetrie_brute (
		"table",
		"nb_lines")
	AS
	SELECT ps_inventaire.v_table AS "table",
		ps_inventaire.nb_enreg AS "nb_lines"
	FROM ps_inventaire() ps_inventaire(v_schema, v_table, nb_enreg)
	WHERE ps_inventaire.v_schema::text = 'public'::text AND ps_inventaire.v_table::text <> 'pgmfavorites'::text;

	COMMENT ON VIEW application.v_volumetrie_brute IS 'Vue utilisée par l''application basée sur le modèle ecological_db permettant d''afficher le nombre de lignes des différentes tables.';

	GRANT SELECT ON application.v_volumetrie_brute TO :grpreading;
	GRANT ALL ON application.v_volumetrie_brute TO :grpwriting;

	ALTER VIEW application.v_volumetrie_brute  OWNER TO :grpapplication;

-- *** Métadonnées de zone *** :
--     Le modèle ecological_db comporte une vue permettant d'extraire les métadonnées de zone en incluant les
--     métadonnées des parents (au sens du champ zon_tree de type ltree). Cet export brut peut-être carossé par
--     une vue qui sélectionnera certains champs, n'affichant que des données utiles et rendant la lecture plus
--     adaptée à une présentation dans les vues statistiques d'une application basée sur le modèle ecological_db
--     Sous réserve de la présence du schéma application et de la vue metadonnees_zone le code 
--     ci-dessous va créer une vue qui pourra être utilisée par le formulaire stats.global.php
	CREATE VIEW application.v_metadonnees_zone_appli (
		hierarchie,
		hierarchie_parent,
		inherited,
		date_measure,
		variable_name,
		code,
		value,
		unit,
		unit_symbol,
		protocol,
		id_zone,
		id_zone_parent)
	AS
	SELECT v_metadonnees_zone.ltree_child AS hierarchie,
			CASE
				WHEN v_metadonnees_zone.inherited THEN v_metadonnees_zone.ltree_parent
				ELSE NULL::ltree
			END AS hierarchie_parent,
		v_metadonnees_zone.inherited AS inherited,
		v_metadonnees_zone.date_measure,
		v_metadonnees_zone.var_name AS variable_name,
		v_metadonnees_zone.var_code AS code,
		v_metadonnees_zone.value,
		v_metadonnees_zone.uni_name AS unite,
		v_metadonnees_zone.uni_symbol AS unit_symbol,
		v_metadonnees_zone.prot_description AS protocol,
		v_metadonnees_zone.id_child AS id_zone,
			CASE
				WHEN v_metadonnees_zone.id_parent <> v_metadonnees_zone.id_child THEN v_metadonnees_zone.id_parent
				ELSE NULL::integer
			END AS id_zone_parent
	FROM v_metadata_zone as v_metadonnees_zone
	ORDER BY v_metadonnees_zone.ltree_child, v_metadonnees_zone.inherited, v_metadonnees_zone.ltree_parent;

	GRANT SELECT ON application.v_metadonnees_zone_appli TO :grpreading;
	GRANT ALL ON application.v_metadonnees_zone_appli TO :grpwriting;

	COMMENT ON VIEW application.v_metadonnees_zone_appli IS 'vue permettant un affichage travaillé des métadonnées de zones d''étude.';

	ALTER VIEW application.v_metadonnees_zone_appli  OWNER TO :grpapplication;
COMMIT