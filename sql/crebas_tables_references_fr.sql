---------------------------------------------------------------------------------
-- Auteur :		Alain Benard
-- Date création :	30/05/2018
-- dernière modificcation	:	20/05/2022
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour créer la structure (Tables, contraintes, index ...). 
--					
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer sur une base précedemment créée à l'aide du script suivant :
--				https://git.renater.fr/anonscm/gitweb?p=script/script.git;a=blob_plain;f=serveurs+virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des index et contraintes ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f crebas_tables_references_fr.sql nombase
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 			
-- MODIFICATION :
\set proprietaire 'ecological_db'
\set grpconnect 'ecological_db_connect'
\set grpecriture 'ecological_db_ecriture'
\set grplecture 'ecological_db_lecture'
\set grpapplication 'ecological_db_application'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

START TRANSACTION;
-- ******************************************************************************************
-- ***** Gestion des droits par défauts lors de la création de nouveaux objets postgres *****
-- ******************************************************************************************

	alter default privileges
		in schema public
		grant all 
		on tables
		to :grpecriture;
	
	alter default privileges
		in schema public
		grant select
		on tables
		to :grplecture;
		
	alter default privileges
		in schema public
		grant all
		on sequences
		to :proprietaire, :grpecriture with grant option;

create extension if not exists ltree;

-- ****************************************************************************
-- ***		Tables du schéma générique de mesures / regroupements			***
-- ****************************************************************************

/*==============================================================*/
/* Table : public.classement                                    */
/*==============================================================*/

create table tr_classement_clas (
   clas_id              serial			not null,
   clas_tree       		public.ltree	not null,
   clas_libelle         varchar(32)		not null,
   clas_definition      varchar			not null,
   constraint tr_classement_clas_pkey primary key (clas_id),
   constraint c_uni_tree_clas unique(clas_tree)
);

comment on table tr_classement_clas is 'Table des plans de classement. Informations relatives aux classement des variables permettant de les positionner dans un ou plusieurs référentiels.';
comment on column tr_classement_clas.clas_id is 'Identifiant automatique numérique du classement';
comment on column tr_classement_clas.clas_tree is 'Arborescence unique complète du classement, chaque noeud étant séparé par le caractère ''.''';
comment on column tr_classement_clas.clas_libelle is 'Libellé usuel du classement';
comment on column tr_classement_clas.clas_definition is 'Définition du classement';
comment on constraint c_uni_tree_clas on public.tr_classement_clas is 'L''arborescence ltree du classement est unique';

alter table public.tr_classement_clas owner to :proprietaire;


/*==============================================================*/
/* Table : public.variable                                      */
/*==============================================================*/
create table tr_variable_var (
   var_id               serial               not null,
   var_code             varchar(25)          not null,
   var_nom              varchar(50)          not null,
   var_description      varchar(255)         not null,
   var_type_donnees     varchar(25)          null,
   var_controle         varchar(128)         null,
   constraint tr_variable_var_pkey primary key (var_id),
   constraint c_uni_code_var unique(var_code)
);

comment on table tr_variable_var is 'Table des variable mesurées, observées ou calculées';
comment on column tr_variable_var.var_id is 'Identifiant unique numérique de la variable';
comment on column tr_variable_var.var_code is 'Code unique de la variable';
comment on column tr_variable_var.var_nom is 'Nom usuel de la variable';
comment on column tr_variable_var.var_description is 'Description de la variable';
comment on column tr_variable_var.var_type_donnees is 'Type de données des mesures permettant des contrôles.';
comment on column tr_variable_var.var_controle is 'Chaîne de caractère réglementée permettant d''effectuer des contrôles sur les valeurs de mesure en association avec l''information mes_type.';
comment on constraint c_uni_code_var on public.tr_variable_var is 'Le code d''une variable doit être unique';

alter table public.tr_variable_var owner to :proprietaire;

/*==============================================================*/
/* Table : public.tj_classervar_cvar                            */
/*==============================================================*/
create table tj_classervar_cvar (
   cvar_var_id          integer              not null,
   cvar_clas_id         integer              not null,
   constraint tj_classervar_cvar_pkey primary key (cvar_var_id, cvar_clas_id),
   constraint c_fk_var_cvar foreign key (cvar_var_id)
		references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_clas_cvar foreign key (cvar_clas_id)
		references public.tr_classement_clas(clas_id)
		on delete no action
		on update no action
		not deferrable
		
);

comment on table tj_classervar_cvar is 'Table de jointure associant une variable à un classement, une variable pouvant être multiclassée';
comment on column tj_classervar_cvar.cvar_var_id is 'Identifiant unique numérique de la variable';
comment on column tj_classervar_cvar.cvar_clas_id is 'Identifiant automatique numérique du classement';
comment on constraint c_fk_var_cvar on public.tj_classervar_cvar is 'Le champ cvar_var_id correspond obligatoirement à variable existante';
comment on constraint c_fk_clas_cvar on public.tj_classervar_cvar is 'Le champ cvar_clas_id correspond obligatoirement à un classement existant';

alter table public.tj_classervar_cvar owner to :proprietaire;

-- Index pour les clés etrangères
create index x_btr_var_cvar on public.tj_classervar_cvar
  using btree (cvar_var_id);
comment on index public.x_btr_var_cvar is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_clas_cvar on public.tj_classervar_cvar
  using btree (cvar_clas_id);
comment on index public.x_btr_clas_cvar is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : public.unite                                         */
/*==============================================================*/
create table tr_unite_uni (
   uni_id               serial				not null,
   uni_symbole          varchar(15)			not null,
   uni_nom              varchar(50)			not null,
   uni_description      varchar(255)		not null,
   uni_type_donnee      varchar(25)			null,
   constraint tr_unite_uni_pkey primary key (uni_id),
   constraint c_uni_symbole_uni unique(uni_symbole),
   constraint c_uni_nom_uni unique(uni_nom)
);

comment on table tr_unite_uni is 'Table des unités de mesure.';
comment on column tr_unite_uni.uni_id is 'Identifiant unique numérique de l''unité de mesure';
comment on column tr_unite_uni.uni_symbole is 'Symbole utilisé pour représenter l''unité de mesure (par exemple m pour mètre)';
comment on column tr_unite_uni.uni_nom is 'Nom usuel de l''unité de mesure';
comment on column tr_unite_uni.uni_description is 'Description de l''unité de mesure';
comment on column tr_unite_uni.uni_type_donnee is 'Type de données courant dans lequel les données exprimées dans l''unité de mesure seront fournies. Par exemple numeric pour des mesurées exprimées en mètres. Cette information optionnelle permet d''implémenter des contrôles de cohérence.';
comment on constraint c_uni_symbole_uni on public.tr_unite_uni is 'Le symbole d''une unité doit être unique';
comment on constraint c_uni_nom_uni on public.tr_unite_uni is 'Le nom d''une unité doit être unique';

alter table public.tr_unite_uni owner to :proprietaire;

/*==============================================================*/
/* Table : public.norme                                         */
/*==============================================================*/
create table tr_norme_norm (
   norm_id              serial               not null,
   norm_nom             varchar(50)          not null,
   norm_date            date                 null,
   norm_url             varchar(255)         null,
   norm_version         varchar(10)          not null,
   constraint tr_norme_norm_pkey primary key (norm_id),
   constraint c_uni_nom_version_norm unique(norm_nom,norm_version)
);

comment on table tr_norme_norm is 'Table contenant les normes référencées dans la base';
comment on column tr_norme_norm.norm_id is 'Identifiant numérique automatique de la norme';
comment on column tr_norme_norm.norm_nom is 'Nom usuel de la norme';
comment on column tr_norme_norm.norm_date is 'Date de parution de la norme';
comment on column tr_norme_norm.norm_url is 'Url d''accès au document de la norme';
comment on column tr_norme_norm.norm_version is 'Information de version de la norme';
comment on constraint c_uni_nom_version_norm on public.tr_norme_norm is 'Le couple nom / version d''une norme doit être unique';

alter table public.tr_norme_norm owner to :proprietaire;

/*==============================================================*/
/* Table : public.protocole                                     */
/*==============================================================*/
create table tr_protocole_prot (
   prot_id              serial               not null,
   prot_nom             varchar(50)          not null,
   prot_description		varchar(255)         not null,
   prot_url             varchar(255)         null,
   constraint tr_protocole_prot_pkey primary key (prot_id),
   constraint c_uni_nom_prot unique(prot_nom)
);

comment on table tr_protocole_prot is 'Table  des protocoles référencés dans la base';
comment on column tr_protocole_prot.prot_id is 'Identifiant unique numérique du protocole';
comment on column tr_protocole_prot.prot_nom is 'Nom usuel unique du protocole';
comment on column tr_protocole_prot.prot_description is 'Description succincte du protocole';
comment on column tr_protocole_prot.prot_url is 'url d''accès au document protocole';
comment on constraint c_uni_nom_prot on public.tr_protocole_prot is 'Le nom du protocole doit être unique';

alter table public.tr_protocole_prot owner to :proprietaire;

/*==============================================================*/
/* Table : public.type_mesure                                   */
/*==============================================================*/
create table tr_type_mesure_typm (
   typm_id              serial               not null,
   typm_code            varchar(16)          not null,
   typm_description     varchar(128)         not null,
   constraint tr_type_mesure_typm_pkey primary key (typm_id),
   constraint c_uni_code_typm unique(typm_code)
);

comment on table tr_type_mesure_typm is 'Table des type de mesure (calculé, mesuré...)';
comment on column tr_type_mesure_typm.typm_id is 'Identifiant automatique numérique du type de mesure';
comment on column tr_type_mesure_typm.typm_code is 'Code du type de mesure';
comment on column tr_type_mesure_typm.typm_description is 'Description complète du type de mesure';
comment on constraint c_uni_code_typm on public.tr_type_mesure_typm is 'Le code du type de mesure doit être unique';

alter table public.tr_type_mesure_typm owner to :proprietaire;

/*==============================================================*/
/* Table : public.operateur                                     */
/*==============================================================*/
create table tr_operateur_ope (
   ope_id               serial               not null,
   ope_nom              varchar(50)          not null,
   ope_prenom           varchar(50)          not null,
   ope_mail             varchar(120)         null,
   ope_employeur		varchar(64)			 null,
   constraint tr_operateur_ope_pkey primary key (ope_id),
   constraint c_uni_nom_prenom_ope unique(ope_nom,ope_prenom)
);

comment on table tr_operateur_ope is 'Table des opérateurs';
comment on column tr_operateur_ope.ope_id is 'Identifiant automatique numérique de l''opérateur';
comment on column tr_operateur_ope.ope_nom is 'Nom de l''opérateur';
comment on column tr_operateur_ope.ope_prenom is 'Prénom de l''opérateur';
comment on column tr_operateur_ope.ope_mail is 'Adresse électronique de l''opérateur';
comment on column tr_operateur_ope.ope_employeur is 'Employeur (forme libre) de l''opérateur';
comment on constraint c_uni_nom_prenom_ope on public.tr_operateur_ope is 'Le couple nom / prénom d''un opérateur doit être unique';

alter table public.tr_operateur_ope owner to :proprietaire;

/*==============================================================*/
/* Table : public.role                                          */
/*==============================================================*/
create table tr_role_rol (
   rol_id               serial               not null,
   rol_libelle          varchar(32)          not null,
   rol_description      text                 not null,
   constraint tr_role_rol_pkey primary key (rol_id),
   constraint c_uni_libelle_rol unique(rol_libelle)
);

comment on table tr_role_rol is 'Table des rôles que peuvent tenir des personnes';
comment on column tr_role_rol.rol_id is 'Identifiant unique automatique du rôle';
comment on column tr_role_rol.rol_libelle is 'Libellé unique du rôle';
comment on column tr_role_rol.rol_description is 'Descritpion complète du rôle';
comment on constraint c_uni_libelle_rol on public.tr_role_rol is 'Le libellé d''un role doit être unique';

alter table public.tr_role_rol owner to :proprietaire;

/*==============================================================*/
/* Table : public.fichier                                       */
/*==============================================================*/
create table tr_fichier_fic (
   fic_id               serial               not null,
   fic_nom              varchar(120)         not null,
   fic_url              varchar(255)         not null,
   constraint tr_fichier_fic_pkey primary key (fic_id),
   constraint c_uni_nom_url_fic unique(fic_nom,fic_url)
);

comment on table tr_fichier_fic is 'Table des fichiers de mesures';
comment on column tr_fichier_fic.fic_id is 'Identifiant automatique numérique du fichier';
comment on column tr_fichier_fic.fic_nom is 'Nom du fichier';
comment on column tr_fichier_fic.fic_url is 'url d''accès au fichier';
comment on constraint c_uni_nom_url_fic on public.tr_fichier_fic is 'Le couple nom / url d''un fichier doit être unique';

alter table public.tr_fichier_fic owner to :proprietaire;

/*==============================================================*/
/* Table : public.regroupement                                  */
/*==============================================================*/
create table t_regroupement_rgp (
   rgp_id              serial				not null,
   rgp_typm_id         integer				null,
-- rgp_var_id          integer				not null, Seulement si l'on prend l'option d'associer un regroupement à une et une seule variable
   rgp_fic_id          integer				null,
   rgp_prot_id         integer				null,
   rgp_norm_id         integer				null,
   rgp_uni_id          integer				null,
   rgp_code            varchar(32)          not null,
   rgp_nom             varchar(32)          not null,
   rgp_description     text                 not null,
   constraint t_regroupement_rgp_pkey primary key (rgp_id),
   constraint c_uni_code_rgp unique(rgp_code),
   constraint c_fk_typm_rgp foreign key (rgp_typm_id)
		references public.tr_type_mesure_typm(typm_id)
		on delete no action
		on update no action
		not deferrable,
-- constraint c_fk_var_rgp foreign key (rgp_var_id)
--	references public.tr_variable_var(var_id)
--	on delete no action
--	on update no action
--	not deferrable, 
   constraint c_fk_fic_rgp foreign key (rgp_fic_id)
		references public.tr_fichier_fic(fic_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_prot_rgp foreign key (rgp_prot_id)
		references public.tr_protocole_prot(prot_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_norm_rgp foreign key (rgp_norm_id)
		references public.tr_norme_norm(norm_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_uni_rgp foreign key (rgp_uni_id)
		references public.tr_unite_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);

comment on table t_regroupement_rgp is 'Table des ensembles regroupant des mesures en vue de factoriser l''environnement de ces mesures (variable, unité, norme, protocole, fichier, opérateur(s)). Les informations satellites sont alors associées au regroupement de mesure et non plus à chaque mesure individuelle.';
comment on column t_regroupement_rgp.rgp_id is 'Identifiant automatique numérique du regroupement';
comment on column t_regroupement_rgp.rgp_typm_id is 'Identifiant automatique numérique du type de mesure associé';
--comment on column t_regroupement_rgp.rgp_var_id is 'Identifiant automatique numérique de la variable associée';
comment on column t_regroupement_rgp.rgp_fic_id is 'Identifiant automatique numérique du fichier associé';
comment on column t_regroupement_rgp.rgp_prot_id is 'Identifiant automatique numérique du protocole associé';
comment on column t_regroupement_rgp.rgp_norm_id is 'Identifiant numérique automatique de la norme associée';
comment on column t_regroupement_rgp.rgp_uni_id is 'Identifiant unique numérique de l''unité de mesure associée';
comment on column t_regroupement_rgp.rgp_code is 'Code du regroupement permettant de l''identifier pour un opérateur humain';
comment on column t_regroupement_rgp.rgp_nom is 'Nom du regroupement';
comment on column t_regroupement_rgp.rgp_description is 'Description du regroupement';
comment on constraint c_uni_code_rgp on public.t_regroupement_rgp is 'Le code d''un regroupement doit être unique';
comment on constraint c_fk_typm_rgp on public.t_regroupement_rgp is 'un regroupement peut être rattaché à O ou 1 type de mesure';
-- comment on constraint c_fk_var_rgp on public.t_regroupement_rgp is 'un regroupement doit être rattaché à une et une seule variable';
comment on constraint c_fk_fic_rgp on public.t_regroupement_rgp is 'un regroupement peut être rattaché à O ou 1 fichier';
comment on constraint c_fk_prot_rgp on public.t_regroupement_rgp is 'un regroupement peut être rattaché à O ou 1 protocole';
comment on constraint c_fk_norm_rgp on public.t_regroupement_rgp is 'un regroupement peut être rattaché à O ou 1 norme';
comment on constraint c_fk_uni_rgp on public.t_regroupement_rgp is 'un regroupement peut être rattaché à O ou 1 unité';

alter table public.t_regroupement_rgp owner to :proprietaire;

-- Index pour les clés etrangères
create index x_btr_fkey_typm_rgp on public.t_regroupement_rgp
  using btree (rgp_typm_id);
comment on index public.x_btr_fkey_typm_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

--create index x_btr_fkey_var_rgp on public.t_regroupement_rgp
--  using btree (rgp_var_id);
--comment on index public.x_btr_fkey_var_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_fic_rgp on public.t_regroupement_rgp
  using btree (rgp_fic_id);
comment on index public.x_btr_fkey_fic_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_prot_rgp on public.t_regroupement_rgp
  using btree (rgp_prot_id);
comment on index public.x_btr_fkey_prot_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_norm_rgp on public.t_regroupement_rgp
  using btree (rgp_norm_id);
comment on index public.x_btr_fkey_norm_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_fkey_uni_rgp on public.t_regroupement_rgp
  using btree (rgp_uni_id);
comment on index public.x_btr_fkey_uni_rgp is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : public.tj_rol_ope_rgp_ror                           */
/*			Table associant un regroupemement à un opérateur    */
/*			et un rôle										    */
/*==============================================================*/
create table tj_rol_ope_rgp_ror (
   ror_rgp_id           integer              not null,
   ror_ope_id           integer              not null,
   ror_rol_id			integer				 not null,
   constraint tj_rol_ope_rgp_ror_pkey primary key (ror_rgp_id, ror_ope_id,ror_rol_id),
   constraint c_fk_rgp_ror foreign key (ror_rgp_id)
		references public.t_regroupement_rgp(rgp_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_ror foreign key (ror_ope_id)
		references public.tr_operateur_ope(ope_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rol_ror foreign key (ror_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable
		
);

comment on table tj_rol_ope_rgp_ror is 'Table de jointure associant un regroupement à un opérateur et à un rôle pour cet opérateur';
comment on column tj_rol_ope_rgp_ror.ror_rgp_id is 'Identifiant unique numérique du regroupement associé';
comment on column tj_rol_ope_rgp_ror.ror_ope_id is 'Identifiant automatique numérique de l''opérateur associé';
comment on column tj_rol_ope_rgp_ror.ror_rol_id is 'Identifiant automatique numérique du rôle associé';
comment on constraint c_fk_rgp_ror on public.tj_rol_ope_rgp_ror is 'Le champ ror_rgp_id correspond obligatoirement à un regroupement existant';
comment on constraint c_fk_ope_ror on public.tj_rol_ope_rgp_ror is 'Le champ ror_ope_id correspond obligatoirement à un opérateur existant';
comment on constraint c_fk_rol_ror on public.tj_rol_ope_rgp_ror is 'Le champ ror_rol_id correspond obligatoirement à un rôle existant';

alter table public.tj_rol_ope_rgp_ror owner to :proprietaire;

-- Index pour les clés etrangères
create index x_btr_rgp_ror on public.tj_rol_ope_rgp_ror
  using btree (ror_rgp_id);
comment on index public.x_btr_rgp_ror is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_ope_ror on public.tj_rol_ope_rgp_ror
  using btree (ror_ope_id);
comment on index public.x_btr_ope_ror is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

create index x_btr_rol_ror on public.tj_rol_ope_rgp_ror
  using btree (ror_rol_id);
comment on index public.x_btr_rol_ror is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : public.type_zone_etude                               */
/*==============================================================*/
create table tr_type_zone_etude_typz (
   typz_id              serial               not null,
   typz_code	        varchar(32)          not null,
   typz_description		varchar(128),
   constraint tr_type_zone_etude_typz_pkey primary key (typz_id),
   constraint c_uni_code_typz unique(typz_code)
);

comment on table tr_type_zone_etude_typz is 'Table des types de zone d''étude permettant de caractériser la typologie des zones d''étude (parcelle, localisation, foret ...)';
comment on column tr_type_zone_etude_typz.typz_id is 'identifiant automatique numérique du type de zone d''étude';
comment on column tr_type_zone_etude_typz.typz_code is 'Nom unique du type de zone d''étude';
comment on column tr_type_zone_etude_typz.typz_description is 'Description du type de zone d''étude';
comment on constraint c_uni_code_typz on public.tr_type_zone_etude_typz is 'Le code d''un type de zone d''étude doit être unique';

INSERT INTO public.tr_type_zone_etude_typz ("typz_code") VALUES ('FORET');

/*==============================================================*/
/* Table : public.zone_etude                                    */
/*==============================================================*/
create table tr_zone_etude_zon (
   zon_id               serial               not null,
   zon_typz_id          integer              not null,
   zon_code				varchar(64)			 not null,
   zon_libelle          varchar(96)          not null,
   zon_tree 			public.ltree		 not null,   
   constraint tr_zone_etude_zon_pkey primary key (zon_id),
   constraint c_uni_tree_zon unique(zon_tree),
   constraint c_fk_typz_zon foreign key (zon_typz_id)
    	references public.tr_type_zone_etude_typz(typz_id)
		on delete no action
		on update no action
		not deferrable
);

comment on table tr_zone_etude_zon is 'Table des zones d''étude représentant une entité géographique de niveau variable et pouvant être imbriquées.';
comment on column tr_zone_etude_zon.zon_id is 'identifiant automatique numérique de la zone d''étude';
comment on column tr_zone_etude_zon.zon_typz_id is 'identifiant automatique numérique du type de zone d''étude';
comment on column tr_zone_etude_zon.zon_code is 'Code de la zone d''étude';
comment on column tr_zone_etude_zon.zon_libelle is 'Libellé de la zone d''étude';
comment on column tr_zone_etude_zon.zon_tree is 'Arborescence unique complète de la zone, chaque noeud étant séparé par le caractère ''.''';
comment on constraint c_fk_typz_zon on public.tr_zone_etude_zon is 'une zone d''étude doit être rattachée à 1 et 1 seul type de zone d''étude';
comment on constraint c_uni_tree_zon on public.tr_zone_etude_zon is 'L''arborescence complète d''une zone d''étude doit être unique';

-- Index gist sur l'objet de type ltree
create index x_gist_tree_zon on public.tr_zone_etude_zon
  using gist(zon_tree);
comment on index public.x_gist_tree_zon is 'Index d''optimisation de parcours d''arbre sur l''objet tree de type ltree';
  
-- Index pour les clés etrangères

create index x_btr_fkey_typz_zon on public.tr_zone_etude_zon
  using btree (zon_typz_id);
comment on index public.x_btr_fkey_typz_zon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : public.referentiel_taxonomique                       */
/*==============================================================*/

create table tr_referentiel_taxonomique_ref (
  ref_id                serial               not null,
  ref_libelle 			varchar(128) 		 not null,
  ref_version 			varchar(16) 		 not null,
  ref_code 				varchar(16)			 not null,
  ref_commentaire 		varchar(255)		null,
  constraint tr_referentiel_taxonomique_ref_pkey primary key(ref_id),
  constraint c_uni_code_ref unique(ref_code),
  constraint c_uni_lib_ver_ref unique(ref_libelle,ref_version)
); 

comment on table public.tr_referentiel_taxonomique_ref is 'Table des référentiels taxonomiques';
comment on column public.tr_referentiel_taxonomique_ref.ref_id is 'Identifiant automatique numérique du référentiel';
comment on column public.tr_referentiel_taxonomique_ref.ref_libelle is 'Nom du référentiel';
comment on column public.tr_referentiel_taxonomique_ref.ref_version is 'Version du référentiel';
comment on column public.tr_referentiel_taxonomique_ref.ref_code is 'Code unique permettant d''identifier le référentiel (hors versions)';
comment on column public.tr_referentiel_taxonomique_ref.ref_commentaire is 'Commentaire libre sur le référentiel.';
comment on constraint c_uni_code_ref on public.tr_referentiel_taxonomique_ref is 'Le code du référentiel est unique';
comment on constraint c_uni_lib_ver_ref on public.tr_referentiel_taxonomique_ref is 'Le couple libellé / version du référentiel est unique';

INSERT INTO public.tr_referentiel_taxonomique_ref ("ref_libelle", "ref_version", "ref_code") VALUES ('TAXREF', 'V15','TAXREFV15');

/*==============================================================*/
/* Table : public.taxon                                         */
/*==============================================================*/
create table tr_taxon_tax (
   tax_id               serial               not null,
   tax_ref_id           integer              not null,
   tax_libelle          varchar(255)         not null,
   tax_identifiant_ext  varchar(64)			 not null,
   constraint tr_taxon_tax_pkey primary key (tax_id),
   constraint c_uni_ref_libelle_tax unique(tax_ref_id, tax_libelle),
   constraint c_uni_ref_identifiant_ext_tax unique(tax_ref_id, tax_identifiant_ext),
   constraint c_fk_ref_tax foreign key (tax_ref_id)
    	references public.tr_referentiel_taxonomique_ref(ref_id)
		on delete no action
		on update no action
		not deferrable   
);

comment on table tr_taxon_tax is 'Table des taxons (espèces) végétales référencées dans la base';
comment on column tr_taxon_tax.tax_id is 'identifiant automatique numérique du taxon';
comment on column tr_taxon_tax.tax_ref_id is 'Identifiant automatique numérique du référentiel de rattachement';
comment on column tr_taxon_tax.tax_libelle is 'Nom courant unique du taxon dans son référentiel d''origine';
comment on column tr_taxon_tax.tax_identifiant_ext is 'Identifiant dans la source d''information (référentiel d''origine). Par exemple le ''Champ CD_NOM d''une table taxref''. Ce champ doit permettre un suivi de l''évolution des référentiels dans le temps.';
comment on constraint c_uni_ref_libelle_tax on public.tr_taxon_tax is 'Le couple référentiel / libellé est unique';
comment on constraint c_uni_ref_identifiant_ext_tax on public.tr_taxon_tax is 'Le couple référentiel / identifiant extérieur est unique';
comment on constraint c_fk_ref_tax on public.tr_taxon_tax is 'Un taxon est rattaché à 1 et 1 seul référentiel';

-- Index pour les clés etrangères
create index x_btr_fkey_ref_tax on public.tr_taxon_tax
  using btree (tax_ref_id);
comment on index public.x_btr_fkey_ref_tax is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

--******************************************************************************************************************************************
--******************* Ensemble de 4 tables dédiées à la gestion des mesures sur des zones (métadonnées) et issues du modèle générique ******
--******************************************************************************************************************************************

/*====================================================================================*/
/* Table : public.t_mesure_mzon                                    					  */
/*====================================================================================*/
create table t_mesure_mzon (
   mzon_id               serial               not null,
   mzon_zon_id			 integer			  not null, 
   mzon_typm_id          integer              null,
   mzon_var_id           integer              not null,
   mzon_fic_id           integer              null,
   mzon_prot_id          integer              null,
   mzon_norm_id          integer              null,
   mzon_uni_id           integer              null,
   mzon_valeur_num       float8               null,
   mzon_valeur_text      varchar              null,
   mzon_valeur_date      date                 null,
   mzon_date_mesure      date                 null,
   mzon_precision_date_mesure float8          null,
   mzon_precision_mesure 	  float8          null,
   mzon_remarque         varchar              null,
   constraint t_mesure_mzon_pkey primary key (mzon_id),
   constraint c_uni_zon_var_datemesure_mzon UNIQUE(mzon_zon_id, mzon_var_id, mzon_date_mesure), -- Cette clé 'humaine' pourra être revisitée lors de la spécialisation pour les mesures de différents objets
   constraint c_fk_zon_mzon foreign key (mzon_zon_id)
		references public.tr_zone_etude_zon(zon_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_typm_mzon foreign key (mzon_typm_id)
		references public.tr_type_mesure_typm(typm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_var_mzon foreign key (mzon_var_id)
    	references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_fic_mzon foreign key (mzon_fic_id)
		references public.tr_fichier_fic(fic_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_prot_mzon foreign key (mzon_prot_id)
		references public.tr_protocole_prot(prot_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_norm_mzon foreign key (mzon_norm_id)
		references public.tr_norme_norm(norm_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_uni_mzon foreign key (mzon_uni_id)
		references public.tr_unite_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table t_mesure_mzon is 'Table des mesures de métadonnées pour les zones d''étude';
comment on column t_mesure_mzon.mzon_id is 'Identifiant automatique numérique de la mesure de métadonnées de la zone';
comment on column t_mesure_mzon.mzon_zon_id is 'Identifiant automatique numérique de la zone';
comment on column t_mesure_mzon.mzon_typm_id is 'Identifiant automatique numérique du type de mesure associé';
comment on column t_mesure_mzon.mzon_var_id is 'Identifiant unique numérique de la variable associée';
comment on column t_mesure_mzon.mzon_fic_id is 'Identifiant automatique numérique du fichier associé';
comment on column t_mesure_mzon.mzon_prot_id is 'Identifiant unique numérique du protocole associé';
comment on column t_mesure_mzon.mzon_norm_id is 'Identifiant numérique automatique de la norme associée';
comment on column t_mesure_mzon.mzon_uni_id is 'Identifiant unique numérique de l''unité de mesure associée';
comment on column t_mesure_mzon.mzon_valeur_num is 'valeur de la mesure si celle-ci est de type numérique';
comment on column t_mesure_mzon.mzon_valeur_text is 'valeur de la mesure si celle-ci est de type texte';
comment on column t_mesure_mzon.mzon_valeur_date is 'valeur de la mesure si celle-ci est de type date';
comment on column t_mesure_mzon.mzon_date_mesure is 'date de la mesure';
comment on column t_mesure_mzon.mzon_precision_date_mesure is 'Précision concernant la date de la mesure';
comment on column t_mesure_mzon.mzon_precision_mesure is 'Précision de la valeur de la mesure';
comment on column t_mesure_mzon.mzon_remarque is 'Information complémentaire associée à la mesure';
comment on constraint c_fk_zon_mzon on public.t_mesure_mzon is 'une mesure doit être rattachée à un et un seul objet';
comment on constraint c_fk_typm_mzon on public.t_mesure_mzon is 'une mesure peut être rattachée à O ou 1 type de mesure';
comment on constraint c_fk_var_mzon on public.t_mesure_mzon is 'une mesure doit être rattachée à une et une seule variable';
comment on constraint c_fk_fic_mzon on public.t_mesure_mzon is 'une mesure peut être rattachée à O ou 1 fichier';
comment on constraint c_fk_prot_mzon on public.t_mesure_mzon is 'une mesure peut être rattachée à O ou 1 protocole';
comment on constraint c_fk_norm_mzon on public.t_mesure_mzon is 'une mesure peut être rattachée à O ou 1 norme';
comment on constraint c_fk_uni_mzon on public.t_mesure_mzon is 'une mesure peut être rattachée à O ou 1 unité';

-- date_mesure est null
create unique index c_uni_zon_var_mzon on public.t_mesure_mzon
  using btree (mzon_zon_id, mzon_var_id)
  where (mzon_date_mesure is null);
comment on index public.c_uni_zon_var_mzon is 'le couple variable / zone est unique si le champs date de mesure est non renseigné.';

-- Index pour les clés etrangères
create index x_btr_fkey_zon_mzon on public.t_mesure_mzon
  using btree (mzon_zon_id);
comment on index public.x_btr_fkey_zon_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_typm_mzon on public.t_mesure_mzon
  using btree (mzon_typm_id);
comment on index public.x_btr_fkey_typm_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_var_mzon on public.t_mesure_mzon
  using btree (mzon_var_id);
comment on index public.x_btr_fkey_var_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_fic_mzon on public.t_mesure_mzon
  using btree (mzon_fic_id);
comment on index public.x_btr_fkey_fic_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_prot_mzon on public.t_mesure_mzon
  using btree (mzon_prot_id);
comment on index public.x_btr_fkey_prot_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_norm_mzon on public.t_mesure_mzon
  using btree (mzon_norm_id);
comment on index public.x_btr_fkey_norm_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_fkey_uni_mzon on public.t_mesure_mzon
  using btree (mzon_uni_id);
comment on index public.x_btr_fkey_uni_mzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

/*==============================================================*/
/* Table : public.tj_rol_ope_mzon_roz                          */
/*			Table associant une mesure d'objet à un opérateur   */
/*			et un rôle										    */
/*==============================================================*/
create table tj_rol_ope_mzon_roz (
   roz_mzon_id           integer              not null,
   roz_ope_id           integer              not null,
   roz_rol_id			integer				 not null,
   constraint tj_rol_ope_mzon_roz_pkey primary key (roz_mzon_id, roz_ope_id,roz_rol_id),
   constraint c_fk_mzon_roz foreign key (roz_mzon_id)
		references public.t_mesure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_roz foreign key (roz_ope_id)
		references public.tr_operateur_ope(ope_id)
		on delete no action
		on update no action
		not deferrable, 		
   constraint c_fk_rol_roz foreign key (roz_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_rol_ope_mzon_roz is 'Table de jointure associant une mesure à un opérateur et à un rôle pour cet opérateur';
comment on column tj_rol_ope_mzon_roz.roz_mzon_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_rol_ope_mzon_roz.roz_ope_id is 'Identifiant automatique numérique de l''opérateur associé';
comment on column tj_rol_ope_mzon_roz.roz_rol_id is 'Identifiant automatique numérique du rôle associé';
comment on constraint c_fk_mzon_roz on public.tj_rol_ope_mzon_roz is 'Le champ roz_mzon_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_ope_roz on public.tj_rol_ope_mzon_roz is 'Le champ roz_ope_id correspond obligatoirement à un opérateur existant';
comment on constraint c_fk_rol_roz on public.tj_rol_ope_mzon_roz is 'Le champ roz_rol_id correspond obligatoirement à un rôle existant';
-- Index pour les clés etrangères
create index x_btr_mzon_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_mzon_id);
comment on index public.x_btr_mzon_roz is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_ope_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_ope_id);
comment on index public.x_btr_ope_roz is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_rol_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_rol_id);
comment on index public.x_btr_rol_roz is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
/*=================================================================*/
/* Table : public.tj_rgp_mzon_gmz                                  */
/*			Table associant une mesure d'objet à un regroupement   */
/*=================================================================*/
create table tj_rgp_mzon_gmz (
   gmz_mzon_id          integer              not null,
   gmz_rgp_id           integer              not null,
   constraint tj_rgp_mzon_gmz_pkey primary key (gmz_mzon_id, gmz_rgp_id),
   constraint c_fk_mzon_gmz foreign key (gmz_mzon_id)
		references public.t_mesure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rgp_gmz foreign key (gmz_rgp_id)
		references public.t_regroupement_rgp(rgp_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_rgp_mzon_gmz is 'Table de jointure associant une mesure à un regroupement.';
comment on column tj_rgp_mzon_gmz.gmz_mzon_id is 'Identifiant unique numérique de la mesure associée';
comment on column tj_rgp_mzon_gmz.gmz_rgp_id is 'Identifiant automatique numérique du regroupement associé';
comment on constraint c_fk_mzon_gmz on public.tj_rgp_mzon_gmz is 'Le champ gmz_mzon_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_rgp_gmz on public.tj_rgp_mzon_gmz is 'Le champ gmz_rgp_id correspond obligatoirement à un regroupememnt existant';
-- Index pour les clés etrangères
create index x_btr_mzon_gmz on public.tj_rgp_mzon_gmz
  using btree (gmz_mzon_id);
comment on index public.x_btr_mzon_gmz is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_rgp_gmz on public.tj_rgp_mzon_gmz
  using btree (gmz_rgp_id);
comment on index public.x_btr_rgp_gmz is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
/*=================================================================*/
/* Table : public.tj_mzon_mzon_tzon                                */
/*			Table associant une mesure d'objet à une autre mesure  */
/*			d'objet, la seconde étant complémentaire (métadonnée)  */
/*			ou nécessaire à l'interprétation de la première		   */
/*=================================================================*/
create table tj_mzon_mzon_tzon (
   tzon_ref_mzon_id          integer              not null,
   tzon_meta_mzon_id         integer              not null,
   tzon_statut				 varchar(32)		  null,
   constraint tj_mzon_mzon_tzon_pkey primary key (tzon_ref_mzon_id, tzon_meta_mzon_id),
   constraint c_fk_ref_mzon_tzon foreign key (tzon_ref_mzon_id)
		references public.t_mesure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_meta_mzon_tzon foreign key (tzon_meta_mzon_id)
		references public.t_mesure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_mzon_mzon_tzon is 'Table de jointure associant une mesure à une autre mesure, la seconde étant complémentaire (métadonnée) ou nécessaire à l''interprétation de la première.';
comment on column tj_mzon_mzon_tzon.tzon_ref_mzon_id is 'Identifiant automatique numérique de la mesure référence';
comment on column tj_mzon_mzon_tzon.tzon_meta_mzon_id is 'Identifiant automatique numérique de la mesure considérée comme métadonnée de la mesure référence';
comment on column tj_mzon_mzon_tzon.tzon_statut is 'Indicateur précisant le statut de la métadonnée par rapport à la mesure de référence. Par exemple le caractère obligatoire ou recommandé pour l''interprétation.';
comment on constraint c_fk_ref_mzon_tzon on public.tj_mzon_mzon_tzon is 'Le champ tzon_ref_mzon_id correspond obligatoirement à une mesure existante';
comment on constraint c_fk_meta_mzon_tzon on public.tj_mzon_mzon_tzon is 'Le champ tzon_meta_mzon_id correspond obligatoirement à une mesure existante';
-- Index pour les clés etrangères
create index x_btr_ref_mzon_tzon on public.tj_mzon_mzon_tzon
  using btree (tzon_ref_mzon_id);
comment on index public.x_btr_ref_mzon_tzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    
create index x_btr_meta_mzon_tzon on public.tj_mzon_mzon_tzon
  using btree (tzon_meta_mzon_id);
comment on index public.x_btr_meta_mzon_tzon is 'Index de clé étrangère pour accélérer les mises à jour / suppression';    

--******************************************************************************************************************************************
--******************* 				Vues permettant d'exploiter plus facilement la structure de base. 				************************
--******************************************************************************************************************************************

/*=================================================================*/
/* Vue : public.v_metadonnees_zone                                 */
/*			Vue permettant d'afficher les mesures des zones et 	   */	
/*			celles de leurs parents (arborescence complète) 	   */
/*=================================================================*/

create or replace view public.v_metadonnees_zone(
    id_enfant,
    ltree_enfant,
    id_parent,
    ltree_parent,
    date_mesure,
    var_nom,
    var_code,
    valeur,
    prot_description,
    uni_nom,
    uni_symbole,
	est_heritee)
as
with full_zone as(
  select zon1.zon_id as id_enfant,
         zon1.zon_tree as ltree_enfant,
         zon2.zon_id as id_parent,
         zon2.zon_tree as ltree_parent
  from tr_zone_etude_zon zon1
       join tr_zone_etude_zon zon2 on zon2.zon_tree @> zon1.zon_tree
  order by zon1.zon_tree,
           zon2.zon_tree)
    select full_zone.id_enfant,
           full_zone.ltree_enfant,
           full_zone.id_parent,
           full_zone.ltree_parent,
           t_mesure_mzon.mzon_date_mesure as date_mesure,
           tr_variable_var.var_nom,
           tr_variable_var.var_code,
           coalesce(t_mesure_mzon.mzon_valeur_num::text,
             t_mesure_mzon.mzon_valeur_text::text,
             t_mesure_mzon.mzon_valeur_date::text) as valeur,
           tr_protocole_prot.prot_description,
           tr_unite_uni.uni_nom,
           tr_unite_uni.uni_symbole,
		   full_zone.id_parent <> full_zone.id_enfant AS est_heritee
    from full_zone
         join t_mesure_mzon on full_zone.id_parent = t_mesure_mzon.mzon_zon_id
         join tr_variable_var on t_mesure_mzon.mzon_var_id =
           tr_variable_var.var_id
         left join tr_unite_uni on t_mesure_mzon.mzon_uni_id =
           tr_unite_uni.uni_id
         left join tr_protocole_prot on t_mesure_mzon.mzon_prot_id =
           tr_protocole_prot.prot_id;
comment on view public.v_metadonnees_zone is 'Vue permettant l''affichage des mesures de zones (variables / métadonnées)';
		   
COMMIT;