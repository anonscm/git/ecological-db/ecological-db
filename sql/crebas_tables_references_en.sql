---------------------------------------------------------------------------------
-- Auteur :		Alain Benard
-- Creation date :	30/05/2018
-- Last update	:	20/05/2022
---------------------------------------------------------------------------------
-- Description / objectif : 	This SQL script performs a transaction on the current database
--                              - to create the structure (Tables, constraints, index ...). 
--					
--					The file of this script is encoded in UTF8 
-- Usage : 	*	This script should be run on a previously created database using the following scrip :
--				https://git.renater.fr/anonscm/gitweb?p=script/script.git;a=blob_plain;f=serveurs+virtuels/pggeodb/exploitation/creer_base.sh
--			* 	The account to use must have sufficient privileges to create tables, indexes and constraints ...
--			*	The use of an interpolated psql variable in the sql (\ set) code only works with the psql tool that should be used
--          *   to run this script (otherwise the script must be modified beforehand to comply with the Classic SQL.
--			* SYNTAXE : psql -f crebas_tables_references_en.sql nombase
-- PARAMETERS :
--		entrance :
-- 			nothing
--		output :
-- 			
-- UPDATE :
\set owner 'ecological_db'
\set grpconnect 'ecological_db_connect'
\set grpwriting 'ecological_db_writing'
\set grpreading 'ecological_db_reading'
\set grpapplication 'ecological_db_application'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

START TRANSACTION;
-- ******************************************************************************************
-- ***** Management of default rights when creating new postgres objects                *****
-- ******************************************************************************************

	alter default privileges
		in schema public
		grant all 
		on tables
		to :grpwriting;
	
	alter default privileges
		in schema public
		grant select
		on tables
		to :grpreading;
		
	alter default privileges
		in schema public
		grant all
		on sequences
		to :owner, :grpwriting with grant option;

create extension if not exists ltree;

-- ****************************************************************************
-- ***		Tables of the generic diagram of measures / groupings			***
-- ****************************************************************************

/*==============================================================*/
/* Table : public.ranking                                       */
/*==============================================================*/

create table tr_ranking_rank (
   rank_id				serial			not null,
   rank_tree       		public.ltree	not null,
   rank_label	        varchar(32)		not null,
   rank_definition      varchar			not null,
   constraint tr_ranking_rank_pkey primary key (rank_id),
   constraint c_uni_tree_rank unique(rank_tree)
);

comment on table tr_ranking_rank is 'Table of classification plans. Information relating to the classification of variables allowing them to be positioned in one or more repositories.';
comment on column tr_ranking_rank.rank_id is 'Numeric automatic classification identifier';
comment on column tr_ranking_rank.rank_tree is 'Complete single tree structure of the class, each node being separated by the character ''.''';
comment on column tr_ranking_rank.rank_label is 'Usual classification label';
comment on column tr_ranking_rank.rank_definition is 'Definition of classification';
comment on constraint c_uni_tree_rank on public.tr_ranking_rank is 'The ranking ltree is unique';

alter table public.tr_ranking_rank owner to :owner;


/*==============================================================*/
/* Table : public.variable                                      */
/*==============================================================*/
create table tr_variable_var (
   var_id               serial               not null,
   var_code             varchar(25)          not null,
   var_name             varchar(50)          not null,
   var_description      varchar(255)         not null,
   var_data_type        varchar(25)          null,
   var_control          varchar(128)         null,
   constraint tr_variable_var_pkey primary key (var_id),
   constraint c_uni_code_var unique(var_code)
);

comment on table tr_variable_var is 'Table of measured, observed or calculated variables';
comment on column tr_variable_var.var_id is 'Unique numeric identifier of the variable';
comment on column tr_variable_var.var_code is 'Unique variable code';
comment on column tr_variable_var.var_name is 'Usual name of the variable';
comment on column tr_variable_var.var_description is 'Description of the variable';
comment on column tr_variable_var.var_data_type is 'Data type of measurements allowing controls.';
comment on column tr_variable_var.var_control is 'Regulated character string allowing checks to be carried out on the measurement values in association with the mes_type information.';
comment on constraint c_uni_code_var on public.tr_variable_var is 'The code of a variable must be unique';

alter table public.tr_variable_var owner to :owner;

/*==============================================================*/
/* Table : public.tj_classifyvar_cvar                           */
/*==============================================================*/
create table tj_classifyvar_cvar (
   cvar_var_id          integer              not null,
   cvar_rank_id         integer              not null,
   constraint tj_classifyvar_cvar_pkey primary key (cvar_var_id, cvar_rank_id),
   constraint c_fk_var_cvar foreign key (cvar_var_id)
		references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rank_cvar foreign key (cvar_rank_id)
		references public.tr_ranking_rank(rank_id)
		on delete no action
		on update no action
		not deferrable
		
);

comment on table tj_classifyvar_cvar is 'Join table associating a variable with a classification, a variable which can be multiclassed';
comment on column tj_classifyvar_cvar.cvar_var_id is 'Unique numeric identifier of the variable';
comment on column tj_classifyvar_cvar.cvar_rank_id is 'Numeric automatic classification identifier';
comment on constraint c_fk_var_cvar on public.tj_classifyvar_cvar is 'The cvar_var_id field must correspond to an existing variable';
comment on constraint c_fk_rank_cvar on public.tj_classifyvar_cvar is 'The cvar_rank_id field necessarily corresponds to an existing ranking';

alter table public.tj_classifyvar_cvar owner to :owner;

-- Index for foreign keys
create index x_btr_var_cvar on public.tj_classifyvar_cvar
  using btree (cvar_var_id);
comment on index public.x_btr_var_cvar is 'Foreign key index to speed up updates / deletion';    

create index x_btr_rank_cvar on public.tj_classifyvar_cvar
  using btree (cvar_rank_id);
comment on index public.x_btr_rank_cvar is 'Foreign key index to speed up updates / deletion';    

/*==============================================================*/
/* Table : public.unit                                          */
/*==============================================================*/
create table tr_unit_uni (
   uni_id               serial				not null,
   uni_symbol           varchar(15)			not null,
   uni_name             varchar(50)			not null,
   uni_description      varchar(255)		not null,
   uni_data_type        varchar(25)			null,
   constraint tr_unit_uni_pkey primary key (uni_id),
   constraint c_uni_symbol_uni unique(uni_symbol),
   constraint c_uni_name_uni unique(uni_name)
);

comment on table tr_unit_uni is 'Table of measurement units.';
comment on column tr_unit_uni.uni_id is 'Unique numeric identifier of the unit of measure';
comment on column tr_unit_uni.uni_symbol is 'Symbol used to represent unit of measure (e.g. m for meter)';
comment on column tr_unit_uni.uni_name is 'Common name of the unit of measurement';
comment on column tr_unit_uni.uni_description is 'Description of the unit of measure';
comment on column tr_unit_uni.uni_data_type is 'Current data type in which the data expressed in the measurement unit will be provided. For example numeric for measurements expressed in meters. This optional information is used to implement consistency checks.';
comment on constraint c_uni_symbol_uni on public.tr_unit_uni is 'The symbol of a unit must be unique';
comment on constraint c_uni_name_uni on public.tr_unit_uni is 'The name of a unit must be unique';

alter table public.tr_unit_uni owner to :owner;

/*==============================================================*/
/* Table : public.standard                                      */
/*==============================================================*/
create table tr_standard_std (
   std_id              serial               not null,
   std_name            varchar(50)          not null,
   std_date            date                 null,
   std_url             varchar(255)         null,
   std_version         varchar(10)          not null,
   constraint tr_standard_std_pkey primary key (std_id),
   constraint c_uni_name_version_std unique(std_name,std_version)
);

comment on table tr_standard_std is 'Table containing the standards referenced in the database';
comment on column tr_standard_std.std_id is 'Automatic numeric identifier of the standard';
comment on column tr_standard_std.std_name is 'Common name of the standard';
comment on column tr_standard_std.std_date is 'Date of publication of the standard';
comment on column tr_standard_std.std_url is 'Url for accessing the standard document';
comment on column tr_standard_std.std_version is 'Standard version information';
comment on constraint c_uni_name_version_std on public.tr_standard_std is 'The name / version pair of a standard must be unique';

alter table public.tr_standard_std owner to :owner;

/*==============================================================*/
/* Table : public.protocol                                      */
/*==============================================================*/
create table tr_protocol_prot (
   prot_id              serial               not null,
   prot_name            varchar(50)          not null,
   prot_description		varchar(255)         not null,
   prot_url             varchar(255)         null,
   constraint tr_protocol_prot_pkey primary key (prot_id),
   constraint c_uni_name_prot unique(prot_name)
);

comment on table tr_protocol_prot is 'Table of protocols referenced in the database';
comment on column tr_protocol_prot.prot_id is 'Unique numeric identifier of the protocol';
comment on column tr_protocol_prot.prot_name is 'Unique common name of the protocol';
comment on column tr_protocol_prot.prot_description is 'Brief description of the protocol';
comment on column tr_protocol_prot.prot_url is 'url for accessing the protocol document';
comment on constraint c_uni_name_prot on public.tr_protocol_prot is 'Protocol name must be unique';

alter table public.tr_protocol_prot owner to :owner;

/*==============================================================*/
/* Table : public.type_measure                                  */
/*==============================================================*/
create table tr_type_measure_tym (
   tym_id              serial               not null,
   tym_code            varchar(16)          not null,
   tym_description     varchar(128)         not null,
   constraint tr_type_measure_tym_pkey primary key (tym_id),
   constraint c_uni_code_tym unique(tym_code)
);

comment on table tr_type_measure_tym is 'Table of measurement types (calculated, measured...)';
comment on column tr_type_measure_tym.tym_id is 'Automatic numeric identifier of the type of measurement';
comment on column tr_type_measure_tym.tym_code is 'Measurement type code';
comment on column tr_type_measure_tym.tym_description is 'Complete description of the type of measurement';
comment on constraint c_uni_code_tym on public.tr_type_measure_tym is 'The measurement type code must be unique';

alter table public.tr_type_measure_tym owner to :owner;

/*==============================================================*/
/* Table : public.operator                                      */
/*==============================================================*/
create table tr_operator_ope (
   ope_id               serial               not null,
   ope_name             varchar(50)          not null,
   ope_first_name       varchar(50)          not null,
   ope_mail             varchar(120)         null,
   ope_employer			varchar(64)			 null,
   constraint tr_operator_ope_pkey primary key (ope_id),
   constraint c_uni_name_first_name_ope unique(ope_name,ope_first_name)
);

comment on table tr_operator_ope is 'Operator table';
comment on column tr_operator_ope.ope_id is 'Automatic digital operator identifier';
comment on column tr_operator_ope.ope_name is 'Operator name';
comment on column tr_operator_ope.ope_first_name is 'Operator''s first name';
comment on column tr_operator_ope.ope_mail is 'Operator email address';
comment on column tr_operator_ope.ope_employer is 'Employer of Operator';
comment on constraint c_uni_name_first_name_ope on public.tr_operator_ope is 'The name / first name pair of an operator must be unique';

alter table public.tr_operator_ope owner to :owner;

/*==============================================================*/
/* Table : public.role                                          */
/*==============================================================*/
create table tr_role_rol (
   rol_id               serial               not null,
   rol_label            varchar(32)          not null,
   rol_description      text                 not null,
   constraint tr_role_rol_pkey primary key (rol_id),
   constraint c_uni_label_rol unique(rol_label)
);

comment on table tr_role_rol is 'Table of roles that people can play';
comment on column tr_role_rol.rol_id is 'Automatic unique identifier of the role';
comment on column tr_role_rol.rol_label is 'Unique role label';
comment on column tr_role_rol.rol_description is 'Full role description';
comment on constraint c_uni_label_rol on public.tr_role_rol is 'The label of a role must be unique';

alter table public.tr_role_rol owner to :owner;

/*==============================================================*/
/* Table : public.file                                          */
/*==============================================================*/
create table tr_file_fil (
   fil_id               serial               not null,
   fil_name             varchar(120)         not null,
   fil_url              varchar(255)         not null,
   constraint tr_file_fil_pkey primary key (fil_id),
   constraint c_uni_name_url_fil unique(fil_name,fil_url)
);

comment on table tr_file_fil is 'Table of measurement files';
comment on column tr_file_fil.fil_id is 'Digital automatic file identifier';
comment on column tr_file_fil.fil_name is 'File name';
comment on column tr_file_fil.fil_url is 'url to access the file';
comment on constraint c_uni_name_url_fil on public.tr_file_fil is 'The name / url pair of a file must be unique';

alter table public.tr_file_fil owner to :owner;

/*==============================================================*/
/* Table : public.grouping                                      */
/*==============================================================*/
create table t_grouping_grp (
   grp_id              serial				not null,
   grp_tym_id          integer				null,
-- grp_var_id          integer				not null, Only if we take the option of associating a grouping with one and only one variable
   grp_fil_id          integer				null,
   grp_prot_id         integer				null,
   grp_std_id          integer				null,
   grp_uni_id          integer				null,
   grp_code            varchar(32)          not null,
   grp_name            varchar(32)          not null,
   grp_description     text                 not null,
   constraint t_grouping_grp_pkey primary key (grp_id),
   constraint c_uni_code_grp unique(grp_code),
   constraint c_fk_tym_grp foreign key (grp_tym_id)
		references public.tr_type_measure_tym(tym_id)
		on delete no action
		on update no action
		not deferrable,
-- constraint c_fk_var_grp foreign key (grp_var_id)
--	references public.tr_variable_var(var_id)
--	on delete no action
--	on update no action
--	not deferrable, 
   constraint c_fk_fil_grp foreign key (grp_fil_id)
		references public.tr_file_fil(fil_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_prot_grp foreign key (grp_prot_id)
		references public.tr_protocol_prot(prot_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_std_grp foreign key (grp_std_id)
		references public.tr_standard_std(std_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_uni_grp foreign key (grp_uni_id)
		references public.tr_unit_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);

comment on table t_grouping_grp is 'Table of sets regrouping measures in order to factorize the environment of these measures (variable, unit, standard, protocol, file, operator (s)). The satellite information is then associated with the measurement grouping and no longer with each individual measurement.';
comment on column t_grouping_grp.grp_id is 'Numerical automatic identifier of the grouping';
comment on column t_grouping_grp.grp_tym_id is 'Automatic numeric identifier of the associated measurement type';
--comment on column t_grouping_grp.grp_var_id is 'Automatic numeric identifier of the associated variable';
comment on column t_grouping_grp.grp_fil_id is 'Automatic numeric identifier of the associated file';
comment on column t_grouping_grp.grp_prot_id is 'Automatic numeric identifier of the associated protocol';
comment on column t_grouping_grp.grp_std_id is 'Automatic numeric identifier of the associated standard';
comment on column t_grouping_grp.grp_uni_id is 'Unique numeric identifier of the associated measurement unit';
comment on column t_grouping_grp.grp_code is 'Code of the group to identify it for a human operator';
comment on column t_grouping_grp.grp_name is 'Group name';
comment on column t_grouping_grp.grp_description is 'Description of the grouping';
comment on constraint c_uni_code_grp on public.t_grouping_grp is 'The code of a group must be unique';
comment on constraint c_fk_tym_grp on public.t_grouping_grp is 'a grouping can be attached to O or 1 type of measure';
-- comment on constraint c_fk_var_grp on public.t_grouping_grp is 'a grouping must be attached to one and only one variable';
comment on constraint c_fk_fil_grp on public.t_grouping_grp is 'a grouping can be attached to O or 1 file';
comment on constraint c_fk_prot_grp on public.t_grouping_grp is 'a grouping can be attached to O or 1 protocol';
comment on constraint c_fk_std_grp on public.t_grouping_grp is 'a grouping can be attached to O or 1 standard';
comment on constraint c_fk_uni_grp on public.t_grouping_grp is 'a grouping can be attached to O or 1 unit';

alter table public.t_grouping_grp owner to :owner;

-- Index for foreign keys
create index x_btr_fkey_tym_grp on public.t_grouping_grp
  using btree (grp_tym_id);
comment on index public.x_btr_fkey_tym_grp is 'Foreign key index to speed up updates / deletion';    

--create index x_btr_fkey_var_grp on public.t_grouping_grp
--  using btree (grp_var_id);
--comment on index public.x_btr_fkey_var_grp is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_fil_grp on public.t_grouping_grp
  using btree (grp_fil_id);
comment on index public.x_btr_fkey_fil_grp is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_prot_grp on public.t_grouping_grp
  using btree (grp_prot_id);
comment on index public.x_btr_fkey_prot_grp is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_std_grp on public.t_grouping_grp
  using btree (grp_std_id);
comment on index public.x_btr_fkey_std_grp is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_uni_grp on public.t_grouping_grp
  using btree (grp_uni_id);
comment on index public.x_btr_fkey_uni_grp is 'Foreign key index to speed up updates / deletion';    

/*==============================================================*/
/* Table : public.tj_rol_ope_grp_rog                           */
/*			Table associating a grouping with an operator		*/
/*			and a role											*/
/*==============================================================*/
create table tj_rol_ope_grp_rog (
   rog_grp_id           integer              not null,
   rog_ope_id           integer              not null,
   rog_rol_id			integer				 not null,
   constraint tj_rol_ope_grp_rog_pkey primary key (rog_grp_id, rog_ope_id,rog_rol_id),
   constraint c_fk_grp_rog foreign key (rog_grp_id)
		references public.t_grouping_grp(grp_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_rog foreign key (rog_ope_id)
		references public.tr_operator_ope(ope_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_rol_rog foreign key (rog_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable
		
);

comment on table tj_rol_ope_grp_rog is 'Join table associating a grouping with an operator and a role for this operator';
comment on column tj_rol_ope_grp_rog.rog_grp_id is 'Unique numeric identifier of the associated grouping';
comment on column tj_rol_ope_grp_rog.rog_ope_id is 'Numerical automatic identifier of the associated operator';
comment on column tj_rol_ope_grp_rog.rog_rol_id is 'Numeric automatic identifier of the associated role';
comment on constraint c_fk_grp_rog on public.tj_rol_ope_grp_rog is 'The rog_grp_id field necessarily corresponds to an existing grouping';
comment on constraint c_fk_ope_rog on public.tj_rol_ope_grp_rog is 'The rog_ope_id field must correspond to an existing operator';
comment on constraint c_fk_rol_rog on public.tj_rol_ope_grp_rog is 'The rog_rol_id field must correspond to an existing role';

alter table public.tj_rol_ope_grp_rog owner to :owner;

-- Index for foreign keys
create index x_btr_grp_rog on public.tj_rol_ope_grp_rog
  using btree (rog_grp_id);
comment on index public.x_btr_grp_rog is 'Foreign key index to speed up updates / deletion';    

create index x_btr_ope_rog on public.tj_rol_ope_grp_rog
  using btree (rog_ope_id);
comment on index public.x_btr_ope_rog is 'Foreign key index to speed up updates / deletion';    

create index x_btr_rol_rog on public.tj_rol_ope_grp_rog
  using btree (rog_rol_id);
comment on index public.x_btr_rol_rog is 'Foreign key index to speed up updates / deletion';    

/*==============================================================*/
/* Table : public.type_study_zone                               */
/*==============================================================*/
create table tr_type_study_zone_typz (
   typz_id			serial               not null,
   typz_code		varchar(32)          not null,
   typz_description	varchar(128),
   constraint type_study_typz_typz_pkey primary key (typz_id),
   constraint c_uni_code_typz unique(typz_code)
);

comment on table tr_type_study_zone_typz is 'Table of study zone types allowing to characterize the typology of study areas (plot, location, forest ...)';
comment on column tr_type_study_zone_typz.typz_id is 'automatic numeric identifier of the type of study zone';
comment on column tr_type_study_zone_typz.typz_code is 'Unique name of the study zone type';
comment on column tr_type_study_zone_typz.typz_description is 'Description of the type of study zone';
comment on constraint c_uni_code_typz on public.tr_type_study_zone_typz is 'The code for a study zone type must be unique';

INSERT INTO public.tr_type_study_zone_typz ("typz_code") VALUES ('FOREST');

/*==============================================================*/
/* Table : public.study_zone                                    */
/*==============================================================*/
create table tr_study_zone_zon (
   zon_id               serial               not null,
   zon_typz_id			integer              not null,
   zon_code				varchar(64)			 not null,
   zon_label            varchar(96)          not null,
   zon_tree 			public.ltree		 not null,   
   constraint tr_study_zone_zon_pkey primary key (zon_id),
   constraint c_uni_tree_zon unique(zon_tree),
   constraint c_fk_typz_zon foreign key (zon_typz_id)
    	references public.tr_type_study_zone_typz(typz_id)
		on delete no action
		on update no action
		not deferrable
);

comment on table tr_study_zone_zon is 'Table of study zones representing a geographic entity of variable level and that can be nested.';
comment on column tr_study_zone_zon.zon_id is 'automatic numeric identifier of the study zone';
comment on column tr_study_zone_zon.zon_typz_id is 'automatic numeric identifier of the type of study zone';
comment on column tr_study_zone_zon.zon_code is 'Study zone code';
comment on column tr_study_zone_zon.zon_label is 'Label of the study zone';
comment on column tr_study_zone_zon.zon_tree is 'Complete single tree structure of the zone, each node being separated by the character ''.''';
comment on constraint c_fk_typz_zon on public.tr_study_zone_zon is 'a study zone must be attached to 1 and 1 type of study zone';
comment on constraint c_uni_tree_zon on public.tr_study_zone_zon is 'The entire tree structure of a study zone must be unique';

-- Gist index on the object of type ltree
create index x_gist_tree_zon on public.tr_study_zone_zon
  using gist(zon_tree);
comment on index public.x_gist_tree_zon is 'Index of optimization of tree traversals on the tree object of type ltree';
  
-- Index for foreign keys

create index x_btr_fkey_typz_zon on public.tr_study_zone_zon
  using btree (zon_typz_id);
comment on index public.x_btr_fkey_typz_zon is 'Foreign key index to speed up updates / deletion';    

/*==============================================================*/
/* Table : public.taxonomic_referential                                   */
/*==============================================================*/

create table tr_taxonomic_referential_ref (
  ref_id                serial               not null,
  ref_label 			varchar(128) 		 not null,
  ref_version 			varchar(16) 		 not null,
  ref_code 				varchar(16)			 not null,
  ref_comment			varchar(255)		 null,
  constraint tr_taxonomic_referential_ref_pkey primary key(ref_id),
  constraint c_uni_code_ref unique(ref_code),
  constraint c_uni_lab_ver_ref unique(ref_label,ref_version)
); 

comment on table public.tr_taxonomic_referential_ref is 'Table of references';
comment on column public.tr_taxonomic_referential_ref.ref_id is 'Digital automatic identifier of the referential';
comment on column public.tr_taxonomic_referential_ref.ref_label is 'referential name';
comment on column public.tr_taxonomic_referential_ref.ref_version is 'Version of the referential';
comment on column public.tr_taxonomic_referential_ref.ref_code is 'Unique code to identify the referential (except versions)';
comment on column public.tr_taxonomic_referential_ref.ref_comment is 'Free comment.';
comment on constraint c_uni_code_ref on public.tr_taxonomic_referential_ref is 'The code of the referential is unique';
comment on constraint c_uni_lab_ver_ref on public.tr_taxonomic_referential_ref is 'The label / version pair of the referential is unique';

INSERT INTO public.tr_taxonomic_referential_ref ("ref_label", "ref_version", "ref_code") VALUES ('TAXREF', 'V15','TAXREFV15');

/*==============================================================*/
/* Table : public.taxon                                         */
/*==============================================================*/
create table tr_taxon_tax (
   tax_id               serial               not null,
   tax_ref_id           integer              not null,
   tax_label            varchar(255)         not null,
   tax_identifier_ext   varchar(64)			 not null,
   constraint tr_taxon_tax_pkey primary key (tax_id),
   constraint c_uni_ref_label_tax unique(tax_ref_id, tax_label),
   constraint c_uni_ref_identifier_ext_tax unique(tax_ref_id, tax_identifier_ext),
   constraint c_fk_ref_tax foreign key (tax_ref_id)
    	references public.tr_taxonomic_referential_ref(ref_id)
		on delete no action
		on update no action
		not deferrable   
);

comment on table tr_taxon_tax is 'Table of plant taxa (species) referenced in the database';
comment on column tr_taxon_tax.tax_id is 'digital automatic identifier of the taxon';
comment on column tr_taxon_tax.tax_ref_id is 'Automatic numeric identifier of the home referential';
comment on column tr_taxon_tax.tax_label is 'Unique common name of the taxon in its original referential';
comment on column tr_taxon_tax.tax_identifier_ext is 'Identifier in the information source (original referential). For example the '' CD_NOM field of a taxref table ''. This field should allow monitoring of the evolution of benchmarks over time.';
comment on constraint c_uni_ref_label_tax on public.tr_taxon_tax is 'The referential / label pair is unique';
comment on constraint c_uni_ref_identifier_ext_tax on public.tr_taxon_tax is 'The referential / external identifier pair is unique';
comment on constraint c_fk_ref_tax on public.tr_taxon_tax is 'A taxon is linked to 1 and 1 single referential';

-- Index for foreign keys
create index x_btr_fkey_ref_tax on public.tr_taxon_tax
  using btree (tax_ref_id);
comment on index public.x_btr_fkey_ref_tax is 'Foreign key index to speed up updates / deletion';    

--******************************************************************************************************************************************
--******************* Set of 4 tables dedicated to the management of measurements on zones (metadata) and resulting from the generic model *
--******************************************************************************************************************************************

/*====================================================================================*/
/* Table : public.t_measure_mzon                                  					  */
/*====================================================================================*/
create table t_measure_mzon (
   mzon_id							serial		not null,
   mzon_zon_id						integer		not null, 
   mzon_tym_id						integer		null,
   mzon_var_id						integer		not null,
   mzon_fil_id						integer		null,
   mzon_prot_id						integer		null,
   mzon_std_id						integer		null,
   mzon_uni_id						integer		null,
   mzon_value_num					float8		null,
   mzon_value_text					varchar		null,
   mzon_value_date					date		null,
   mzon_date_measurement			date		null,
   mzon_precision_date_measurement	float8		null,
   mzon_precision_measurement		float8		null,
   mzon_note						varchar		null,
   constraint t_measure_mzon_pkey primary key (mzon_id),
   constraint c_uni_zon_var_datemeasure_mzon UNIQUE(mzon_zon_id, mzon_var_id, mzon_date_measurement), -- This 'human' key can be revisited during the specialization for the measurements of different objects.
   constraint c_fk_zon_mzon foreign key (mzon_zon_id)
		references public.tr_study_zone_zon(zon_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_tym_mzon foreign key (mzon_tym_id)
		references public.tr_type_measure_tym(tym_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_var_mzon foreign key (mzon_var_id)
    	references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_fil_mzon foreign key (mzon_fil_id)
		references public.tr_file_fil(fil_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_prot_mzon foreign key (mzon_prot_id)
		references public.tr_protocol_prot(prot_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_std_mzon foreign key (mzon_std_id)
		references public.tr_standard_std(std_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_uni_mzon foreign key (mzon_uni_id)
		references public.tr_unit_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table t_measure_mzon is 'Table of metadata measures for study zones';
comment on column t_measure_mzon.mzon_id is 'Numeric automatic identifier of the metadata measurement of the zone';
comment on column t_measure_mzon.mzon_zon_id is 'Automatic numeric zone identifier';
comment on column t_measure_mzon.mzon_tym_id is 'Automatic numeric identifier of the associated measurement type';
comment on column t_measure_mzon.mzon_var_id is 'Unique numeric identifier of the associated variable';
comment on column t_measure_mzon.mzon_fil_id is 'Automatic numeric identifier of the associated file';
comment on column t_measure_mzon.mzon_prot_id is 'Unique numeric identifier of the associated protocol';
comment on column t_measure_mzon.mzon_std_id is 'Automatic numeric identifier of the associated standard';
comment on column t_measure_mzon.mzon_uni_id is 'Unique numeric identifier of the associated measurement unit';
comment on column t_measure_mzon.mzon_value_num is 'value of the measurement if it is of numeric type';
comment on column t_measure_mzon.mzon_value_text is 'value of the measure if it is a text type';
comment on column t_measure_mzon.mzon_value_date is 'value of the measure if it is of type date';
comment on column t_measure_mzon.mzon_date_measurement is 'date of measurement';
comment on column t_measure_mzon.mzon_precision_date_measurement is 'Clarification concerning the measurement date';
comment on column t_measure_mzon.mzon_precision_measurement is 'Accuracy of the measurement value';
comment on column t_measure_mzon.mzon_note is 'Additional information associated with the measure';
comment on constraint c_fk_zon_mzon on public.t_measure_mzon is 'a measure must be attached to one and only one object';
comment on constraint c_fk_tym_mzon on public.t_measure_mzon is 'a measure can be attached to O or 1 type of measure';
comment on constraint c_fk_var_mzon on public.t_measure_mzon is 'a measure must be linked to one and only one variable';
comment on constraint c_fk_fil_mzon on public.t_measure_mzon is 'a measurement can be attached to O or 1 file';
comment on constraint c_fk_prot_mzon on public.t_measure_mzon is 'a measurement can be attached to O or 1 protocol';
comment on constraint c_fk_std_mzon on public.t_measure_mzon is 'a measure can be attached to O or 1 standard';
comment on constraint c_fk_uni_mzon on public.t_measure_mzon is 'a measure can be attached to 0 or 1 unit';

-- date_measurement is null
create unique index c_uni_zon_var_mzon on public.t_measure_mzon
  using btree (mzon_zon_id, mzon_var_id)
  where (mzon_date_measurement is null);
comment on index public.c_uni_zon_var_mzon is 'the variable / zone pair is unique if the measurement date field is not filled in.';

-- Index for foreign keys
create index x_btr_fkey_zon_mzon on public.t_measure_mzon
  using btree (mzon_zon_id);
comment on index public.x_btr_fkey_zon_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_tym_mzon on public.t_measure_mzon
  using btree (mzon_tym_id);
comment on index public.x_btr_fkey_tym_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_var_mzon on public.t_measure_mzon
  using btree (mzon_var_id);
comment on index public.x_btr_fkey_var_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_fil_mzon on public.t_measure_mzon
  using btree (mzon_fil_id);
comment on index public.x_btr_fkey_fil_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_prot_mzon on public.t_measure_mzon
  using btree (mzon_prot_id);
comment on index public.x_btr_fkey_prot_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_std_mzon on public.t_measure_mzon
  using btree (mzon_std_id);
comment on index public.x_btr_fkey_std_mzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_fkey_uni_mzon on public.t_measure_mzon
  using btree (mzon_uni_id);
comment on index public.x_btr_fkey_uni_mzon is 'Foreign key index to speed up updates / deletion';    

/*================================================================*/
/* Table : public.tj_rol_ope_mzon_roz                            */
/*			Table associating an object measure with an operator  */
/*			and a role										      */
/*================================================================*/
create table tj_rol_ope_mzon_roz (
   roz_mzon_id          integer              not null,
   roz_ope_id           integer              not null,
   roz_rol_id			integer				 not null,
   constraint tj_rol_ope_mzon_roz_pkey primary key (roz_mzon_id, roz_ope_id,roz_rol_id),
   constraint c_fk_mzon_roz foreign key (roz_mzon_id)
		references public.t_measure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_roz foreign key (roz_ope_id)
		references public.tr_operator_ope(ope_id)
		on delete no action
		on update no action
		not deferrable, 		
   constraint c_fk_rol_roz foreign key (roz_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_rol_ope_mzon_roz is 'Join table associating a measure with an operator and a role for this operator';
comment on column tj_rol_ope_mzon_roz.roz_mzon_id is 'Unique numeric identifier of the associated measurement';
comment on column tj_rol_ope_mzon_roz.roz_ope_id is 'Numerical automatic identifier of the associated operator';
comment on column tj_rol_ope_mzon_roz.roz_rol_id is 'Numeric automatic identifier of the associated role';
comment on constraint c_fk_mzon_roz on public.tj_rol_ope_mzon_roz is 'The roz_mzon_id field necessarily corresponds to an existing measurement';
comment on constraint c_fk_ope_roz on public.tj_rol_ope_mzon_roz is 'The roz_ope_id field necessarily corresponds to an existing operator';
comment on constraint c_fk_rol_roz on public.tj_rol_ope_mzon_roz is 'The roz_rol_id field must correspond to an existing role';

-- Index for foreign keys
create index x_btr_mzon_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_mzon_id);
comment on index public.x_btr_mzon_roz is 'Foreign key index to speed up updates / deletion';    
create index x_btr_ope_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_ope_id);
comment on index public.x_btr_ope_roz is 'Foreign key index to speed up updates / deletion';    
create index x_btr_rol_roz on public.tj_rol_ope_mzon_roz
  using btree (roz_rol_id);
comment on index public.x_btr_rol_roz is 'Foreign key index to speed up updates / deletion';   
 
/*=================================================================*/
/* Table : public.tj_grp_mzon_gmz                                  */
/*			Table associating an object measure with a grouping    */
/*=================================================================*/
create table tj_grp_mzon_gmz (
   gmz_mzon_id          integer              not null,
   gmz_grp_id           integer              not null,
   constraint tj_grp_mzon_gmz_pkey primary key (gmz_mzon_id, gmz_grp_id),
   constraint c_fk_mzon_gmz foreign key (gmz_mzon_id)
		references public.t_measure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_grp_gmz foreign key (gmz_grp_id)
		references public.t_grouping_grp(grp_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_grp_mzon_gmz is 'Join table associating a measure with a grouping.';
comment on column tj_grp_mzon_gmz.gmz_mzon_id is 'Unique numeric identifier of the associated measurement';
comment on column tj_grp_mzon_gmz.gmz_grp_id is 'Numerical automatic identifier of the associated grouping';
comment on constraint c_fk_mzon_gmz on public.tj_grp_mzon_gmz is 'The gmz_mzon_id field necessarily corresponds to an existing measurement';
comment on constraint c_fk_grp_gmz on public.tj_grp_mzon_gmz is 'The gmz_grp_id field necessarily corresponds to an existing grouping';

-- Index for foreign keys
create index x_btr_mzon_gmz on public.tj_grp_mzon_gmz
  using btree (gmz_mzon_id);
comment on index public.x_btr_mzon_gmz is 'Foreign key index to speed up updates / deletion';    
create index x_btr_grp_gmz on public.tj_grp_mzon_gmz
  using btree (gmz_grp_id);
comment on index public.x_btr_grp_gmz is 'Foreign key index to speed up updates / deletion';
   
/*===================================================================*/
/* Table : public.tj_mzon_mzon_tzon                                  */
/*			Table associating an object measure with another object  */
/*			measure, the second being complementary (metadata)       */
/*			or necessary for the interpretation of the first		 */
/*===================================================================*/
create table tj_mzon_mzon_tzon (
   tzon_ref_mzon_id        integer           not null,
   tzon_meta_mzon_id       integer           not null,
   tzon_status				varchar(32)		  null,
   constraint tj_mzon_mzon_tzon_pkey primary key (tzon_ref_mzon_id, tzon_meta_mzon_id),
   constraint c_fk_ref_mzon_tzon foreign key (tzon_ref_mzon_id)
		references public.t_measure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_meta_mzon_tzon foreign key (tzon_meta_mzon_id)
		references public.t_measure_mzon(mzon_id)
		on delete no action
		on update no action
		not deferrable		
		
);
comment on table tj_mzon_mzon_tzon is 'Join table associating a measure with another measure, the second being complementary (metadata) or necessary for the interpretation of the first.';
comment on column tj_mzon_mzon_tzon.tzon_ref_mzon_id is 'Automatic numeric identifier of the reference measurement';
comment on column tj_mzon_mzon_tzon.tzon_meta_mzon_id is 'Automatic numeric identifier of the measurement considered as metadata of the reference measurement';
comment on column tj_mzon_mzon_tzon.tzon_status is 'Indicator specifying the status of the metadata in relation to the reference measure. For example the mandatory or recommended character for interpretation.';
comment on constraint c_fk_ref_mzon_tzon on public.tj_mzon_mzon_tzon is 'The tzon_ref_mzon_id field necessarily corresponds to an existing measurement';
comment on constraint c_fk_meta_mzon_tzon on public.tj_mzon_mzon_tzon is 'The tzon_meta_mzon_id field necessarily corresponds to an existing measurement';

-- Index for foreign keys
create index x_btr_ref_mzon_tzon on public.tj_mzon_mzon_tzon
  using btree (tzon_ref_mzon_id);
comment on index public.x_btr_ref_mzon_tzon is 'Foreign key index to speed up updates / deletion';    
create index x_btr_meta_mzon_tzon on public.tj_mzon_mzon_tzon
  using btree (tzon_meta_mzon_id);
comment on index public.x_btr_meta_mzon_tzon is 'Foreign key index to speed up updates / deletion';    

--******************************************************************************************************************************************
--******************* 				Views making it easier to use the basic structure.				 				************************
--******************************************************************************************************************************************

/*=================================================================*/
/* Vue : public.v_metadata_zone                                    */
/*			View used to display zone measurements and		 	   */	
/*			those of their parents (complete tree structure) 	   */
/*=================================================================*/

create or replace view public.v_metadata_zone(
    id_child,
    ltree_child,
    id_parent,
    ltree_parent,
    date_measure,
    var_name,
    var_code,
    value,
    prot_description,
    uni_name,
    uni_symbol,
	inherited)
as
with full_zone as(
  select zon1.zon_id as id_child,
         zon1.zon_tree as ltree_child,
         zon2.zon_id as id_parent,
         zon2.zon_tree as ltree_parent
  from tr_study_zone_zon zon1
       join tr_study_zone_zon zon2 on zon2.zon_tree @> zon1.zon_tree
  order by zon1.zon_tree,
           zon2.zon_tree)
    select full_zone.id_child,
           full_zone.ltree_child,
           full_zone.id_parent,
           full_zone.ltree_parent,
           t_measure_mzon.mzon_date_measurement as date_mesure,
           tr_variable_var.var_name,
           tr_variable_var.var_code,
           coalesce(t_measure_mzon.mzon_value_num::text,
             t_measure_mzon.mzon_value_text::text,
             t_measure_mzon.mzon_value_date::text) as value,
           tr_protocol_prot.prot_description,
           tr_unit_uni.uni_name,
           tr_unit_uni.uni_symbol,
		   full_zone.id_parent <> full_zone.id_child AS inherited
    from full_zone
         join t_measure_mzon on full_zone.id_parent = t_measure_mzon.mzon_zon_id
         join tr_variable_var on t_measure_mzon.mzon_var_id =
           tr_variable_var.var_id
         left join tr_unit_uni on t_measure_mzon.mzon_uni_id =
           tr_unit_uni.uni_id
         left join tr_protocol_prot on t_measure_mzon.mzon_prot_id =
           tr_protocol_prot.prot_id;
comment on view public.v_metadata_zone is 'View allowing the display of zone measurements (variables / metadata)';
		   
COMMIT;