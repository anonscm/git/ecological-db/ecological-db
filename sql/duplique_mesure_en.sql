-- File containing an extract of crebas (4 end of script tables) and on which you can run a program allowing duplication / customization

/*==========================================================================================*/
/* Table : t_measure_mobj                                           				  		*/
/*		This table must be renamed or even duplicated to take into account as many    		*/
/*		business objects to be measured as necessary. For example a measure_tre table 		*/
/*		to contain the measures concerning trees.								   	  		*/
/*		The satellite tables will also have to be renamed or even duplicated to meet  		*/
/*		the need (these are in particular the following 3 tables:		  			  		*/
/*			the one which associates the measure with an operator and a role.		  		*/
/*			the one which possibly associates the measure with 1 or more regroupings  		*/
/*			the one that associates the measure with another measure considered as metadata	*/
/*==========================================================================================*/
create table t_measure_mobj (
   mobj_id               serial               not null,
   mobj_obj_id			 bigint				  not null, 
   mobj_tym_id           integer              null,
   mobj_var_id           integer              not null,
   mobj_fil_id           integer              null,
   mobj_prot_id          integer              null,
   mobj_std_id           integer              null,
   mobj_uni_id           integer              null,
   mobj_value_num        float8               null,
   mobj_value_text       varchar              null,
   mobj_value_date       date                 null,
   mobj_date_measure     date                 null,
   mobj_precision_date_measure float8          null,
   mobj_precision_measure 	  float8          null,
   mobj_observation        varchar              null,
   constraint t_measure_mobj_pkey primary key (mobj_id),
   constraint c_uni_obj_var_datemeasure_mobj unique(mobj_obj_id,mobj_var_id,mobj_date_measure), -- This 'human' key can be revisited during the specialization for the measurements of different objects.
   constraint c_fk_obj_mobj foreign key (mobj_obj_id)
		references public.t_objet_obj(obj_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_tym_mobj foreign key (mobj_tym_id)
		references public.tr_type_measure_tym(tym_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_var_mobj foreign key (mobj_var_id)
    	references public.tr_variable_var(var_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_fil_mobj foreign key (mobj_fil_id)
		references public.tr_file_fil(fil_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_prot_mobj foreign key (mobj_prot_id)
		references public.tr_protocol_prot(prot_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_std_mobj foreign key (mobj_std_id)
		references public.tr_standard_std(std_id)
		on delete no action
		on update no action
		not deferrable, 
   constraint c_fk_uni_mobj foreign key (mobj_uni_id)
		references public.tr_unit_uni(uni_id)
		on delete no action
		on update no action
		not deferrable
);
comment on table t_measure_mobj is 'Table of variable measures for a specific type of object';
comment on column t_measure_mobj.mobj_id is 'Automatic numeric identifier of the measure';
comment on column t_measure_mobj.mobj_obj_id is 'Automatic numeric identifier of the associated object';
comment on column t_measure_mobj.mobj_tym_id is 'Automatic numeric identifier of the associated type of measure';
comment on column t_measure_mobj.mobj_var_id is 'Automatic numeric identifier of the associated variable';
comment on column t_measure_mobj.mobj_fil_id is 'Automatic numeric identifier of the associated file';
comment on column t_measure_mobj.mobj_prot_id is 'Automatic numeric identifier of the associated protocol';
comment on column t_measure_mobj.mobj_std_id is 'Automatic numeric identifier of the associated standard';
comment on column t_measure_mobj.mobj_uni_id is 'Automatic numeric identifier of the associated unit of measure';
comment on column t_measure_mobj.mobj_value_num is 'value of the measure if it is of numeric type';
comment on column t_measure_mobj.mobj_value_text is 'value of the measure if it is of type text';
comment on column t_measure_mobj.mobj_value_date is 'value of the measure if it is of type date';
comment on column t_measure_mobj.mobj_date_measure is 'date of the measure';
comment on column t_measure_mobj.mobj_precision_date_measure is 'Precision concerning the date of the measurement';
comment on column t_measure_mobj.mobj_precision_measure is 'Precision of the measure value';
comment on column t_measure_mobj.mobj_observation is 'Additional information associated with the measure';
comment on constraint c_uni_obj_var_datemeasure_mobj on public.t_measure_mobj is 'The set identifier of the object / identifier of the variable / date of the measure is unique';

comment on constraint c_fk_obj_mobj on public.t_measure_mobj is 'a measure must be attached to one and only one object';
comment on constraint c_fk_tym_mobj on public.t_measure_mobj is 'a measure can be attached to O or 1 type of measure';
comment on constraint c_fk_var_mobj on public.t_measure_mobj is 'a measure must be attached to one and only one variable';
comment on constraint c_fk_fil_mobj on public.t_measure_mobj is 'a measure can be attached to O or 1 file';
comment on constraint c_fk_prot_mobj on public.t_measure_mobj is 'a measure can be attached to O or 1 protocol';
comment on constraint c_fk_std_mobj on public.t_measure_mobj is 'a measure can be attached to O or 1 standard';
comment on constraint c_fk_uni_mobj on public.t_measure_mobj is 'a measure can be attached to O or 1 unit';

create unique index c_uni_obj_var_mobj on public.t_measure_mobj
  using btree (mobj_obj_id, mobj_var_id)
  where mobj_date_measure is null;
comment on index public.c_uni_obj_var_mobj is 'The variable / object pair is unique if the measure date is not entered.';

-- Index for foreign keys
create index x_btr_fkey_obj_mobj on public.t_measure_mobj
  using btree (mobj_obj_id);
comment on index public.x_btr_fkey_obj_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_tym_mobj on public.t_measure_mobj
  using btree (mobj_tym_id);
comment on index public.x_btr_fkey_tym_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_var_mobj on public.t_measure_mobj
  using btree (mobj_var_id);
comment on index public.x_btr_fkey_var_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_fil_mobj on public.t_measure_mobj
  using btree (mobj_fil_id);
comment on index public.x_btr_fkey_fil_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_prot_mobj on public.t_measure_mobj
  using btree (mobj_prot_id);
comment on index public.x_btr_fkey_prot_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_std_mobj on public.t_measure_mobj
  using btree (mobj_std_id);
comment on index public.x_btr_fkey_std_mobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_fkey_uni_mobj on public.t_measure_mobj
  using btree (mobj_uni_id);
comment on index public.x_btr_fkey_uni_mobj is 'Foreign key index to speed up updates / deletion';    

/*==============================================================*/
/* Table : tj_rol_ope_mobj_rom                                 */
/*			Table associating an object measure with an 		*/
/*			operator and a role									*/
/*==============================================================*/
create table tj_rol_ope_mobj_rom (
   rom_mobj_id           integer              not null,
   rom_ope_id           integer              not null,
   rom_rol_id			integer				 not null,
   constraint tj_rol_ope_mobj_rom_pkey primary key (rom_mobj_id, rom_ope_id,rom_rol_id),
   constraint c_fk_mobj_rom foreign key (rom_mobj_id)
		references public.t_measure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_ope_rom foreign key (rom_ope_id)
		references public.tr_operator_ope(ope_id)
		on delete no action
		on update no action
		not deferrable, 		
   constraint c_fk_rol_rom foreign key (rom_rol_id)
		references public.tr_role_rol(rol_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_rol_ope_mobj_rom is 'Join table associating a measure with an operator and a role for this operator';
comment on column tj_rol_ope_mobj_rom.rom_mobj_id is 'Automatic numeric identifier of the associated measure';
comment on column tj_rol_ope_mobj_rom.rom_ope_id is 'Automatic numeric identifier of the associated operator';
comment on column tj_rol_ope_mobj_rom.rom_rol_id is 'Automatic numeric identifier of the associated role';
comment on constraint c_fk_mobj_rom on public.tj_rol_ope_mobj_rom is 'The rom_mobj_id field necessarily corresponds to an existing measure';
comment on constraint c_fk_ope_rom on public.tj_rol_ope_mobj_rom is 'The rom_ope_id field necessarily corresponds to an existing operator';
comment on constraint c_fk_rol_rom on public.tj_rol_ope_mobj_rom is 'The rom_rol_id field necessarily corresponds to an existing role';

-- Index for foreign keys
create index x_btr_mobj_rom on public.tj_rol_ope_mobj_rom
  using btree (rom_mobj_id);
comment on index public.x_btr_mobj_rom is 'Foreign key index to speed up updates / deletion';    

create index x_btr_ope_rom on public.tj_rol_ope_mobj_rom
  using btree (rom_ope_id);
comment on index public.x_btr_ope_rom is 'Foreign key index to speed up updates / deletion';    

create index x_btr_rol_rom on public.tj_rol_ope_mobj_rom
  using btree (rom_rol_id);
comment on index public.x_btr_rol_rom is 'Foreign key index to speed up updates / deletion';    

/*=================================================================*/
/* Table : tj_grp_mobj_gmo                                         */
/*			Table associating an object measure to a grouping      */
/*=================================================================*/
create table tj_grp_mobj_gmo (
   gmo_mobj_id          integer              not null,
   gmo_grp_id           integer              not null,
   constraint tj_grp_mobj_gmo_pkey primary key (gmo_mobj_id, gmo_grp_id),
   constraint c_fk_mobj_gmo foreign key (gmo_mobj_id)
		references public.t_measure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_grp_gmo foreign key (gmo_grp_id)
		references public.t_grouping_grp(grp_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_grp_mobj_gmo is 'Join table associating a measure with a grouping.';
comment on column tj_grp_mobj_gmo.gmo_mobj_id is 'Automatic numeric identifier of the associated measure';
comment on column tj_grp_mobj_gmo.gmo_grp_id is 'Automatic numeric identifier of the associated grouping';
comment on constraint c_fk_mobj_gmo on public.tj_grp_mobj_gmo is 'The gmo_mobj_id field necessarily corresponds to an existing measure';
comment on constraint c_fk_grp_gmo on public.tj_grp_mobj_gmo is 'The gmo_grp_id field necessarily corresponds to an existing grouping';

-- Index for foreign keys
create index x_btr_mobj_gmo on public.tj_grp_mobj_gmo
  using btree (gmo_mobj_id);
comment on index public.x_btr_mobj_gmo is 'Foreign key index to speed up updates / deletion';    

create index x_btr_grp_gmo on public.tj_grp_mobj_gmo
  using btree (gmo_grp_id);
comment on index public.x_btr_grp_gmo is 'Foreign key index to speed up updates / deletion';    

/*=================================================================*/
/* Table : tj_mobj_mobj_tobj                                       */
/*			Table associating an object measure to another object  */
/*			measure, the second being complementary (metadata) or  */
/*			necessary for the interpretation of the first		   */
/*=================================================================*/
create table tj_mobj_mobj_tobj (
   tobj_ref_mobj_id          integer              not null,
   tobj_meta_mobj_id         integer              not null,
   tobj_statut				 varchar(32)		  null,
   constraint tj_mobj_mobj_tobj_pkey primary key (tobj_ref_mobj_id, tobj_meta_mobj_id),
   constraint c_fk_ref_mobj_tobj foreign key (tobj_ref_mobj_id)
		references public.t_measure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable,
   constraint c_fk_meta_mobj_tobj foreign key (tobj_meta_mobj_id)
		references public.t_measure_mobj(mobj_id)
		on delete no action
		on update no action
		not deferrable		
		
);

comment on table tj_mobj_mobj_tobj is 'Table associating an object measure to another object measure, the second being complementary (metadata) or necessary for the interpretation of the first';
comment on column tj_mobj_mobj_tobj.tobj_ref_mobj_id is 'Automatic numeric identifier of the reference measure';
comment on column tj_mobj_mobj_tobj.tobj_meta_mobj_id is 'Automatic numeric identifier of the measure considered as metadata of the reference measure';
comment on column tj_mobj_mobj_tobj.tobj_statut is 'Indicator specifying the status of the metadata in relation to the reference measure. For example the mandatory or recommended character for interpretation.';

comment on constraint c_fk_ref_mobj_tobj on public.tj_mobj_mobj_tobj is 'The tobj_ref_mobj_id field necessarily corresponds to an existing measure';
comment on constraint c_fk_meta_mobj_tobj on public.tj_mobj_mobj_tobj is 'The tobj_meta_mobj_id field necessarily corresponds to an existing measure';

-- Index for foreign keys
create index x_btr_mobj_tobj on public.tj_mobj_mobj_tobj
  using btree (tobj_ref_mobj_id);
comment on index public.x_btr_mobj_tobj is 'Foreign key index to speed up updates / deletion';    

create index x_btr_grp_tobj on public.tj_mobj_mobj_tobj
  using btree (tobj_meta_mobj_id);
comment on index public.x_btr_grp_tobj is 'Foreign key index to speed up updates / deletion';    
