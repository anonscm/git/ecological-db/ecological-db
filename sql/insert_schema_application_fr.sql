---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- Date création :	19/07/2022
-- dernière modificcation	:	19/07/2022
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour effectuer des insertions. 
--					
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer sur une base précedemment créée à l'aide du script suivant :
--				https://git.renater.fr/anonscm/gitweb?p=script/script.git;a=blob_plain;f=serveurs+virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des index et contraintes ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f insert_schema_application_fr.sql -d nombase
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 			
-- MODIFICATION :

START TRANSACTION;

/*==============================================================*/
/* Table : application.configuration formulaires access              */
/*==============================================================*/

insert into application.t_formulaire_access_configuration_faconf(faconf_sgbd, faconf_version, faconf_odbc, faconf_deconnecte,faconf_num_instance, faconf_database, faconf_role_ecriture, faconf_tag) 
values ('postgresql', '15', true, false, null, 'db_ecological_db', 'ecological_db_ecriture', null);

/*==============================================================*/
/* Table : application.t_utilisateur_usr                        */
/*==============================================================*/

insert into application.t_utilisateur_usr(usr_login, usr_mail)
values('anonyme', 'anonyme@anonyme.fr');

/*==============================================================*/
/* Table : application.tr_role_rol                              */
/*==============================================================*/

insert into application.tr_role_rol(rol_libelle, rol_description)
values('administrateur', 'Rôle permettant d''administrer l''application.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('dpo', 'Rôle permettant de gérer les demandes liées aux données personnelles.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('rédacteur', 'Rôle permetteant d''écrire dans la base de données.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('lecteur', 'Rôle permettant de lire les données présentent dans la base.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('lecteur-limité', 'Rôle permettant de lire un échantillon des données de la base.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('guest', 'Rôle permettant d''accéder aux contenus nécessitant une authentification.');
insert into application.tr_role_rol(rol_libelle, rol_description)
values('sql', 'Rôle permettant de visualiser quelques unes des requêtes sql générée par l''application.');

/*==============================================================*/
/* Table : application.tr_permission_perm                       */
/*==============================================================*/

insert into application.tr_permission_perm(perm_libelle, perm_description)
values('administrer', 'Permission accordant le droit d''administrer l''application.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('dpo', 'Permission accordant le droit de gérer les demandes RGPD.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('écriture', 'Permission accordant le droit d''écrire dans la base de données.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('lecture', 'Permission accordant le droit de lire sur la base de données.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('lecture-limitée', 'Permission accordant le droit de lire un échantillion de données.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('guest', 'Permission donnat accés aux contenus nécessitant une authentification.');
insert into application.tr_permission_perm(perm_libelle, perm_description)
values('sql', 'Permission permettant de visualiser quelques unes des requêtes sql générée par l''application.');

/*==============================================================*/
/* Table : application.tj_rol_rol_rolp                          */
/*==============================================================*/

insert into application.tj_rol_rol_rolp(rolp_role_parent_id, rolp_role_enfant_id) values(
	(select rol_id from application.tr_role_rol where rol_libelle = 'administrateur'),
	(select rol_id from application.tr_role_rol where rol_libelle = 'dpo')
);
insert into application.tj_rol_rol_rolp(rolp_role_parent_id, rolp_role_enfant_id) values(
	(select rol_id from application.tr_role_rol where rol_libelle = 'administrateur'),
	(select rol_id from application.tr_role_rol where rol_libelle = 'rédacteur')
);
insert into application.tj_rol_rol_rolp(rolp_role_parent_id, rolp_role_enfant_id) values(
	(select rol_id from application.tr_role_rol where rol_libelle = 'rédacteur'),
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur')
);
insert into application.tj_rol_rol_rolp(rolp_role_parent_id, rolp_role_enfant_id) values(
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur'),
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur-limité')
);
insert into application.tj_rol_rol_rolp(rolp_role_parent_id, rolp_role_enfant_id) values(
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur'),
	(select rol_id from application.tr_role_rol where rol_libelle = 'guest')
);

/*==============================================================*/
/* Table : application.tj_rol_perm_aperm                        */
/*==============================================================*/

insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'administrateur'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'administrer')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'dpo'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'dpo')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'rédacteur'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'écriture')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'lecture')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'lecteur-limité'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'lecture-limitée')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'guest'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'guest')
);
insert into application.tj_rol_perm_aperm(aperm_rol_id, aperm_perm_id) values (
	(select rol_id from application.tr_role_rol where rol_libelle = 'sql'),
	(select perm_id from application.tr_permission_perm where perm_libelle = 'sql')
);

/*==============================================================*/
/* Table : application.tr_type_rgpd_typrgpd                     */
/*==============================================================*/

insert into application.tr_type_rgpd_typrgpd(typrgpd_code, typrgpd_libelle)
values	('access', 'Droit d''accès'),
		('update', 'Droit de rectification'),
		('anonymous', 'Droit à l''anonymisation'),
		('no-consent', 'Droit de retirer son consentement'),
		('portability', 'Droit à la portabilité des données'),
		('opposition', 'Droit d''opposition');

COMMIT