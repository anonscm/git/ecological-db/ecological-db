Attribute VB_Name = "Codifications"
Option Explicit 'oblige la d�claration des variables
Option Compare Database
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 28/09/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'* Ce module permet de g�rer les �crans de codifications de mani�re uniforme. (Acc�s aux      *
'* diff�rents contr�les li�s, pr�sence de certains boutons selon le contexte ...              *
'*                                                                                            *
'*      REGLES A RESPECTER : La fen�tre de codification � traiter devra respecter les r�gles  *
'*                           suivantes :                                                      *
'*                              - La fen�tre devra contenir au moins un contr�le DATA pour    *
'*                                acc�der aux codifications.                                  *
'*                              - Une fonction publique portant le nom ControleValidite       *
'*                                devra �tre impl�ment�e dans le module de la feuille. Cette  *
'*                                fonction admet un param�tre permettant de savoir quel bouton*
'*                                est activ�, elle effectue �ventuelement des contr�les de    *
'*                                validit� et renvoie True si l'op�ration sur les donn�es a   *
'*                                �t� accept�e et False dans le cas contraire. Elle peut      *
'*                                utiliser des m�thodes telles que Update ou Delete et traiter*
'*                                le r�sultat ou bien n'effectuer que des contr�les sp�cifiques*
'*                                ou encore laisser la fonction VisibiliteBoutonsCodification *
'*                                faire les mises � jour et traiter les erreurs.              *
'*                              - Une fonction publique portant le nom Interface devra �tre   *
'*                                impl�ment�e dans le module de la feuille. Cette fonction    *
'*                                admet un param�tre permettant de savoir quelle situation se *
'*                                pr�sente. ( si une mise � jour (cr�ation ou modification)   *
'*                                vient d'�tre r�ussie; si l'op�rateur passe en mise � jour   *
'*                                (cr�ation ou modification)).Ainsi le programmeur peut       *
'*                                adapter l'interface en plus de l'adaptation faite par la    *
'*                                fonction VisibiliteBoutonsCodification.                     *
'*                              - Une fonction sync appelant la fonction globale globalsync et*
'*                                pouvant �ventuellemet la compl�ter dans le but de veiller � *
'*                                la bonne synchronisation de tous les �l�ments de l'�cran.   *
'*                                                                                            *
'*      PROBLEMES EVENTUELS :   - Si la feuille contient plusieurs contr�le DATA ayant un ou  *
'*                                plusieurs champs portant le m�me nom dans les 2 contr�les   *
'*                                il est impossible de savoir � quel champ est li� un contr�le*
'*                                texte (par exemple) au moment de l'ex�cution.               *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :
'*                      Date    :
'*                      Objet   :
'**********************************************************************************************

'Les 5 constantes suivantes ne doivent pas �tre modifi�es. Elles permettent de remplir un
'tableau avec les libell�s des Boutons n�cessaires sur un �cran de codifications. Elles
'permettent �galement de construire ces boutons sur la feuille concern�e et l'index du bouton
'sera �gal � la constante ad�quat. Cet index permettra par ailleurs de savoir quel bouton a
'�t� cliqu� et d'effectuer le traitement ad�quat.
Public Const MODIFIER = 0
Public Const MISE_A_JOUR = 1
Public Const AJOUTER = 2
Public Const SUPPRIMER = 3
Public Const ANNULER = 4
Public Const QUITTER = 5

' Constantes utilis�es dans les fonctions Sync des formulaires, ces fonctions �tant activ�es depuis la fonction VisibiliteBoutons
Public Const SYNC_AUCUN = 0
Public Const SYNC_AJOUT_REALISE = 1
Public Const SYNC_MODIF_REALISEE = 2
Public Const SYNC_SUPPR_REALISEE = 3
Public Const SYNC_AJOUT_ANNULE = 4

'La constante CONSULTATION permet d'initialiser l'�cran de codification en mode consultation.
Public Const CONSULTATION = 100

'Dim ListeDesIntitul�sBoutons(0 To 5) As String 'liste des libell�s des boutons d'un �cran de
'codification. Cette liste est rempli par la proc�dure Sub RemplirListeIntitul�()

'Ci-dessous les constantes utilis�e pour le type d'un contr�le (Voir EtablirListeDesContr�le..)
Public Const C_TEXT = 1
Public Const C_DBCOMBO = 2
Public Const C_OLE = 3

Type ControleLie
    NomControle As String
    TypeControle As Integer
    NomChampLie As String
    TypeChampLie As Integer
    LongueurAutorisee As Integer
End Type

Global ListeTmp() As ControleLie

' Nom du composant permettant de choisir un �l�ment (Liste de choix)
Global Const NOM_LISTE_CHOIX = "LST_Choix"

' Nom de l'�tiquette contenant le titre du formulaire.
Global Const NOM_TITRE = "Titre"

' Libell� des param�tres de type String lors de l'execution des requ�tes param�tr�es
Global Const TYPE_PARAM_STRING As String = "string"
' Libell� des param�tres de type integer lors de l'execution des requ�tes param�tr�es
Global Const TYPE_PARAM_INT As String = "integer"

' Type retourn� par la fonction ValideDonnee
Type ValidationData
    ' vrai si la donn�es contr�l�e est jug�e valide
    DonneeValide As Boolean
    ' information renseignant sur les valeurs possibles de la donn�e
    InfoBulle As String
End Type

' Type retourn� par la fonction ValideNombre
Type ValidationPatternNumerique
    ' vrai si le nombre contr�l� respecte le pattern simple
    NombreValide As Boolean
    ' borne sup�rieure correspondant au pattern (permet de contr�ler si l'ordre des pattern est respect�)
    BorneSuperieure As Variant
End Type

Public Const ERREUR_PATTERN_NUMERIQUE = "Mauvais pattern"
Public Const AUCUNE_SYNCHRO = 0
Public Const SYNCHRO_SUR_LISTE = 1
Public Const SYNCHRO_SUR_FORMULAIRE = 2

Public Function IsInArray(Tableau As Variant, Element As String) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 25/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Tableau : le tableau � parcourir
'*          - Element : la valeur de l'�l�ment � tester dans le tableau
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si l'�l�ment est pr�sent dans le tableau
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION : V�rifier la pr�sence d'un �l�ment dans un tableau
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo ErreurIsInArray
Dim I As Integer

IsInArray = False
For I = 0 To UBound(Tableau)
    If Tableau(I) = Element Then
        IsInArray = True
        Exit Function
    End If
Next

Exit Function
ErreurIsInArray:
    Call GestionErreur(Err, "IsInArray", "")
End Function

Public Function ConstruireListeDesChampsLies(Fenetre As Form, Source As Recordset, Optional ControleIgnore As Variant) As Integer
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 13/03/2017
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre : Objet Form qui doit �tre initialis�. En principe il s'agit d'une      *
'*                      fen�tre permettant la mise � jour de codifications.                   *
'*          - Source : Objet de type Recordset auquel les contr�les de la feuille sont li�s   *
'*                     permettant ainsi l'ajout des contr�les li�s dans la variable global    *
'*                     ListeTmp().                                                            *
'*          - ControleIgnore (optionnel) : Contr�le devant �tre ignor�s lors de la           *
'*                                          construction de la liste des champs li�s          *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si tout se d�roule correctement et false dans le cas contraire             *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION : Ins�rer dans la variable globale ListeTmp() tous les contr�les   *
'*                           li�s au SGBD ou � une base access                                *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  : M GUIWARCH
'*                      Date    : 25/11/2016
'*                      Objet   : A cause du changement de type pour la variable NomIdSeriam
'*                              dans le formulaire, il fallait commenter la premi�re condition
'*                              If.
'*                              Il falait �galement cr�er la fonction IsInArray afin de tester
'*                              si le contr�le actuel appartient au tableau NomIdSerial
'*                              (contenant les champs formant la cl� primaire de la table)
'*                              De plus ajout de l'utilisation de la requete (� condition
'*                              qu'elle existe) qui permet de r�cup�rer la longueur maximale
'*                              d'un champ de type text long (> 255 caract�res).
'*          MODIF2 :    Auteur  : M GUIWARCH
'*                      Date    : 13/03/2017
'*                      Objet   : Correction d'un bug lorsque la longueur max d'un champ
'*                              n'est pas d�finie. On a choisit de fixer � z�ro la valeur
'*                              de la variable LongueurAutoris�e lorsque ce cas se produit.
'**********************************************************************************************
On Error GoTo Erreur_ConstruireListeDesChampsLies
Dim ObjetControl As Control
Dim Rang As Integer
Dim I As Integer
Dim NomControle() As String
Dim TypeControle() As Integer
Dim NomChampLie() As String
Dim TypeChampLie() As Integer
Dim LongueurAutorisee() As Integer
Dim NomChampIgnore As String
Dim ResultatRequete As Recordset

' Si le type ou la valeur de la variable NomIdSerial du formulaire n'est pas correcte on affecte un tableau vide � la variable Controle_Ignore
If Not NomIdSerialEstUnTableau(Fenetre) Then
    ControleIgnore = Array()
End If

'Pour chaque contr�le de la feuille
For Each ObjetControl In Fenetre.Controls
    If ControleLieADesDonnees(ObjetControl, Source) And Not IsInArray(ControleIgnore, ObjetControl.Name) Then
        Rang = Rang + 1
        ReDim Preserve NomControle(Rang)
        ReDim Preserve TypeControle(Rang)
        ReDim Preserve NomChampLie(Rang)
        ReDim Preserve TypeChampLie(Rang)
        ReDim Preserve LongueurAutorisee(Rang)

        NomControle(Rang) = ObjetControl.Name
        NomChampLie(Rang) = ObjetControl.ControlSource
        TypeChampLie(Rang) = Source.Fields(ObjetControl.ControlSource).Type

        'Le controle est un ext long (> 255)
        If TypeChampLie(Rang) = dbMemo Then
            'Si la requ�te permettant de r�cup�rer la longueur maximal d'un champ de type text long (> 255 caract�res), on l'execute.
            'REMARQUE : Un champ varchar (sans longueur max) sous postgres sera consid�r� dans ACCESS comm un texte court d'une longueur maximale de 255 charact�res
            'un champ varchar(300) sous postgres sera consid�r� dans ACCESS comme un texte long avec une longueur maximale de 300 charact�res
            'un champs text sous postgres sera consid�r� dans ACCESS comme un texte long sans limite de charact�res
            If RequeteExiste(NOM_REQUETE_TAILLE_MAX_TEXT_LONG) Then
                Set ResultatRequete = ExecuteRequeteParametre(NOM_REQUETE_TAILLE_MAX_TEXT_LONG, Array(Replace(Fenetre.RecordSource, "public_", ""), ObjetControl.Name), TYPE_PARAM_STRING)
                
                ''''' BUG : le fait de passer le param�tre ObjetControl.Name ci-dessus au lieu de ObjetControl.ControlSource aboutit � une req�ete en erreur qui indique que le champs
                ''''' n'existe pas (donc pas de longueur retourn�e) si le nom du champs est diff�rent du nom du contr�le associ�
                ''''' cas concret : passage � la version anglaise de db_toff avec un objet/champ per_mail/per_mail devenu per_mail/per_email.
                ''''' La solution a �t� de renommer l'objet mais sur le principe il faudrait  adapter le code.
                ''''' A discuter avec Seb.
                
                'On a fait le choix positionner la valeur de la variable LongueurAutoris�e � 0
                'lorsque la longueur du champ d�finit pas le sgbd n'est pas renseign�e (donc chaine de caract�res illimit�e
                If IsNull(ResultatRequete.Fields(0)) Then
                    LongueurAutorisee(Rang) = 0
                Else
                    LongueurAutorisee(Rang) = CInt(ResultatRequete.Fields(0))
                End If
                Set ResultatRequete = Nothing
            End If
        End If
        'Si le champ li� est de type chaine
        If TypeChampLie(Rang) = dbText Then
            LongueurAutorisee(Rang) = Source.Fields(ObjetControl.ControlSource).Size
        End If

        'le contr�le est un TextBox
        If TypeOf ObjetControl Is TextBox Then
            TypeControle(Rang) = C_TEXT
        End If

        'Le controle est un DBCombo
        If TypeOf ObjetControl Is ComboBox Then
            TypeControle(Rang) = C_DBCOMBO
        End If
    End If
Next

ReDim ListeTmp(Rang)
For I = 1 To Rang
    ListeTmp(I).NomControle = NomControle(I)
    ListeTmp(I).TypeControle = TypeControle(I)
    ListeTmp(I).NomChampLie = NomChampLie(I)
    ListeTmp(I).TypeChampLie = TypeChampLie(I)
    ListeTmp(I).LongueurAutorisee = LongueurAutorisee(I)
Next

ConstruireListeDesChampsLies = True
    
Exit Function
Erreur_ConstruireListeDesChampsLies:
    Call GestionErreur(Err, "ConstruireListeDesChampsLies", Fenetre.Name)
End Function

Function AucunEnregistrement(Enregistrement As Recordset) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 20/01/98
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Enregistrement   : Objet de type Recordset qui doit faire l'objet de la v�rif   *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si l'objet Recordset est vide (pas d'enregistrement) et false si au moins  *
'*            un enregistrement est pr�sent.                                                  *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Analyser si au moins un enregistrement est pr�sent dans le Recordset pass� en   *
'*            param�tre.                                                                      *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo ErreurAucunEnregistrement

'Si aucun enregistrement n'est pr�sent
If Enregistrement.RecordCount = 0 Then
    AucunEnregistrement = True
Else
    AucunEnregistrement = False
End If
    
Exit Function
ErreurAucunEnregistrement:
    Call GestionErreur("AucunEnregistrement", Err, Enregistrement.Name)
End Function

Function InitialiserFenetreCodification(Fenetre As Form, Rs As Recordset, Optional AjoutAutomatique As Boolean) As Integer
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :                                *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre : Objet Form qui doit �tre initialis�. En principe il s'agit d'une      *
'*                      fen�tre permettant la mise � jour de codifications.                   *
'*          - Rs : Objet de type Data control auquel les contr�les de la         *
'*                              feuille sont li�s pour la mise � jour des codifications.      *
'*          - AjoutAutomatique (optionnel) : Boolean indiquant si l'on passe en mode ajout    *
'*                                           automatique qui sera eventuellement pass� � la   *
'*                                           fonction VisibiliteBoutons en cas de RecordSet   *
'*                                           vide.                                            *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si tout se d�roule correctement et false dans le cas contraire             *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Charger une feuille permettant de g�rer des codifications et initialiser cette  *
'*            feuille (passage en consultation).                                              *
'*          - Retourner une valeur au programme appelant pour indiquer si l'ensemble de       *
'*            l'initialisation s'est effectu� correctement.                                   *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_InitialiserFenetreCodification
Dim Reponse As Integer

'Place la fen�tre en mode consultation
If IsMissing(AjoutAutomatique) Then
    If Not VisibiliteBoutons(Fenetre, Rs, CONSULTATION) Then
        InitialiserFenetreCodification = False
        Exit Function
    End If
Else
    If Not VisibiliteBoutons(Fenetre, Rs, CONSULTATION, AjoutAutomatique) Then
        InitialiserFenetreCodification = False
        Exit Function
    End If
End If

InitialiserFenetreCodification = True

Exit Function
'implicitement la valeur de retour est false en cas d'erreur
Erreur_InitialiserFenetreCodification:
    Call GestionErreur(Err, "InitialiserFenetreCodification", "")
End Function

Function FenetreDejaChargee(Fenetre As String) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre : F�n�tre Form pour laquelle on doit v�rifier si elle est ouverte ou pas*
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si la fen�tre est d�j� charg�e et false dans le cas contraire             *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION : V�rifie si la fen�tre appel�e est d�j� charg�e ou pas.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_FenetreDejaChargee
Dim I As Integer

For I = 0 To Forms.Count - 1
    If UCase(Forms(I).Name) = UCase(Fenetre) Then
        FenetreDejaChargee = True
        Exit Function
    End If
Next

Exit Function
Erreur_FenetreDejaChargee:
    Call GestionErreur(Err, "FenetreDejaChargee", "")
End Function

Function ControleLieADesDonnees(ControleConcerne As Control, Enregistrement As Recordset) As Integer
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 05/02/98
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          ControleConcerne :  Objet de type Contr�le qui fera l'objet du traitement.        *
'*          Enregistrement   :  Objet de type Recordset qui sera analys�.                     *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          True    : si le contr�le pass� en param�tre est li� � un champ dont le Nom        *
'*                    correspond � un champ de l'objet Recordset pass� en param�tre           *
'*          False   : dans le cas contraire ou si le contr�le ne peut �tre li� � des donn�es  *
'*                    ce qui se traduit par une erreur 438.                                   *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          Analyser si le contr�le pass� en param�tre est li� (donn�es)� un champ de l'objet *
'*          Recordset pass� �galement en param�tre. Pour ce faire il y a une analyse de la    *
'*          propri�t� DATAFIELD du contr�le et des diff�rents champs de l'objet Recordset.    *
'*          NOTA : en cours d'ex�cution la propri�t� DATASOURCE d'un contr�le n'est pas       *
'*                 accessible (Voir commentaire dans la partie d�claration : R�gles ...       *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_ControleLieADesDonnees
'objet de type champ permettant d'analyser la collection des champs du contr�le data.
Dim Champ As Field

'Pour chaque champ du contr�le data
For Each Champ In Enregistrement.Fields
    'Si le nom de champ = Propri�t� ControlSource du contr�le pass� en param�tre.
    If ControleConcerne.ControlSource = Champ.Name Then
        'renvoi la valeur True
        ControleLieADesDonnees = True
        Exit Function
    End If
Next

'La fonction renvoie false par d�faut si on passe sur cette ligne
Exit Function
Erreur_ControleLieADesDonnees:
    Select Case Err
        'erreur r�pertori�e correspondant � la non existence de la propri�t� demand�e pour le
        'contr�le concern�. L'erreur 438 est celle couramment rencontr�e tandis que le code 2455 correspond � l'absence de la propri�t� pour un contr�le de type Treeview
        Case 438, 2455

        'erreur non g�r�e : appel proc�dure g�n�rale de gestion d'erreur
        Case Else
            Call GestionErreur(Err, "ControleLieADesDonnees", "aucun")
    End Select
End Function

Public Function RecupereListeDesChampsLies(Fenetre As Form) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre : F�n�tre Form pour laquelle on doit construire la liste des champs li�s*
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                     *
'*          - True si tout s'est bien pass� et false dans le cas contraire.                   *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION : Construire la liste des champs de la fen�tre li�s � la table     *
'*                           concern�e (ODBC ou Access)
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_RecupereListeDesChampsLies

RecupereListeDesChampsLies = Fenetre.RenvoiListeDesChampsLies

Exit Function
Erreur_RecupereListeDesChampsLies:
    Call GestionErreur(Err, "RecupereListeDesChampsLies", "")
End Function

Function EnregistrementSupprime(Enregistrement As Recordset, Position As String) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 05/02/98
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Enregistrement    : Objet Recordset qui fait l'objet du traitement.             *
'*          - Position          : Chaine contenant un BookMark identifiant un enregistrement. *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si l'enregistrement correspondant � la chaine contenue dans Position a �t� *
'*            supprim� et false si l'enregistrement correspondant au bookmark contenu dans    *
'*            position est toujours pr�sent dans le recordset pass� en param�tre.             *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Tester si un enregistrement identifi� par la propri�t� BookMark a �t� supprim�  *
'*            de l'objet Recordset qui le contient. Cette fonction est appel�e pour savoir si *
'*            le programmeur a trait� la m�thode Delete dans son code ou si la fonction       *
'*            VisibiliteBoutonsCodifications doit s'en charger.                               *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_EnregistrementSupprime

'Essaie de se repositionner sur l'enregistrement test�.
Enregistrement.Bookmark = Position
'Si la commande ci dessus aboutit l'enregistrement n'a pas �t� supprim� dans le cas contraire une erreur sera d�clench�e
EnregistrementSupprime = False

Exit Function
Erreur_EnregistrementSupprime:
    Select Case Err
        'L'enregistrement a �t� supprim�.
        Case 3167
            EnregistrementSupprime = True

        Case Else
            Call GestionErreur(Err, "EnregistrementSupprime", "")
            EnregistrementSupprime = False
    End Select
End Function

Function VerificationTailleEtTypeChamp(Fenetre As Form) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 13/03/2017
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre   : objet de type form dont les contr�les seront analys�s               *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si tous les contr�les li�s � un champ de l'objet Recordset ont une taille  *
'*            et un type corrects. False si au moins un contr�le li� contient une information*
'*            dont la taille est incoh�rente avec le champ concern� (trop longue) ou si le    *
'*            type est incorrect (date, num�rique ...)
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - V�rifier qu'une zone de saisie ne contient pas une information trop longue pour *
'*            �tre enregistr�e dans la base de donn�es.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :  S. GUIWARCH                                                *
'*                      Date    :  D�cemebre 2016                                             *
'*                      Objet   :  Prise en compte du type dbmemo (champs texte > 255 car     *
'*          MODIF2 :    Auteur  :  S. GUIWARCH                                                *
'*                      Date    :  13/03/2017                                                 *
'*                      Objet   :  Prise en compte de la valeur z�ro pour la variable
'*                              LongueurAutorisee lorsque la taille du champ est illimit�e.
'*                              (Cf. MODIF2 de la fonction ConstruireListeDesChampsLies).
'**********************************************************************************************
On Error GoTo Erreur_VerificationTailleEtTypeChamp
Dim ObjetControl As Control
Dim Longueur As Integer
Dim TypeChamp As Integer
Dim I As Integer

If Not RecupereListeDesChampsLies(Fenetre) Then
    MsgBox "Impossible de r�cup�rer la liste des champs li�s"
Else
    For I = 1 To UBound(ListeTmp)
        With ListeTmp(I)
            Select Case .TypeChampLie
                Case dbText 'Le champ doit �tre de type texte
                    Select Case .TypeControle
                        Case C_TEXT
                            'Ces lignes sont comment�es car Access limite tout seul le nombre de caract�res que l'on peut saisir lorsque le champ de la BDD � une longueur max
                            ' de 255 caract�res.
                            
                            'If Len(Fenetre.Controls(.NomControle).Value) > .LongueurAutorisee Then
                            '    MsgBox "le champ " + .NomControle + " ne peut contenir que " + Str$(.LongueurAutorisee) + " caract�re"
                            '    Fen�tre.Controls(.NomControle).SetFocus
                            '    Fen�tre.Controls(.NomControle).SelLength = Len(Fenetre.Controls(.NomControle))
                            '    VerificationTailleEtTypeChamp = False
                            '    Exit Function
                            'End If

                        Case C_DBCOMBO

                        'Rien en attendant de mieux traiter les cases � cocher.
                        Case 0

                        Case Else
                            MsgBox "V�riftaille � adapter : Type du contr�le non g�r�"
                    End Select

                'Le champ doit �tre num�rique
                Case dbLong, dbInteger, dbSingle, dbDouble, dbByte, dbCurrency
                    Select Case .TypeControle
                        Case C_TEXT
                            If Not IsNumeric(Fenetre.Controls(.NomControle).Value) Then
                                'On tol�re la valeur nulle - � am�liorer en fonction de la r�alit� dans le sgbd
                                If Not (IsNull(Fenetre.Controls(.NomControle).Value)) Then
                                    MsgBox "le champ " + .NomControle + " doit �tre num�rique"
                                    VerificationTailleEtTypeChamp = False
                                    Exit Function
                                End If
                            End If

                        Case C_DBCOMBO

                        Case Else
                            MsgBox "V�riftaille � adapter : Type du contr�le non g�r�"
                    End Select

                'Notamment Champ de type OLE
                Case dbLongBinary
                    If .TypeControle <> C_OLE Then
                        MsgBox "le champ " + .NomControle + " doit de type OLE"
                        VerificationTailleEtTypeChamp = False
                        Exit Function
                    End If

                'Le champ doit �tre de type date
                Case dbDate
                    Select Case .TypeControle
                        Case C_TEXT
                            If Not IsDate(Fenetre.Controls(.NomControle).Value) Then
                                If Fenetre.Controls(.NomControle).Value <> "" Then
                                    MsgBox "le champ " + .NomControle + " doit contenir une date"
                                    VerificationTailleEtTypeChamp = False
                                    Exit Function
                                End If
                            End If

                         Case C_DBCOMBO

                         Case Else
                            MsgBox "V�riftaille � adapter : Type du contr�le non g�r�"
                    End Select

                'Ce type est retourn� pour une utilisation ODBC direct uniquement - Les cl�s externes postgresql bigint avec comme source un serail sont vues en dbdecimal.
                'Aucun contr�le ici.
                Case dbDecimal

                Case dbBoolean

                'Le champ doit �tre de type texte long
                'REMARQUE : Un champ varchar (sans longueur max) sous postgres sera consid�r� dans ACCESS comm un texte court d'une longueur maximale de 255 charact�res
                'un champ varchar(300) sous postgres sera consid�r� dans ACCESS comme un texte long avec une longueur maximale de 300 charact�res
                'un champs text sous postgres sera consid�r� dans ACCESS comme un texte long sans limite de charact�res
                'On a fait le choix positionner la valeur de la variable LongueurAutoris�e � 0
                'lorsque la longueur du champ d�finit pas le sgbd n'est pas renseign�e (donc chaine de caract�res illimit�e
                Case dbMemo
                    If .LongueurAutorisee <> 0 Then
                        If Len(Fenetre.Controls(.NomControle).Value) > .LongueurAutorisee Then
                            MsgBox "le champ " + .NomControle + " ne peut contenir que " + Str$(.LongueurAutorisee) + " caract�res"
                            Fenetre.Controls(.NomControle).SetFocus
                            Fenetre.Controls(.NomControle).SelLength = Len(Fenetre.Controls(.NomControle))
                            VerificationTailleEtTypeChamp = False
                            Exit Function
                        End If
                    End If

                Case Else
                    MsgBox "V�riftaille � adapter : Type du champ non g�r�" & Chr$(13) & ListeTmp(I).TypeChampLie
            End Select
        End With
    Next
End If

VerificationTailleEtTypeChamp = True

Exit Function
Erreur_VerificationTailleEtTypeChamp:
    Call GestionErreur(Err, "VerificationTailleEtTypeChamp :" + ObjetControl.Name, "")
End Function

Public Function VisibiliteBoutons(Fenetre As Form, Rs As Recordset, BoutonClique As Integer, Optional AjoutAutomatique As Boolean) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 27/07/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre   : Objet Form pour lequel la visibilit� des boutons standards de mise  *
'*                        � jour de donn�es doit �tre modifi�e.                               *
'*          - Rs   : Objet de type contr�le data auquel les contr�les de la                   *
'*                                feuille sont li�s pour la mise � jour des codifications.    *
'*          - BoutonClique  : Index du bouton qui a �t� cliqu� dans la fen�tre                *
'*          - AjoutAutomatique (optionnel) : Boolean indiquant si l'on passe en mode ajout    *
'*                                           automatique qui sera eventuellement pass� � la   *
'*                                           fonction VisibiliteBoutons en cas de RecordSet   *
'*                                           vide.                                            *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si l'op�ration a pu �tre men�e � son terme sans probl�me et false dans le  *
'*            cas contraire.                                                                  *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Adapter l'interface d'une feuille de mise � jour de codification (visibilit� et *
'*            acc�s aux contr�les. Elle est appel�e dans la proc�dure �v�nementielle click du *
'*            groupe de bouton (voir InitialiserLesBoutonsCodifications). En fonction du      *
'*            contexte elle appelera une fonction ControleValidite de la feuille avec comme   *
'*            param�tre le type de mise � jour � appliquer aux donn�es. Cette autre fonction  *
'*            peut effectuer des contr�les sp�cifiques aux codifications trait�es, effectuer  *
'*            la mise � jour des donn�es (M�thode Update,delete ... de l'objet Recordset      *
'*            concern�) et doit retourner une valeur bool�enne indiquant si la fonction       *
'*            VisibiliteBoutonsCodifications peut continuer son traitement normalement ou si  *
'*            l'Contr�leDonn�es en cours ne peut �tre mis � jour avec les valeurs saisies;    *
'*            Cependant cette fonction ControleValidite peut laisser la fonction visibilt�Bou-*
'*            -tons effectuer les mises � jour (Delete, update ...) et traiter les *
'*            erreurs �ventuelles.                                                            *
'*            La visibilit� des boutons et les acc�s aux diff�rentes zones seront alors       *
'*            adapt�s au nouveau contexte.                                                    *
'*            La fonction Visibilit�... appelera ensuite une fonction Interface de la feuille *
'*            avec comme param�tre le nouveau contexte (MISE_A_JOUR ou Consultation). Cette     *
'*            fonction pourra alors adapter l'interface de mani�re sp�cifique � la feuille.   *
'*                                                                                            *
'*      ORGANIGRAMME :    EXEMPLE                                                             *
'*            Une fen�tre de codification voit son bouton "Mise � jour" cliqu�                *
'*            L'�v�nement Click("index du bouton 'Mise � jour' qui correspond � la constante  *
'*                              MISE_A_JOUR) survient.                                          *
'*            La function VisibiliteBoutons(MISE_A_JOUR) est appel�e               *
'*            Notre fonction teste alors quel bouton a �t� cliqu� (param�tre)                 *
'*            Elle appelle ensuite la fonction Fen�tre.ControleValidite(MISE_A_JOUR)            *
'*            Si le r�sultat est juste on teste si la mise � jour a �t� effectu�e au niveau de*
'*                                     la feuille.On effectue �ventuellement les M.A.J et on  *
'*                                     traite les erreurs.
'*              sinon on laisse l'op�rateur effectuer les corrections.
'*
'*            Une autre approche consisterait � laisser la feuille g�rer compl�tement sa mise *
'*            � jour de donn�es (ajout, suppression, annulation ...) et � appeler ensuite la  *
'*            fonction pr�sente pour ne g�rer que l'affichage. Cependant il faudrait dans     *
'*            chaque feuille de codification g�rer l'annulation , la mise � jour ... et savoir*
'*            ensuite dans quelle contexte on doit travailler; d'un projet � l'autre certains *
'*            �v�nements ne serait pas g�r� de la m�me mani�re. (Par exemple en cas d'erreur  *
'*            lors d'une mise � jour repasse-t-on en consultation ou pas? l'annulation d'une  *
'*            saisie en cours remet-elle tous les champs aux valeurs du recordset champ par   *
'*            champ ou utilise-t-on la m�thode UpdateControls du recordset.                   *
'*            Dans la solution pr�sente la fonction ControleValidite propre � chaque feuille  *
'*            est libre d'effectuer ou non les contr�les et/Ou mises � jour.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :  BENARD                                                     *
'*                      Date    :  17/05/2000                                                 *
'*                      Objet   :  Ajout d'un param�tre optionnel 'AjoutAutomatique'    *
'*                                 qui indique si l'on passe en mode cr�ation automatiquement *
'*                                 sans question utilisateur lorsque le RecordSet est vide.   *
'*                                 Cette modification va de pair avec celle de la fonction    *
'*                                 appelante InitialiserFenetreCodification.
'*
'*          MODIF2 :    Auteur  :  S. GUIWARCH
'*                      Date    :  27/07/2018
'*                      Objet   :  Modification du titre du formulaire en fonction du bouton
'*                                 cliqu� (CONSULTATION, AJOUTER, MODIFIER) cette modification
'*                                 se base sur :
'*                                  - la nouvelle constante de ce module : NOM_TITRE,
'*                                    correspondant au nom du controle du formulaire contenant
'*                                     le titre.
'*                                  - la variable publique du formulaire : NomFenetre, qui est
'*                                    utilis� comme titre pour le mode CONSULTATION.
'*                                  - la nouvelle variable publique : NomFenetreSingulier, qui
'*                                    est concat�n� avec "Ajout " pour le mode AJOUTER ou alors
'*                                    avec "Modification " pour le mode MODIFIER.
'**********************************************************************************************
On Error GoTo Erreur_VisibiliteBoutons
Dim PositionEnregistrementAvantDelete As String
Dim ObjetControl As Control
Dim Reponse As Integer
Dim ModeSynchro As Integer

'Initialisation du mode de synchronisation
ModeSynchro = SYNC_AUCUN

'test quel bouton (MODIFIER,ANNULER ...) a �t� cliqu�
Select Case BoutonClique
    'si le bouton Modifier a �t� cliqu�. Cel� se produit uniquement lorsque l'on se trouvait pr�c�demment en consultation.
    Case MODIFIER
        'On autorise les modifications
        Fenetre.AllowEdits = True
        'Les boutons passent en mise � jour
        Fenetre.BTN_Sauvegarder.Visible = True
        Fenetre.BTN_Ajouter.Visible = False
        Fenetre.BTN_Supprimer.Visible = False
        Fenetre.BTN_Annuler.Visible = True
        Fenetre.BTN_Annuler.SetFocus ' Car on ne peut d�sactiver le contr�le Actif
        Fenetre.BTN_Quitter.Visible = True
        Fenetre.BTN_Modifier.Visible = False

        ' On modifie le titre du formulaire.
        Fenetre.Controls(NOM_TITRE).Caption = "Modification " & Fenetre.NomFenetreSingulier

        'Fen�tre.BTN_Sauvegarder.Default = True
        'La modification est autoris�e
        Rs.Edit

        'Donne la main � la feuille pour une �ventuelle adaptation de l'interface
        Call Fenetre.Interface(MODIFIER)

    'Si le bouton Mise � jour a �t� cliqu�. Cel� se produit � partir du mode mise � jour ou cr�ation d'un Contr�leDonn�es
    Case MISE_A_JOUR
        Screen.MousePointer = 11
        'La fonction sp�cifique � la fen�tre est charg�e d'effectuer le traitement sur les donn�es.
        If VerificationTailleEtTypeChamp(Fenetre) Then
            'si le contr�le sp�cifique au niveau feuille est correct
            If Fenetre.ControleValidite(BoutonClique) Then
                Select Case Rs.EditMode
                    'La fonction ControleValidite de la fen�tre a pu effectuer la mise � jour et traiter les erreurs. Ceci donne de la
                    'souplesse au programmeur qui peut ou non valider lui-m�me ses modifications.
                    Case dbEditNone
                          ModeSynchro = SYNC_AJOUT_REALISE

                    Case dbEditAdd
                        'rs.Update
                        'Call Fen�tre.Valide
                        'RS.MoveNext
                        'RS.MovePrevious
                        
                        'Au moins un champ de saisie a �t� modifi�
                        If Fenetre.Dirty Then
                            DoCmd.RunCommand acCmdSaveRecord
                            ModeSynchro = SYNC_AJOUT_REALISE
                        'Aucune saisie
                        'Les 5 lignes ci-apr�s sont ises en commentaire par ALain Benard le 08/03/2022 car elles posent probl�me
                        'Else
                        '    MsgBox "Vous devez saisir des informations avant de sauvegarder"
                        '    VisibiliteBoutons = False
                        '    Screen.MousePointer = 0
                        '    Exit Function
                        End If

                    Case dbEditInProgress
                        'RS.Update
                        'Call Fen�tre.Valide
                        'RS.MoveNext
                        'RS.MovePrevious

                        DoCmd.RunCommand acCmdSaveRecord
                        ModeSynchro = SYNC_MODIF_REALISEE

                    'Case dbEditAdd 'L'enregistrement courant reste le dernier enregistrement s�lectionn�
                    'Mis en commentaire est remplac� par la ligne d''apr�s car en cas de violation de contrainte d''unicit� Access plante.
                        'rs.MoveLast 'Positionnement sur le dernier enregistrement (celui qui vient d'�tre cr��)
                                    'Ce d�placement valide l'enregistrement.
                        'DoCmd.RunCommand acCmdSaveRecord

                    Case Else
                        MsgBox "editmode inconnu"
                End Select

                If Not VisibiliteBoutons(Fenetre, Rs, CONSULTATION) Then
                    VisibiliteBoutons = False
                    Screen.MousePointer = 0
                    Exit Function
                End If
            Else
                VisibiliteBoutons = False
                Screen.MousePointer = 0
                Exit Function
            End If
        'Le contr�le de taille et type des champs a d�tect� une erreur.
        Else
            VisibiliteBoutons = False
            Screen.MousePointer = 0
            Exit Function
        End If

        Fenetre.Sync ModeSynchro

    'Si le bouton Ajouter a �t� cliqu�. Cel� se produit uniquement lorsque l'on se trouvait pr�c�demment en consultation.
    Case AJOUTER
        'Cr�ation d'un Contr�leDonn�es vierge
        Rs.AddNew

        'Car on ne peut d�sactiver le contr�le Actif
        Fenetre.BTN_Quitter.SetFocus
        Fenetre.BTN_Modifier.Visible = False
        Fenetre.BTN_Sauvegarder.Visible = True
        'Car on ne peut d�sactiver le contr�le Actif
        'Fen�tre.BTN_Sauvegarder.SetFocus
        Fenetre.BTN_Ajouter.Visible = False
        Fenetre.BTN_Supprimer.Visible = False
        Fenetre.BTN_Annuler.Visible = True
        Fenetre.BTN_Quitter.Visible = True
        
        ' On modifit le titre du formulaire.
        Fenetre.Controls(NOM_TITRE).Caption = "Ajout " & Fenetre.NomFenetreSingulier
        
        'Donne la main � la feuille pour une �ventuelle adaptation de l'interface
        Call Fenetre.Interface(AJOUTER)

    'Si le bouton Supprimer a �t� cliqu�. Cel� se produit uniquement lorsque l'on se trouvait pr�c�demment en consultation.
    Case SUPPRIMER
        Screen.MousePointer = 11
        Reponse = MsgBox("Confirmez-vous la suppression?", vbYesNo)
        
        'Si l'op�rateur confirme la suppression
        If Reponse = vbYes Then
            PositionEnregistrementAvantDelete = Rs.Bookmark
            'si le contr�le sp�cifique au niveau feuille est correct. Laisse la possibilit� au programmeur de lancer la methode delete et de la g�rer compl�tement.
            If Fenetre.ControleValidite(BoutonClique) Then
                'si le programmeur n'a pas effectu� le delete au niveau de la feuille
                If Not EnregistrementSupprime(Rs, PositionEnregistrementAvantDelete) Then
                    Rs.Delete
                End If
                Rs.MoveFirst
                'rafraichit le contr�le donn�es
                'Contr�leDonn�es.Refresh
            'ControleValidite a renvoy� False
            Else
                VisibiliteBoutons = False
                Screen.MousePointer = 0
                Exit Function
            End If

            If Not VisibiliteBoutons(Fenetre, Rs, CONSULTATION) Then
                'On force � true car ce n'est pas la suppression qui est en d�faut.
                VisibiliteBoutons = True
                Exit Function
            End If

            'A ce stade la suppression a �t� effectu�e et le recordset du formulaire est positionn� sur le 1� enregistrement ?? (bug movefirst si suppression dernier??)
            Fenetre.Sync SYNC_SUPPR_REALISEE
            
            'If Not AucunEnregistrement(Rs) Then
            '    Fenetre.Sync False, SUPPRIMER
            'End If
        End If
        Screen.MousePointer = 0

    Case ANNULER
        'si on �tait en ajout sur une table vide
        If AucunEnregistrement(Rs) Then
            Rs.CancelUpdate
            Fenetre.Undo
            'Unload Fen�tre impossible de d�charger le formulaire Access2007 actif de cette fa�on
            DoCmd.Close acForm, Fenetre.Name, acSaveNo
        Else
            'V�rifie que l'on a bien une modif en cours
            Select Case Rs.EditMode
                Case dbEditAdd
                    'Remet les champs �cran avec les valeurs contenues dans le Contr�leDonn�es courant.
                    'Contr�leDonn�es.UpdateControls

                     Rs.CancelUpdate
                     Fenetre.Undo
                     Rs.MoveFirst
                     ModeSynchro = SYNC_AJOUT_ANNULE

                Case dbEditInProgress
                     Rs.CancelUpdate
                     Fenetre.Undo

                Case Else
            End Select

            If Not VisibiliteBoutons(Fenetre, Rs, CONSULTATION) Then
                VisibiliteBoutons = False
                Exit Function
            End If

            Fenetre.Sync ModeSynchro
         End If
     Case QUITTER
        'Si une modification ou une saisie est en cours
        If Rs.EditMode <> dbEditNone Then
            Rs.CancelUpdate
            Fenetre.Undo
        End If
        'Unload Fenetre
        DoCmd.Close

    'Notamment � l'initialisation de la fen�tre
    Case CONSULTATION
        If AucunEnregistrement(Rs) Then
            If DroitsUtilisateur = DROIT_ECRITURE Then
                If IsMissing(AjoutAutomatique) Then
                    Reponse = MsgBox("La liste est vide. Voulez-vous ajouter un enregistrement?", vbYesNo)
                Else
                    'on pose la question
                    If AjoutAutomatique = False Then
                        Reponse = MsgBox("La liste est vide. Voulez-vous ajouter un enregistrement?", vbYesNo)
                    'on force la r�ponse
                    Else
                        Reponse = vbYes
                    End If
                End If
                If Reponse = vbYes Then
                    If Not VisibiliteBoutons(Fenetre, Rs, AJOUTER) Then
                        VisibiliteBoutons = False
                        Exit Function
                    Else
                        VisibiliteBoutons = True
                        'permet de sortir sans ex�cuter la fin (r�curence)
                        Exit Function
                    End If
                Else
                    'car le d�roulement ne peut se poursuivre sur une 'table' vide (d�chargement du formulaire)
                    VisibiliteBoutons = False
                    'Unload Fen�tre impossible de d�charger le formulaire Access2007 actif de cette fa�on
                    DoCmd.Close acForm, Fenetre.Name, acSaveNo
                    MsgBox "Le formulaire " & Fenetre.NomFenetre & " ne peut �tre utilis� sans au moins un enregistrement."
                    Exit Function
                End If
            'Pas de droit d'�criture - on d�charge le formulaire car aucun enregistrement � afficher
            Else
                'car le d�roulement ne peut se poursuivre sur une 'table' vide (d�chargement du formulaire)
                VisibiliteBoutons = False
                'Unload Fen�tre impossible de d�charger le formulaire Access2007 actif de cette fa�on
                DoCmd.Close acForm, Fenetre.Name, acSaveNo
                MsgBox "Le formulaire " & Fenetre.NomFenetre & " ne peut �tre utilis� sans au moins un enregistrement. La table est vide et vous ne disposez pas du droit d'ajout."
                Exit Function
            End If
        Else
            'On emp�che la modification
            Fenetre.AllowEdits = False

            ' On modifie le titre du formulaire.
            Fenetre.Controls(NOM_TITRE).Caption = Fenetre.NomFenetre

            If DroitsUtilisateur <> DROIT_LECTURE Then
                Fenetre.BTN_Modifier.Visible = True
                'Car on ne peut d�sactiver le contr�le Actif
                Fenetre.BTN_Modifier.SetFocus
                Fenetre.BTN_Sauvegarder.Visible = False
                Fenetre.BTN_Ajouter.Visible = True
                Fenetre.BTN_Supprimer.Visible = True
                Fenetre.BTN_Annuler.Visible = False
                Fenetre.BTN_Quitter.Visible = True
            Else
                Fenetre.BTN_Quitter.SetFocus
                Fenetre.BTN_Ajouter.Visible = False
                Fenetre.BTN_Modifier.Visible = False
                Fenetre.BTN_Annuler.Visible = False
                Fenetre.BTN_Sauvegarder.Visible = False
                Fenetre.BTN_Supprimer.Visible = False
            End If
        End If

        Screen.MousePointer = 0
        Call Fenetre.Interface(CONSULTATION)

    'le bouton cliqu� n'est pas connu? Les constantes ont peut �tre �t� modifi�es
    Case Else
        MsgBox "VisibiliteBoutons : Param�tre boutoncliqu� non valide"
        VisibiliteBoutons = False
        Exit Function
End Select

VisibiliteBoutons = True

Exit Function
Erreur_VisibiliteBoutons:
     Screen.MousePointer = 0
     Call GestionErreur(Err, "VisibiliteBoutons", "param�tres")
End Function

Function GenereIdentifiant(NomTable As String, ChampCle As String) As Long
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 21/03/2014
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*              - NomTable  : Nom de la table pour laquelle on doit g�n�rer un identifiant    *
'*                            d'enregistrement.                                               *
'*              - ChampCle  : Nom du champ qui constitue la cl� primaire de la table          *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - Entier long correspondant � un identifiant valide pour cr�er un enregistrement  *
'*            dans la table pass�e en param�tre NomTable. En cas d'erreur la valeur 0 est     *
'*            retourn�e.                                                                      *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA FONCTION  :                                                                 *
'*          G�n�rer un identifiant (en mode deconnecte) pour cr�er un nouvel enregistrement   *
'*          dans une table. En l'absence de type autoincrement cette fonction regarde qu'elle *
'*          est le plus grand identifiant pr�sent dans la table pass�e en param�tre et        *
'*          retourne cette valeur incr�ment�e de 1 ou 1 000 000 000 (1 milliard) si cette     *
'*          valeur est inf�rieure � 1 milliard. Un fonctionnement affin� bas� sur une table de*
'*          servitude pourra �tre impl�ment� ult�rieurement.                                  *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_GenereIdentifiant
Dim Rs As Recordset
Dim Id As Long

Set Rs = Application.CurrentDb.OpenRecordset("SELECT MAX (" + ChampCle + ") as identifiant FROM " + NomTable)
'Ce test d�tecte le cas o� la table cible est vide
If IsNull(Rs.Fields("identifiant").Value) Then
    GenereIdentifiant = ID_MIN_DECONNECTE
Else
    Id = Rs.Fields("identifiant").Value
    If Id < ID_MIN_DECONNECTE Then
        GenereIdentifiant = ID_MIN_DECONNECTE
    Else
        GenereIdentifiant = Id + 1
    End If
End If

Exit Function
Erreur_GenereIdentifiant:
    Call GestionErreur(Err, "GenereIdentifiant", NomTable + "  " + ChampCle)
    GenereIdentifiant = 0
End Function

Sub PersonnalisationLabel(Fenetre As Form, IgnoreInfosBulles As Boolean)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 27/07/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*                  - Fenetre : Objet Form que l'on vient d'ouvrir et pour lequel il faut
'*                    personnaliser la couleur des labels pour les champs obligatoires.
'*                  - IgnoreInfosBulles : Boolean indiquant si on affecte aux label une
'*                    info-bulle correspondant � la description du champ concern� dans la BDD.
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE : Personnaliser la couleur des labels pour les champs obligatoires,
'*                            ainsi que des champs facultatifs.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  : S. GUIWARCH
'*                      Date    : 27/08/2018
'*                      Objet   : Execution de la requ�te "GetFieldDescription", puis
'*                                affectation de la valeur r�cup�r�e ou d'une valeur par d�faut
'*                                � la propri�t� info-bulle du label concernant le controle
'*                                correspondant au champs de la base de donn�es.
'**********************************************************************************************
On Error GoTo Erreur_PersonnalisationLabel
Dim ObjetControl As Control
Dim Field As Field
Dim Schema, Table As String
Dim ResultatRequete As Recordset

For Each ObjetControl In Fenetre.Controls
    If TypeName(ObjetControl) = "Label" Then
        For Each Field In Fenetre.Recordset.Fields
            If ObjetControl.Parent.Name = Field.Name Then
                'On personnalise le label selon s'il est obligatoire ou non.
                If Field.Required Then
                    ObjetControl.ForeColor = COULEUR_LABEL_OBLIGATOIRE
                Else
                    ObjetControl.ForeColor = COULEUR_LABEL_OPTIONNEL
                End If

                'Si le param�tre IgnoreInfosBulles est a false on r�cup�re la description du champs dans la BDD puis on l'affecte � la propri�t� ControlTipText
                If Not IgnoreInfosBulles Then
                    'On r�cup�re le nom du sch�ma.
                    Schema = Split(Fenetre.RecordSource, "_", 2)(0)
                    'On r�cup�re le nom de la table.
                    Table = Split(Fenetre.RecordSource, "_", 2)(1)
                    'On ex�cute la requ�te permettant d'obtenir la description du champs dans la base de donn�es.
                    Set ResultatRequete = ExecuteRequeteParametre("GetFieldDescription", Array(Schema, Table, Field.Name), TYPE_PARAM_STRING)
                    'Si le recordset ne contient aucun enregistrement cela signifie que la description du champ n'a pas �t� renseigner dans la base de donn�es.
                    If ResultatRequete.RecordCount = 0 Then
                        ObjetControl.ControlTipText = "Aucune description pour ce champ."
                    Else
                        'ObjetControl.ControlTipText = ResultatRequete.Fields(0)
                        '''' Bug  : si la taille du commentaire de champs sous postgres exc�de 262 une erreur est d�clench�e
                        ObjetControl.ControlTipText = Left(ResultatRequete.Fields(0), 255)
                        
                    End If
                End If
            End If
        Next
    End If
Next

Exit Sub
Erreur_PersonnalisationLabel:
    Call GestionErreur(Err, "Personnalisationlabel", "")
End Sub

Public Function GlobalSync(Fenetre As Form, MajDbList As Boolean, MajSousForm As Boolean, MajLstChoix As Boolean, TypeSynchro As Integer, PremierListe As Boolean) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 13/10/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - Fenetre : Objet Form en cours d'utilisation                                     *
'*          - MajDbList   : Boolean indiquant si le rafraichissement des listes d�roulantes *
'*                            du formulaire doit �tre ex�cut�. (hors liste de s�lection)      *
'*          - MajSousForm : Boolean indiquant si le rafraichissement des sous-formualaires  *
'*                            du formulaire doit �tre ex�cut�.                                *
'*          - MajLstChoix : Boolean indiquant si le rafraichissement de la liste de         *
'*                            s�lection du formulaire doit �tre ex�cut�.                      *
'*          - TypeSynchro  : Entier � choisir parmi les constantes suivantes :               *
'*                  - AUCUNE_SYNCHRO        : rien � faire                                    *
'*                  - SYNCHRO_SUR_LISTE       : synchronisation des donn�es du formulaire selon *
'*                            ce qui est s�lectionn� dans la liste.                           *
'*                  - SYNCHRO_SUR_FORMULAIRE  : synchronisation de la liste de s�lection selon  *
'*                            ce qui est affich� sur le formulaire.                           *
'*          - PremierListe : Boolean indiquant si la liste de s�lection doit �tre repositio- *
'*                            nn�e sur son premier �l�ment.                                   *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE : Synchroniser l'ensemble des informations pr�sentes � l'�cran
'*                            (sous-formulaires, listes d�roulantes, liste de s�lection ...)
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :   Alain Benard                                              *
'*                      Date    :   09/05/2017                                                *
'*                      Objet   :   R��criture pour plus de souplesse dans la combinaisons    *
'*                                  des traitements � ex�cuter                                *
'*                                                                                            *
'**********************************************************************************************
On Error GoTo Erreur_GlobalSync
Dim ObjetControl As Control
Dim Rs As DAO.Recordset
Dim TableauId As Variant
'Variable aliment�e avec le N� de la colonne li�e de la liste de s�lection - L'utilisation d'une variable au lieu de la propri�t� permet une meilleure lisibilit�
Dim ColonneLiee As Integer
Dim I As Integer
'Variable utilis�e pour indiquer que la liste d�roulante et les donn�es de formulaire sont d�synchronis�es, n�cessitant un resynchronisation,
Dim Desynchronise As Boolean

'Initiliasation de la variable
Desynchronise = False
'Analyse la n�cessit� de rafra�chir certains type de contr�le au vu des 3 param�tres maj_lst_choix, maj_lst_choix et maj_sous_form
For Each ObjetControl In Fenetre.Controls
    'le contr�le est une liste d�roulante
    If "ComboBox" = TypeName(ObjetControl) Then
        'Si le contr�le analys� est la liste de s�lection
        If (ObjetControl.Name = NOM_LISTE_CHOIX) Then
            'Si le rafraichissement de la liste de s�lection est souhait�
            If MajLstChoix Then
                ObjetControl.Requery
                DoEvents
                'Le code ci-dessous force le rechargement car l'on constate des dysfonctionnements suite � Requery
                ' On copie le recordset de la liste dans la variable rs
                'Set Rs = CurrentDb.OpenRecordset(Fen�tre.Controls(NOM_LISTE_CHOIX).RowSource)
                'Puis on lui re-affecte, ceci permet par la suite d'effectuer un FindFirst sur le recordset.
                'Si on ne le fait pas le recordset aura un repositionnement douteux ce qui aboutira � de possibles
                'd�synchronisation entre le contenu de la liste de s�lection et les donn�es affich�es dans le formulaire
                'Set Fen�tre.Controls(NOM_LISTE_CHOIX).Recordset = Rs
            End If
        Else
            'Le rafra�chissement des listes d�roulantes est souhait�
            If MajDbList Then
                ObjetControl.Requery
            End If
        End If
    'Le contr�le n'est pas une liste d�roulante
    Else
        If "SubForm" = TypeName(ObjetControl) And MajSousForm Then
            'Si le sous-formulaire est dans un �tat o� une modification en cours n'est pas encore valid�e on ne le met pas � jour
            If Not ObjetControl.Form.Dirty Then
                ObjetControl.Requery
                If Not AucunEnregistrement(ObjetControl.Form.Recordset) Then
                    ObjetControl.Form.Recordset.MoveFirst
                End If
            End If
        End If
    End If
Next

'Le repositionnement dans la liste de s�lection est activ�.
If PremierListe Then
    Fenetre.Controls(NOM_LISTE_CHOIX).Value = RetourneValeurLieePremierElement(Fenetre.Controls(NOM_LISTE_CHOIX))
End If

'on teste la valeur et le type de la variable NomIdSerial du formulaire, si elle n'est pas valide on sort de la fonction
If Not NomIdSerialEstUnTableau(Fenetre) Then
    GlobalSync = True
    Exit Function
Else
    'Cette ligne est n�cessaire car Fen�tre.NomIdSerial ne peut �tre utilis� directement comme un tableau
    TableauId = Fenetre.NomIdSerial
End If

Select Case TypeSynchro
    Case AUCUNE_SYNCHRO
        'Rien � faire

    'On positionne le recordset du formulaire en fonction de l'�l�ment s�lectionn� dans la liste de choix
    Case SYNCHRO_SUR_LISTE
        If Not AucunEnregistrement(Fenetre.Recordset) Then
            Fenetre.Recordset.FindFirst (GetClauseWhere(Fenetre, False))
        End If

    'On positionne le recordset de la liste de choix en fonction des donn�es du formulaire.
    Case SYNCHRO_SUR_FORMULAIRE
        'Fen�tre.Controls(NOM_LISTE_CHOIX).Recordset.FindFirst (GetClauseWhere(Fen�tre, True)) 'La m�thode FindFirst apr�s le requery donne des r�sultats tr�s incertains
        'obligeant � trouver d'autres solutions que le cheminemement naturel (FindFirst sur le recordset de la liste d�roulante puis positionnement de la liste d�roulante
        'au regard de la position de son recordset [propri�t� AbsolutePosition qui donne �galement des r�sultats non satisfaisants])
        'L'instruction ci-apr�s pose probl�me et obligerai � d�commenter le code Set rs = CurrentDb.OpenRecordset(Fen�tre.Controls(NOM_LISTE_CHOIX).RowSource) plus haut
        'dans cette fonction mais ce code pose lui-m�me un probl�me d'�norme lenteur si la liste de s�lection est volumineuse.
        'Fenetre.Controls(NOM_LISTE_CHOIX).Value = Fenetre.Controls(NOM_LISTE_CHOIX).ItemData(Fenetre.Controls(NOM_LISTE_CHOIX).Recordset.AbsolutePosition)

        'La cl� est constitu� d'un seul champ
        If UBound(Fenetre.NomIdSerial) = 0 Then
            'La colonne li�e de la liste de s�lection doit �tre celle qui contient la cl� primaire
            'On positionne la liste en fonction du contenu du champ de formulaire qui contient la cl� primaire de la table
            Fenetre.Controls(NOM_LISTE_CHOIX).Value = Fenetre.Controls(TableauId(0)).Value
        'La cl� de la table � g�rer n'est pas constitu�e par un unique champ (par exemple pour les tables de jointure)
        Else
            For I = 0 To UBound(TableauId)
                'Fenetre.Controls(NOM_LISTE_CHOIX).RowSource = Fenetre.Controls(NOM_LISTE_CHOIX).RowSource
                'MsgBox Fenetre.Controls(NOM_LISTE_CHOIX).Column(I, 1) & " - " & Fenetre.Controls(TableauId(I)).Value
                If Not (Fenetre.Controls(TableauId(I)).Value = Fenetre.Controls(NOM_LISTE_CHOIX).Recordset.Fields(Fenetre.Controls(TableauId(I)).Name).Value) Then
                    Desynchronise = True
                    Exit For
                End If
            Next

            'Il est n�cessaire de resynchronsier liste et formulaire
            If Desynchronise Or IsNull(Fenetre.Controls(NOM_LISTE_CHOIX).Column(0)) Then
                ColonneLiee = Fenetre.Controls(NOM_LISTE_CHOIX).BoundColumn
                Set Rs = Fenetre.Controls(NOM_LISTE_CHOIX).Recordset
                Rs.FindFirst GetClauseWhere(Fenetre, True)
                'Aucun enregistrement trouv�
                If Rs.NoMatch Then
                    MsgBox "La synchronisation de la liste de s�lection avec les donn�es de formulaire n'a pas pu s'effectuer - FindFirst en �chec"
                    Exit Function
                Else
                    Fenetre.Controls(NOM_LISTE_CHOIX).Value = Rs.Fields(ColonneLiee - 1).Value
                End If

                'V�rification que ce qui est affich� dans la liste de s�lection correspond bien � ce qui figure dans le formulaire notamment parce que le libell�
                'figurant dans la colonne li�e de la liste de s�lection pourrait contenir des doublons. Le recordset est positionn� sur la bonne ligne depuis l'ex�cution du FindFirst
                'mais il faut v�rifier que les autres colonnes de la liste de s�lection (notamment celles non visibles) correspondent bien � l'enregistrement voulu.
                'Le code ci-dessous posant probl�me (certains objets ne sont pas ou plus accessible de mani�re incompr�hensible, on le d�sactive, ce qui est affich� dans la liste �tant
                'en phase avec le formulaire.
                'For I = 0 To Fenetre.Controls(NOM_LISTE_CHOIX).ColumnCount
                '    If Rs.Fields(I).Value <> Fenetre.Controls(NOM_LISTE_CHOIX).Column(I).Value Then
                '        MsgBox "La liste d�roulante de s�lection contient des doublons - la s�lection de la liste n'est pas celle que le formulaire affiche"
                '        'Un positionnement compl�tement programm� pourrait �tre impl�ment� mais nous consid�rons qu'� ce stade ce n'est pas n�cessaire.
                '        GlobalSync = True
                '        Exit Function
                '    End If
                'Next

            End If
        End If

    Case Else
        MsgBox "Fonction GlobalSync : valeur du param�tre type_synchro non impl�ment�e : " & TypeSynchro
End Select

GlobalSync = True
Exit Function
Erreur_GlobalSync:
    Select Case Err
        'Un objet a �t� supprim� (par exemple l'objet fen�tre a �t� d�charg� pr�alablement). Dans ce cas la synchronisation n'a pas de raison d'�tre et on force le retour � vrai
        Case 2467
            GlobalSync = True
    
        Case Else
            Call GestionErreur(Err, "GlobalSync", "")
    End Select
End Function

Function NomIdSerialEstUnTableau(Fenetre As Form) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 13/10/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*                  - Fenetre : Objet Form pour lequel on veut tester la valeur et le type de
'*                    la variable NomIdSerial.
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE : Tester si la variable NomIdSerial de l'objet Form pass� en      *
'*                            param�tre est bien un tableau contenant au moins un �l�ment. Si *
'*                            tel est le cas elle returne vrai et faux dans le cas contraire  *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_NomIdSerialEstUnTableau

'Si la variable NomIdSerial n''est pas un tableau on sort de la fonction
If Not IsArray(Fenetre.NomIdSerial) Then
    NomIdSerialEstUnTableau = False
    Exit Function
End If

'Si le tableau NomIdSerial ne contient aucun �l�ment on sort de la fonction
If UBound(Fenetre.NomIdSerial) < 0 Then
    NomIdSerialEstUnTableau = False
    Exit Function
End If

NomIdSerialEstUnTableau = True

Exit Function
Erreur_NomIdSerialEstUnTableau:
    Call GestionErreur(Err, "NomIdSerialEstUnTableau", Fenetre.NomIdSerial)
End Function

Public Function GetClauseWhere(Fenetre As Form, BaseSurRecordsetFormulaire As Boolean, Optional Ids As Variant) As String
'************************************************************************************************************************************************************
'*                                                                                                                                                          *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 13/03/2017
'*                                                                                                                                                          *
'************************************************************************************************************************************************************
'*                                                                                                                                                          *
'*      LES PARAMETRES :                                                                                                                                    *
'*          - Fenetre      : objet form pour lequel on souhaite construire la clause where qui permettra d'interroger un objet recordset en vue de         *
'*                            synchroniser des �l�ments de l'�cran.
'*          - BaseSurRecordsetFormulaire : Boolean indiquant si la clause where se construit par rapport au valeur du formulaire (champs de saisies),
'*              ou si l'on se base sur l'�l�ment s�lectionn� dans la liste de choix. Si l'op�rateur appuie sur F5 alors on sera dans un cas ou
'*              'Bas�SurRecordsetFormulaire' devra prendre la valeur false car les informations de l'�cran devront �tre resynchronis� au vu de l'enregistrement
'*              s�lectionn� dans la liste de s�lection. En revanche si une synchronisation est d�clench�e apr�s une mise � jour d'un enregistrement la valeur
'*              de 'Bas�SurRecordsetFormulaire' doit �tre positionn� � true et l'ensemble de l'�cran sera synchronis� par rapport � l'enregistrement en cours de
'*              modification.
'*          - Ids : Optionnel, tableau contenant les valeurs � partir desquelles la clause where va se construire.
'*                                                                                                                                                          *
'*      VALEUR DE RETOUR :                                                                                                                                  *
'*          - Une chaine de caract�res correspondant � la clause where
'*                                                                                                                                                          *
'*      REGLES A RESPECTER :                                                                                                                                *
'*                                                                                                                                                          *
'*      BUT DE LA FONCTION : Cette fonction permet de construire la clause where d'un recordset soit en se basant sur les colonnes de la liste de choix
'*                           soit en se basant sur la valeur des champs du formulaire ou en se basant sur une variable optionnelle qui contiendrait la
'*                           liste des valeurs. Elle permet de rendre g�n�rique certains traitements en construisant la clause where � utiliser de mani�re
'*                           dynamique et g�n�rique (sans aucun nom de champ ni valeur en dur).
'*                                                                                                                                                          *
'*      HISTORIQUE DES MODIFICATIONS :                                                                                                                      *
'*          MODIF1 :    Auteur  : S GUIWARCH                                                                                                                *
'*                      Date    : 13/03/2017                                                                                                                *
'*                      Objet   : Am�lioration du message d'erreur : utilisation incorrecte de la valeur null. Rajout des tests pour v�rifier
'*                                que la colonne de la liste d�roulante ne soit pas �gale � null, ou que le tableua contenant les identifiants possede
'*                                bien assez de valeurs.
'************************************************************************************************************************************************************
On Error GoTo Erreur_GetClauseWhere
Dim ClauseWhere As String
Dim I As Integer
Dim TableauId As Variant
Dim TypeDonnees As Integer

TableauId = Fenetre.NomIdSerial

For I = 0 To UBound(TableauId)
    TypeDonnees = Fenetre.Recordset.Fields(Fenetre.Controls(TableauId(I)).ControlSource).Type
    'Les tables attach�es vers postgresql pour un champ de type bigserial voit une donn�e de type decimal qui n'est pas trait�e correctement
    'par buildcriteria. On remplace donc ce type par de l'entier long en attendant de trouver une solution plus �l�gante
    If TypeDonnees = dbDecimal Then
        TypeDonnees = dbLong
    End If
    Select Case I
        Case 0
            'Si la variable contenant les identifiants est renseign�e
            If Not IsMissing(Ids) Then
                'Test plut�t li� au debug
                If IsNull(Ids(I)) Then
                    MsgBox "La colonne N�" & I & " du tableau pass� en param�tre contenant les valeurs des colonnes constituant la cl� primaire est nulle" & _
                        ", il est donc impossible de construire la clause where de la requ�te."
                    Exit Function
                End If
                ClauseWhere = ClauseWhere & BuildCriteria(TableauId(I), TypeDonnees, Ids(I))
            'dans le cas contraire on test si l'on se base sur les donn�es du formulaire ou bine sur la liste de choix
            Else
                'Les donn�es du formulaire sont utilis�e pour la requ�te
                If BaseSurRecordsetFormulaire Then
                    ClauseWhere = ClauseWhere & BuildCriteria(TableauId(I), TypeDonnees, Fenetre.Controls(TableauId(I)).Value)
                'Les donn�es de la liste de s�lection sont utilis�es
                Else
                    'Test plut�t li� au debug
                    If IsNull(Fenetre.Controls(NOM_LISTE_CHOIX).Column(I)) Then
                        MsgBox "La colonne N�" & I & " de la liste de s�lection est vide, il est donc impossible de construire la clause where de la requ�te."
                        Exit Function
                    End If
                    ClauseWhere = ClauseWhere & BuildCriteria(TableauId(I), TypeDonnees, Fenetre.Controls(NOM_LISTE_CHOIX).Column(I))
                End If
            End If

        Case Else
            'Si la variable contenant les identifiants est renseign�e
            If Not IsMissing(Ids) Then
                ClauseWhere = ClauseWhere & " and " & BuildCriteria(TableauId(I), TypeDonnees, Ids(I))
            'dans le cas contraire on test si l'on se base sur les donn�es du formulaire ou bine sur la liste de choix
            Else
                'Les donn�es du formulaire sont utilis�e pour la requ�te
                If BaseSurRecordsetFormulaire Then
                    ClauseWhere = ClauseWhere & " and " & BuildCriteria(TableauId(I), TypeDonnees, Fenetre.Controls(TableauId(I)).Value)
                Else
                    ClauseWhere = ClauseWhere & " and " & BuildCriteria(TableauId(I), TypeDonnees, Fenetre.Controls(NOM_LISTE_CHOIX).Column(I))
                End If
            End If
    End Select
Next

GetClauseWhere = ClauseWhere

Exit Function
Erreur_GetClauseWhere:
    Call GestionErreur(Err, "GetClauseWhere - Fen�tre : " & Fenetre.NomFenetre, "")
End Function

'param�tre ValChamp est de type variant pour utiliser la fonction ismissing
Public Sub PassageEnAjout(NomFormulaire As String, Optional NomChampARenseigner As String, Optional ValChamp As Variant)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD et M. GUIWARCH              DERNIERE MISE A JOUR : 15/12/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                      - NomFormulaire : nom du formaulaire � activer et � passer en mode ajout
'*                      - NomChampARenseigner : nom du champ de formulaire qui sera
'*                         pr�renseign� en s'appuyant sur la valeur du dernier param�tre. Si
'*                         ce param�tre est omis le formulaire sera seulement pass� en ajout
'*                         (�quivalent du bouton 'Ajouter de ce formulaire).
'*                      - ValChamp : si le param�tre pr�cedent est renseign� celui-ci
'*                         comportera la valeur permettant de renseigner le champ en question
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Fournir le 1� param�tre ou bien les 3 ensembles.
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*          - Activer (avec ouverture �ventuelle s'il n'est pas d�j� ouvert) le formulaire
'*            pass� en param�tre puis simuler un click de son bouton ajouter et �ventuellement
'*            pr�renseign� un champ particulier. L'usage courant est le suivant :
'*              - l'op�rateur doit s�lectionner une valeur dans une liste d�roulante mais
'*                l'�l�ment qu'il souhaite n'y figure pas.
'*              - il click sur le bouton + � cot� de la liste d�roulante
'*              - il doit se retrouver sur le formulaire permettant d'ajouter une nouvelle valeur.
'*
'*              - Une variante peut �tre de vouloir ajouter une ligne dans un sous formulaire
'*                li� (type onglet) et dans ce cas de vouloir directement lier le nouvel
'*                enregistrement � celui du formulaire parent. Par exemple ajouter une comp�tence
'*                � une personne depuis la fiche d'une personne. Dans ce cas on souhaite ne
'*                pas avoir � ressaisir la personne sur le formulaire d'association personne/comp�tence
'*                et on utilisera la syntaxe d'appel avec les 3 param�tres.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_PassageEnAjout
Dim I As Integer

If DroitsUtilisateur = DROIT_LECTURE Then
    MsgBox "vous ne disposez que du droit lecture. Action annul�e"
    Exit Sub
End If

For I = 0 To Forms.Count - 1
    If Forms(I).Name = NomFormulaire Then
        Forms(I).SetFocus
        If Forms(I).Recordset.EditMode <> dbEditNone Then
            MsgBox "Un ajout ou une modification est en cours. Veuillez terminer cette op�ration avant de proc�der � un nouvel ajout " & Forms(I).TypeObjet
            'On sort de la proc�dure sans affecter la valeur pass�e en param�tre afin de ne pas perdre d'�ventuelles modification faites sans avoir �t�s valid�es.
            Exit Sub
        Else
            'Passage en mode ajout
            Forms(I).BTN_Ajouter_Click
            'On passe directement � l'affectation de la valeur du champ concern�. Cela permet d'�viter la 2�me boucle sur tous les formulaires ouverts.
            GoTo AffecterValeur
        End If
        Exit For
    End If
Next

DoCmd.OpenForm NomFormulaire, acNormal, , , acFormEdit, acWindowNormal

For I = 0 To Forms.Count - 1
    If Forms(I).Name = NomFormulaire Then
        If Forms(I).Recordset.EditMode = dbEditNone Then
            'Passage en mode ajout
            Forms(I).BTN_Ajouter_Click
        End If
        Forms(I).SetFocus
        Exit For
    End If
Next

AffecterValeur:
    ' on peut passer un param�tre � la fonction PassageEnAjout qui permet de remplir automatiquement un champ du formulaire � ouvrir
    If Not IsMissing(NomChampARenseigner) And Not IsMissing(ValChamp) Then
        Forms(I).Controls(NomChampARenseigner) = ValChamp
    End If
    Exit Sub
Erreur_PassageEnAjout:
    Call GestionErreur(Err, "PassageEnAjout - Fen�tre " & NomFormulaire, "")
End Sub

Public Sub PassageEnConsultation(NomFormulaire As String, Id As Variant)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD et M. GUIWARCH              DERNIERE MISE A JOUR : 15/12/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                      - NomFormulaire : nom du formaulaire � activer et � passer en mode    *
'*                        consultation.                                                       *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*                      - Activer un formulaire et passer en mode consultation sur un
'*                        enregistrement particulier dont la valeur de l'idientifiant est
'*                        pass�e en param�tre � la proc�dure.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_PassageEnConsultation
Dim I As Integer
Dim ColonneLiee As Integer
Dim Rs As Recordset

DoCmd.OpenForm NomFormulaire, acNormal, , , acFormEdit, acWindowNormal

For I = 0 To Forms.Count - 1
    If Forms(I).Name = NomFormulaire Then
        If Forms(I).Recordset.EditMode <> dbEditNone Then
            MsgBox "Un ajout ou une modification est en cours. Veuillez terminer cette op�ration avant de proc�der � un nouvel ajout " & Forms(I).TypeObjet
            Forms(I).SetFocus
        Else
            'Positionnement dans la liste de s�lection
            ColonneLiee = Forms(I).Controls(NOM_LISTE_CHOIX).BoundColumn
            Set Rs = Forms(I).Controls(NOM_LISTE_CHOIX).Recordset
            Rs.FindFirst GetClauseWhere(Forms(I), False, Id)
            'Aucun enregistrement trouv�
            If Rs.NoMatch Then
                MsgBox "Impossible de se positionner sur l'enregistrement � modifier - FindFirst en �chec"
                Exit Sub
            Else
                Forms(I).Controls(NOM_LISTE_CHOIX).Value = Rs.Fields(ColonneLiee - 1).Value
                CallByName Forms(I), Forms(I).Controls(NOM_LISTE_CHOIX).Name & "_Click", VbMethod
            End If
        End If
        Exit For
    End If
Next

Exit Sub
Erreur_PassageEnConsultation:
    Select Case Err
        'Se produit notamment lorsque la fen�tre n'est pas encore active (appelle depuis le sous-formulaire des relev�s floristiques dans le formulaire des �tudes)
        Case 91
            'BTN_Rafraichir_Click
            Resume

        Case 2465
            MsgBox "Une erreur est survenue lors du passage en consultation sur la fen�tre " & Forms(I).Name & ". Il se peut que la m�thode Click de la liste de s�lection de ce formulaire ne soit pas de port�e publique."

        Case Else
            Call GestionErreur(Err, "PassageEnConsultation - Fen�tre " & NomFormulaire, "")
    End Select
End Sub

Public Sub PassageEnModif(NomFormulaire As String, Id As Variant)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD et M. GUIWARCH              DERNIERE MISE A JOUR : 15/12/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                      - NomFormulaire : nom du formaulaire � activer et � passer en mode    *
'*                                        modification.
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Voir partie D�claration de ce module pour conna�tre les imp�ratifs � mettre     *
'*            en place sur la feuille pass�e en param�tre pour que la gestion fonctionne.     *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*                      - Activer un formulaire et passer en mode modification sur un
'*                        enregistrement particulier dont la valeur de l'idientifiant est
'*                        pass�e en param�tre � la proc�dure.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_PassageEnModif
Dim I As Integer
Dim ColonneLiee As Integer
Dim Rs As Recordset

If DroitsUtilisateur = DROIT_LECTURE Then
    MsgBox "vous ne disposez que du droit lecture. Action annul�e"
    Exit Sub
End If

DoCmd.OpenForm NomFormulaire, acNormal, , , acFormEdit, acWindowNormal

For I = 0 To Forms.Count - 1
    If Forms(I).Name = NomFormulaire Then
        If Forms(I).Recordset.EditMode <> dbEditNone Then
            MsgBox "Un ajout ou une modification est en cours. Veuillez terminer cette op�ration avant de proc�der � un nouvel ajout " & Forms(I).TypeObjet
            Forms(I).SetFocus
        Else
            'Positionnement dans la liste de s�lection
            ColonneLiee = Forms(I).Controls(NOM_LISTE_CHOIX).BoundColumn
            Set Rs = Forms(I).Controls(NOM_LISTE_CHOIX).Recordset
            Rs.FindFirst GetClauseWhere(Forms(I), False, Id)
            'Aucun enregistrement trouv�
            If Rs.NoMatch Then
                MsgBox "Impossible de se positionner sur l'enregistrement � modifier - FindFirst en �chec"
                Exit Sub
            Else
                Forms(I).Controls(NOM_LISTE_CHOIX).Value = Rs.Fields(ColonneLiee - 1).Value
                CallByName Forms(I), Forms(I).Controls(NOM_LISTE_CHOIX).Name & "_Click", VbMethod
                Forms(I).BTN_Modifier_Click
            End If
        End If
        Exit For
    End If
Next

Exit Sub
Erreur_PassageEnModif:
    Select Case Err
        'Se produit notamment lorsque la fen�tre n'est pas encore active (appelle depuis le sous-formulaire des relev�s floristiques dans le formulaire des �tudes)
        Case 91
            'BTN_Rafraichir_Click
            Resume

        Case 2465
            MsgBox "Une erreur est survenue lors du passage en consultation sur la fen�tre " & Forms(I).Name & ". Il se peut que la m�thode Click de la liste de s�lection de ce formulaire ou du bouton modifier ne soit pas de port�e publique."

        Case Else
            Call GestionErreur(Err, "PassageEnModif - Fen�tre " & NomFormulaire, "")
    End Select
End Sub

Public Function ValideDonnee(ValeurDonnee As String, TypeDonnee As String, PatternControle As String, SeparateurPattern As String, SeparateurPlage As String, SeparateurDecimal As String, ProposeAide As Boolean) As ValidationData
'*****************************************************************************************************************
'*                                                                                                               *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 10/02/2017                                        *
'*                                                                                                               *
'*****************************************************************************************************************
'*                                                                                                               *
'*      LES PARAMETRES :                                                                                         *
'*              - ValeurDonnee      : valeur alphanum�rique de la donn�e � v�rifier                              *
'*              - TypeDonnee        : chaine d�finissant le type pr�sum� de la donn�e                            *
'*              - PatternControle   : chaine d�finissant les valeurs possible que peut prendre la donn�e         *
'*              - SeparateurPattern : caract�re ou chaine qui s�pare les �l�ments dans le PatternControle        *
'*              - SeparateurPlage   : caract�re ou chaine qui separe les extr�mes d'une plage num�rique          *
'*                                    � noter que les plages doivent �tre non chavauchante et ordonn�es dans le  *
'*                                    PatternControle (sinon rejet)                                              *
'*              - SeparateurDecimal : caract�re qui repr�sente le s�parateur d�cimal � utiliser.                 *
'*              - ProposeAide       : bool�en pr�cisant si la partie info_bulle doit �tre renseign�e dans la     *
'*                                    valeur de retour de la fonction.                                           *
'*                                                                                                               *
'*      VALEUR DE RETOUR   :    Une structure de type Validation_Data dont les champs sont renseign�s ainsi :    *
'*                                - donnee_valide : vrai si la valeur de la donn�e est en ad�quation avec le type*
'*                                      pr�sum� et le pattern de contr�le et faux dans le cas contraire.         *
'*                                - info_bulle : renseign� si le param�tre ProposeAide est � vrai et vide dans le*
'*                                      contraire. La valeur est construite en fonction du type de donn�e et du  *
'*                                      pattern de controle.                                                     *
'*                                                                                                               *
'*                                                                                                               *
'*      REGLES A RESPECTER :                                                                                     *
'*             - Le param�tre TypeDonnees doit contenir une valeur dont le traitement est impl�ment� par cette   *
'*               fonction sinon donnee_valide sera mis � false et info_bulle retourn� avec une information sur   *
'*               la nature de l'erreur. Si besoin il faudra impl�menter de nouveaux types.                       *
'*             - Le param�tre PatternControle doit respecter les formes impl�ment�es pour chaque type de donn�es *
'*               sinon donnee_valide sera mis � false et info_bulle retourn� vide. Si besoin il faudra           *
'*               impl�menter de nouveaux couples 'Type de donn�e / Pattern de controle'.                         *
'*               D'une mani�re g�n�rale une valeur de type num�rique peut devoir respecter certaines plages avec *
'*              par exemple un pattern tel que :
'*                  -5..83.6 pour une valeur comprise entre -5 et 83.6.
'*                  >0 pour une valeur srictement sup�rieure � 0
'*                  >=0 pour une valeur sup�rieure ou �gale � 0
'*
'*                                                                                                               *
'*      BUT DE LA FONCTION :  Retourner une information permettant de savoir si la valeur propos�e est conforme  *
'*                            au type de donn�e propos�e et respecte le pattern propos�. Optionnellement         *
'*                            retourner une information sur les valeurs possibles permettant de respecter le     *
'*                            couple 'Type de donn�e / Pattern de controle'.                                     *
'*                                                                                                               *
'*      HISTORIQUE DES MODIFICATIONS :                                                                           *
'*          MODIF1 :    Auteur  :   Alain BENARD                                                                 *
'*                      Date    :   14/02/2017                                                                   *
'*                      Objet   :   Cr�ation                                                                     *
'*****************************************************************************************************************
On Error GoTo Erreur_ValideDonnee
Dim TableauPatternElementaires() As String
Dim NombreValeurPlage As Integer
'Variable de boucles
Dim I As Integer
Dim ValeurNumerique As Double
Dim StatutNumerique As ValidationPatternNumerique
Dim Borne As Variant

'Intialisation des valeurs de retour
ValideDonnee.DonneeValide = False
ValideDonnee.InfoBulle = ""

'Premier passage pour r�aliser les t�ches simples et communes
Select Case TypeDonnee
    'On doit remplir le tableau des pattern �l�mentaires
    Case "Categorial", "Continuous, numeric", "Whole number (integer)"
        TableauPatternElementaires = Split(PatternControle, SeparateurPattern)
        NombreValeurPlage = UBound(TableauPatternElementaires)
        'Si PatternControle est vide notamment - s'il n'y a pas de pattern de contr�le alors toute valeur est valide si son type est correct
        If NombreValeurPlage = -1 Then
            Select Case TypeDonnee
                Case "Continuous, numeric"
                    'Remplacement du separateur d�cimal dans la cha�ne source si celui-ci n'est pas celui de notre syst�me (d�fini dans la constante SEPARATEUR_DECIMAL_SYSTEME)
                    If Not (SeparateurDecimal = SEPARATEUR_DECIMAL_SYSTEME) Then
                        ValeurDonnee = Replace(ValeurDonnee, SeparateurDecimal, SEPARATEUR_DECIMAL_SYSTEME, 1, 1, vbTextCompare)
                    End If
                    If IsNumeric(ValeurDonnee) Then
                        ValideDonnee.DonneeValide = True
                    Else
                        ValideDonnee.DonneeValide = False
                    End If
                    If ProposeAide Then
                        ValideDonnee.InfoBulle = "Valeurs autoris�es : tous nombres d�cimaux ou entiers"
                    End If
                    Exit Function

                Case "Whole number (integer)"
                    'Remplacement du separateur d�cimal dans la cha�ne source si celui-ci n'est pas celui de notre syst�me (d�fini dans la constante SEPARATEUR_DECIMAL_SYSTEME)
                    'Si ce remplacement n'est pas effectu� une erreur 'Type incompatible' surviendra ci-apr�s lors de l'instruction Int(ValeurDonnee).
                    If Not (SeparateurDecimal = SEPARATEUR_DECIMAL_SYSTEME) Then
                        ValeurDonnee = Replace(ValeurDonnee, SeparateurDecimal, SEPARATEUR_DECIMAL_SYSTEME, 1, 1, vbTextCompare)
                    End If

                    'La valeur est num�rique
                    If IsNumeric(ValeurDonnee) Then
                        If Int(ValeurDonnee) = CDec(ValeurDonnee) Then 'sans partie d�cimale
                            ValideDonnee.DonneeValide = True
                        Else
                            ValideDonnee.DonneeValide = False
                        End If
                    Else
                        ValideDonnee.DonneeValide = False
                    End If
                    If ProposeAide Then
                        ValideDonnee.InfoBulle = "Valeurs autoris�es : tous nombres entiers"
                    End If
                    Exit Function

                'Il reste le type Categorial qui est consid�r� comme valide si aucun pattern de contr�le n'est d�fini.
                Case Else
                    ValideDonnee.DonneeValide = True
            End Select

            Exit Function

        'Le pattern n'est pas vide : on v�rifie pour les types num�riques que les comparateurs <,<=,> et >= ne sont pr�sents qu'une seule fois
        Else
            Select Case TypeDonnee
                Case "Continuous, numeric", "Whole number (integer)"
                    If (UBound(Split(PatternControle, ">")) > 1) Or (UBound(Split(PatternControle, "<")) > 1) Then
                        ValideDonnee.InfoBulle = "Op�rateur '>' ou '>=' ou '<' ou '<=' pr�sent plusieurs fois dans le pattern de contr�le"
                        Exit Function
                    End If
            End Select
        End If

    'La valeur ne peut contenir qu'une des 2 valeurs Yes ou No
    Case "Discret, Yes / No"
        If ValeurDonnee = "Yes" Or ValeurDonnee = "No" Then
            ValideDonnee.DonneeValide = True
        End If
        If ProposeAide Then
            ValideDonnee.InfoBulle = "Valeurs autoris�es : Yes , No"
        End If
        Exit Function

    Case Else
        ValideDonnee.InfoBulle = "Le type de donn�e " & TypeDonnee & " n'est pas impl�ment� dans la fonction de v�rification ValideDonnee. Adpatez les donn�es ou bien faites �voluer la fonction."
        Exit Function
End Select

'Analyse quel type de donn�es est � analyser
Select Case TypeDonnee
    'La valeur ne peut prendre qu'une de celles �num�r�es dans la cha�ne PatternControle pas exemple rouge$vert$jaune avec un SeparateurPattern qui vaut $ (une des 3 valeurs rouge, vert ou jaune est alors acceptable)
    Case "Categorial"
        For I = 0 To UBound(TableauPatternElementaires)
            If ValeurDonnee = TableauPatternElementaires(I) Then
                ValideDonnee.DonneeValide = True
                Exit For
            End If
        Next

    'La valeur est obligatoirement num�rique et doit correspondre avec la ou les plages d�finies dans la cha�ne PatternControle
    'par exemple le PatternControle 12..20$25..40 autorise n'importe quel nombre entre 12 et 20 ou bien entre 25 et 40 avec un SeparateurPattern qui vaut $
    'et un SeparateurPlage qui vaut ..
    Case "Continuous, numeric"
        'Remplacement du separateur d�cimal dans la cha�ne source si celui-ci n'est pas celui de notre syst�me (d�fini dans la constante SEPARATEUR_DECIMAL_SYSTEME)
        If Not (SeparateurDecimal = SEPARATEUR_DECIMAL_SYSTEME) Then
            ValeurDonnee = Replace(ValeurDonnee, SeparateurDecimal, SEPARATEUR_DECIMAL_SYSTEME, 1, 1, vbTextCompare)
        End If

        'La valeur est num�rique : le type de donn�e est correct.
        If IsNumeric(ValeurDonnee) Then
                'V�rification avec tous les �l�ments du pattern
                ValeurNumerique = CDbl(ValeurDonnee)
                'Au premier appel il n'y a pas de valeur de borne
                Borne = Null
                For I = 0 To UBound(TableauPatternElementaires)
                    StatutNumerique = ValideNombre(ValeurNumerique, TableauPatternElementaires(I), SeparateurPlage, Borne)
                    If StatutNumerique.NombreValide Then
                        ValideDonnee.DonneeValide = True
                        Exit For
                    Else
                        If StatutNumerique.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE Then
                            ValideDonnee.InfoBulle = ERREUR_PATTERN_NUMERIQUE + Chr$(13) + PatternControle + Chr$(13)
                            'Si au moins un pattern est incorrect on arr�te le traitement.
                            Exit For
                        Else
                            Borne = StatutNumerique.BorneSuperieure
                        End If
                     End If
                 Next
            Else
                ValideDonnee.DonneeValide = False
        End If

    'La valeur ne peut contenir que des valeurs enti�res et doit correspondre avec la ou les plages d�finies dans la cha�ne PatternControle
    Case "Whole number (integer)"
        'Remplacement du separateur d�cimal dans la cha�ne source si celui-ci n'est pas celui de notre syst�me (d�fini dans la constante SEPARATEUR_DECIMAL_SYSTEME)
        'Si ce remplacement n'est pas effectu� une erreur 'Type incompatible' surviendra ci-apr�s lors de l'instruction Int(ValeurDonnee).
        If Not (SeparateurDecimal = SEPARATEUR_DECIMAL_SYSTEME) Then
            ValeurDonnee = Replace(ValeurDonnee, SeparateurDecimal, SEPARATEUR_DECIMAL_SYSTEME, 1, 1, vbTextCompare)
        End If

        'La valeur est num�rique
        If IsNumeric(ValeurDonnee) Then
            'sans partie d�cimale : le type de donn�e est correct.
            If Int(ValeurDonnee) = CDec(ValeurDonnee) Then
                'V�rification avec tous les �l�ments du pattern
                ValeurNumerique = CDbl(ValeurDonnee)
                'Au premier appel il n'y a pas de valeur de borne
                Borne = Null
                For I = 0 To UBound(TableauPatternElementaires)
                    StatutNumerique = ValideNombre(ValeurNumerique, TableauPatternElementaires(I), SeparateurPlage, Borne)
                    If StatutNumerique.NombreValide Then
                        ValideDonnee.DonneeValide = True
                        Exit For
                    Else
                        If StatutNumerique.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE Then
                            ValideDonnee.InfoBulle = ERREUR_PATTERN_NUMERIQUE + Chr$(13) + PatternControle + Chr$(13)
                            'Si au moins un pattern est incorrect on arr�te le traitement.
                            Exit For
                        Else
                            Borne = StatutNumerique.BorneSuperieure
                        End If
                    End If
                 Next
            Else
                ValideDonnee.DonneeValide = False
            End If
        Else
            ValideDonnee.DonneeValide = False
        End If
End Select

If ProposeAide Then
    ValideDonnee.InfoBulle = ValideDonnee.InfoBulle + "Valeurs autoris�es :"
    For I = 0 To UBound(TableauPatternElementaires)
        ValideDonnee.InfoBulle = ValideDonnee.InfoBulle + Chr$(13) + TableauPatternElementaires(I)
    Next
End If
    
Exit Function
Erreur_ValideDonnee:
    Call GestionErreur(Err, "ValideDonnee", "")
End Function

Public Function ValideNombre(ValeurNumerique As Double, PatternSimple As String, SeparateurPlage As String, BornePrecedente As Variant) As ValidationPatternNumerique
'*****************************************************************************************************************
'*                                                                                                               *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 10/02/2017                                        *
'*                                                                                                               *
'*****************************************************************************************************************
'*                                                                                                               *
'*      LES PARAMETRES :                                                                                         *
'*              - ValeurNumerique   : valeur num�rique de la donn�e � v�rifier                                   *
'*              - PatternSimple     : chaine d�finissant le pattern �l�mentaire � tester                         *
'*                                                                                                               *
'*      VALEUR DE RETOUR   :    Une structure de type ValidationPatternNumerique dont les champs sont renseign�s *
'*                              ainsi :                                                                          *
'*                                - nombre_valide : vrai si la valeur du nombre est en ad�quation avec le pattern*
'*                                      de contr�le et faux dans le cas contraire.                               *
'*                                - borne_superieure : nombre repr�sentant la borne sup�rieure du pattern pass�  *
'*                                      en param�tre.                                                            *
'*                                - SeparateurPlage   : caract�re ou chaine qui separe les extr�mes d'une plage  *
'*                                - BornePrecedente : param�tre optionnel qui permet de v�rifier que le pattern  *
'*                                    est compatible avec une borne 'inf�rieure' g�n�ralement issue du r�sultat  *
'*                                    d'un pr�c�dent appel; il s'agit ici d'imposer une liste de patterne tri�e  *
'*                                    pour �viter les conflits de pattern.                                       *
'*                                    Par exemple un premier pattern de comparaison est 8..12 qui renvoie comme  *
'*                                    borne_superieure la valeur 12. Si l'on rappelle la fonction avec un pattern*
'*                                    10..14 et comme param�tre 'BornePrecedente' 12 alors la fonction sera en   *
'*                                    mesure de d�tecter une anomalie. Si la borne ne contient pas une valeur    *
'*                                    num�rique elle sera tout simplement ignor�e.                               *
'*                                                                                                               *
'*      REGLES A RESPECTER :                                                                                     *
'*             - Le param�tre PatternSimple doit respecter une des formes suivantes (sinon nombre_valide sera mis*
'*               � false):                                                                                       *
'*                  Plage de valeur sous la forme 'Nombre1' 'SeparateurPlage' 'Nombre2'                          *
'*                  Comparaison sous la forme     'op�rateur de comparaison' 'Nombre' ou l'op�rateur prend une   *
'*                      des valeurs suivantes :                                                                  *
'*                          >                                                                                    *
'*                          >=                                                                                   *
'*                          <                                                                                    *
'*                          <=                                                                                   *
'*                  Valeur num�rique � laquelle le nombre doit �tre compar�                                      *
'*              exemples :                                                                                       *
'*                  -5..83.6 pour une valeur comprise entre -5 et 83.6.                                          *
'*                  >0 pour une valeur srictement sup�rieure � 0                                                 *
'*                  >=0 pour une valeur sup�rieure ou �gale � 0                                                  *
'*                                                                                                               *
'*                                                                                                               *
'*      BUT DE LA FONCTION :  Retourner une information permettant de savoir si la valeur propos�e est conforme  *
'*                            au pattern simple propos�.                                                         *
'*                            retourner une information sur la borne sup�rieure du pattern qui pourra �tre       *
'*                            utilis�e lors d'un prochain appel � la fonction pour tester le pattern suivant     *
'*                            permettant ainsi d'�viter les conflits de pattern comme >8 puis <12                *
'*                            Si le pattern pass� en param�tre est jug� incorrect alors cette valeur sera        *
'*                            positionn�e avec le contenu de la constante ERREURPATTERNNUMERIQUE                 *
'*                            D'une mani�re g�n�rale cette fonction est appel�e par la fonction ValideDonnee.    *
'*                                                                                                               *
'*      HISTORIQUE DES MODIFICATIONS :                                                                           *
'*          MODIF1 :    Auteur  :   Alain BENARD                                                                 *
'*                      Date    :   14/02/2017                                                                   *
'*                      Objet   :   Cr�ation                                                                     *
'*****************************************************************************************************************
On Error GoTo Erreur_ValideNombre
Dim ListeTmp() As String
Dim Nombre1 As Double
Dim Nombre2 As Double

'Intialisation des valeurs de retour
ValideNombre.NombreValide = False
ValideNombre.BorneSuperieure = ""

'Recherche si le pattern contient une plage sous la forme 'Nombre1' 'separateur de plage' 'nombre2'. Si tel est le cas le r�sultat de la commande split ci-dessous renverra un tableau
'avec 2 lignes (ubound = 1)
ListeTmp = Split(PatternSimple, SeparateurPlage)
If UBound(ListeTmp) = 1 Then
    'V�rifie que les 2 �l�ments sont des nombres
    'Le pattern est une plage correcte - il faut maintenant v�rifier si la valeur est incluse entre ces bornes et tenir compte du param�tre BornePrecedente
    If IsNumeric(ListeTmp(0)) And IsNumeric(ListeTmp(1)) Then
        Nombre1 = CDbl(ListeTmp(0))
        Nombre2 = CDbl(ListeTmp(1))
        'La borne est pr�cis�e
        If Not (IsNull(BornePrecedente)) Then
            'V�rifie que la borne est bien num�rique
            If IsNumeric(BornePrecedente) Then
                'V�rifie que le param�tre BornePrecedente n'est pas strictement sup�rieur � la borne basse de la plage
                If BornePrecedente > Nombre1 Then
                    ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
                    Exit Function
                End If
                'La borne est incorrecte : elle est ignor�e
                'Else
            End If
        End If
        'V�rifie si la valeur est comprise entre les bornes de la plage (de mani�re exclusive)
        If (ValeurNumerique > Nombre1) And (ValeurNumerique < Nombre2) Then
            ValideNombre.NombreValide = True
        'Alimente la bornesuperieure pour les prochains appels
        Else
            ValideNombre.BorneSuperieure = Nombre2
        End If
        Exit Function
    'Plage incorrecte
    Else
        ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
        Exit Function
    End If
'Fin de traitement du pattern type 'Plage num�rique'
End If

'Recherche si le pattern est de type comparaison � savoir qu'il d�bute par une des cha�nes >, >=, <, <=
'** Op�rateur >=
ListeTmp = Split(PatternSimple, ">=")
'Un unique �l�ment est split� (en r�alit� un �l�ment vide puis celui apr�s l'op�rateur); pattern de la forme >= nb; il faut v�rifier s'il est num�rique
'et effectuer la v�rification avec la valeur pass�e dans le param�tre ValeurNumerique
If UBound(ListeTmp) = 1 Then
    If IsNumeric(ListeTmp(1)) Then
        'V�rifie la coh�rence avec l'�ventuelle borne pr�c�dente
        If Not (IsNull(BornePrecedente)) Then
            If CDbl(BornePrecedente) > CDbl(ListeTmp(1)) Then
                ValideNombre.NombreValide = False
                ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
                Exit Function
            End If
        End If
        'V�rifie si la valeur r�pond au pattern
        If ValeurNumerique >= CDbl(ListeTmp(1)) Then
            ValideNombre.NombreValide = True
        Else
            ValideNombre.BorneSuperieure = CDbl(ListeTmp(1))
        End If
    'Le pattern n'est pas correct
    Else
        ValideNombre.NombreValide = False
        ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
    End If
    'Le pattern a �t� trouv� et trait� donc inutile de continuer
    Exit Function
End If

'** Op�rateur >
ListeTmp = Split(PatternSimple, ">")
'Un unique �l�ment est split� (en r�alit� un �l�ment vide puis celui apr�s l'op�rateur); pattern de la forme > nb; il faut v�rifier
's'il est num�rique et effectuer la v�rification avec la valeur pass�e dans le param�tre ValeurNumerique
If UBound(ListeTmp) = 1 Then
    If IsNumeric(ListeTmp(1)) Then
        'V�rifie la coh�rence avec l'�ventuelle borne pr�c�dente
        If Not (IsNull(BornePrecedente)) Then
            If CDbl(BornePrecedente) > CDbl(ListeTmp(1)) Then
                    ValideNombre.NombreValide = False
                    ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
                    Exit Function
            End If
        End If
        'V�rifie si la valeur r�pond au pattern
        If ValeurNumerique > CDbl(ListeTmp(1)) Then
            ValideNombre.NombreValide = True
        Else
            ValideNombre.BorneSuperieure = CDbl(ListeTmp(1))
        End If
    'Le pattern n'est pas correct
    Else
        ValideNombre.NombreValide = False
        ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
    End If
    'Le pattern a �t� trouv� et trait� donc inutile de continuer
    Exit Function
End If

'** Op�rateur <=
ListeTmp = Split(PatternSimple, "<=")
'Un unique �l�ment est split� (en r�alit� un �l�ment vide puis celui apr�s l'op�rateur); pattern de la forme <= nb; il faut v�rifier
's'il est num�rique et effectuer la v�rification avec la valeur pass�e dans le param�tre ValeurNumerique
If UBound(ListeTmp) = 1 Then
    If IsNumeric(ListeTmp(1)) Then
        'V�rifie la coh�rence avec l'�ventuelle borne pr�c�dente
        If Not (IsNull(BornePrecedente)) Then
            If CDbl(BornePrecedente) > CDbl(ListeTmp(1)) Then
                    ValideNombre.NombreValide = False
                    ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
                    Exit Function
            End If
        End If
        'V�rifie si la valeur r�pond au pattern
        If ValeurNumerique <= CDbl(ListeTmp(1)) Then
            ValideNombre.NombreValide = True
        Else
            ValideNombre.BorneSuperieure = CDbl(ListeTmp(1))
        End If
    'Le pattern n'est pas correct
    Else
        ValideNombre.NombreValide = False
        ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
    End If
    'Le pattern a �t� trouv� et trait� donc inutile de continuer
    Exit Function
End If

'** Op�rateur <
ListeTmp = Split(PatternSimple, "<")
'Un unique �l�ment est split� (en r�alit� un �l�ment vide puis celui apr�s l'op�rateur); pattern de la forme < nb; il faut v�rifier
's'il est num�rique et effectuer la v�rification avec la valeur pass�e dans le param�tre ValeurNumerique
If UBound(ListeTmp) = 1 Then
    If IsNumeric(ListeTmp(1)) Then
        'V�rifie la coh�rence avec l'�ventuelle borne pr�c�dente
        If Not (IsNull(BornePrecedente)) Then
            If CDbl(BornePrecedente) > CDbl(ListeTmp(1)) Then
                ValideNombre.NombreValide = False
                ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
                Exit Function
            End If
        End If
        'V�rifie si la valeur r�pond au pattern
        If ValeurNumerique < CDbl(ListeTmp(1)) Then
            ValideNombre.NombreValide = True
        Else
            ValideNombre.BorneSuperieure = CDbl(ListeTmp(1))
        End If
    'Le pattern n'est pas correct
    Else
        ValideNombre.NombreValide = False
        ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
    End If
    'Le pattern a �t� trouv� et trait� donc inutile de continuer
    Exit Function
End If

'Le pattern n'est ni une plage ni une comparaison avec un op�rateur; il doit alors forc�ment �tre une valeur fixe de comparaison; � d�faut il est consid�r� comme erron�
If IsNumeric(PatternSimple) Then
    If ValeurNumerique = CDbl(PatternSimple) Then
        ValideNombre.NombreValide = True
    Else
        ValideNombre.NombreValide = False
        ValideNombre.BorneSuperieure = CDbl(PatternSimple)
    End If
Else
    ValideNombre.NombreValide = False
    ValideNombre.BorneSuperieure = ERREUR_PATTERN_NUMERIQUE
End If

Exit Function
Erreur_ValideNombre:
    Call GestionErreur(Err, "ValideNombre", "")
End Function

Public Function RetourneValeurLieePremierElement(Liste As ComboBox) As Variant
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 23/05/2017
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                          - liste : combobox dont on souhaite extraire la valeur            *
'*
'*      VALEUR DE RETOUR :                                                                    *
'*                          - valeur de la colonne li�e de la combobox pour le prmeir �l�ment *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*                          - Tester si la combobox pass�e en param�tre affiche les ente�tes  *
'*                            colonne et retourner la valeur du premier �l�ment (item 0 ou 1) *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_ValideNombre

If Liste.ColumnHeads Then
    RetourneValeurLieePremierElement = Liste.ItemData(1)
Else
    RetourneValeurLieePremierElement = Liste.ItemData(0)
End If

Exit Function
Erreur_ValideNombre:
    Call GestionErreur(Err, "RetourneValeurLieePremierElement", "")
End Function

Public Sub TuningFormulaireGlobale(Fenetre As Form)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    S. GUIWARCH              DERNIERE MISE A JOUR : 27/07/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                           - Fenetre : Formulaire contenenant les controles a tuner.
'*
'*      VALEUR DE RETOUR :                                                                    *
'*                           - Aucune
'*                                                                                            *
'*      REGLES A RESPECTER : - L'ordre des �l�ments de chaques tableaux est tr�s important car
'*                             l'index dans le tableau NomControlesATuner est r�-utilis� dans
'*                             tous les autres tableaux.
'*                             Exemple : dans le cas ci-dessous le bouton supprimer a l'index
'*                             0 dans le tableau NomControlesATuner, de ce faitsa couleur de
'*                             bordures sera l'�l�ment ayant l'index 0 dans le tableau
'*                             CouleursBordures. Idem pour les autres tableaux.
'*                                                                                            *
'*      BUT DE LA FONCTION : - Personnaliser des controles d'un formulaire en changeant une ou
'*                             plusieurs de ses propri�t�s dans notre cas il s'agit de tuner
'*                             les boutons principaux en modifiant leurs couleurs, la couleur
'*                             de leurs bordure, la couleur du texte, la couleur de fond,
'*                             la couleur du texte au survole de la souris, la couleurs du
'*                             fond lors du survole de la souris, la couleur du texte lors du
'*                             clique sur le controle et la couleur de fond lors du clique sur
'*                             le controle. Le developpeur peut surcharger cette fonction en
'*                             utilisant la fonction "TuningFormulaire" propre � chaque
'*                             formulaires.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_TuningFormulaireGlobale
Const COULEUR_ENTETE As Long = 13882323
Const COULEUR_DETAIL As Long = 16777215
Dim NomControlesATuner, CouleursBordures, CouleursTextes, CouleursFonds, CouleursTextesSurvol, CouleursSurvol, couleursTextesClique, CouleursFondsCliques As Variant
Dim I As Integer

' On d�finit le tableau contenant les noms des controles a tuner
NomControlesATuner = Array("BTN_Supprimer", "BTN_Modifier", "BTN_Ajouter", "BTN_Quitter", "BTN_Sauvegarder", "BTN_Annuler")

' On d�finit la couleur des bordures pour chaque controles a tuner
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursBordures = Array(1716164, 1599207, 12349952, 0, 2203732, 8355711)

' On d�finit la couleur du texte pour chaques controles a tuner
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursTextes = Array(-2147483630, -2147483630, -2147483630, 16777215, -2147483630, -2147483630)

' On d�finit la couleur de fond pour chaques controles a tuner
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursFonds = Array(10069234, 7644145, 14865828, 0, 6413972, 12566463)

' On d�finit la couleur de fond lorsque le pointer de la souris survole le conrole
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursTextesSurvol = Array(16777215, 16777215, 16777215, 0, 16777215, 16777215)

' On d�finit la couleur de fond lorsque le curseur de la souris survol le controle
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursSurvol = Array(1716164, 1599207, 12349952, 16777215, 2203732, 8355711)

' On d�finit la couleur du texte lorsque l'on clique sur le controle
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
couleursTextesClique = Array(16777215, 16777215, 16777215, 0, 1677215, 16777215)

' On d�finit la couleur de fond lorsque l'on clique sur le controle
' (ATTENTION l'ordre des �l�ments du tableau doit �tre le m�me que celui du tableau NomControlesATuner)
CouleursFondsCliques = Array(1716164, 1599207, 12349952, 16777215, 2203732, 8355711)

' On modifit la couleur de fond de l'ent�te du formulaire.
Fenetre.Ent�teFormulaire.BackColor = COULEUR_ENTETE

' On modifit la couleur de fond de la section D�tail.
Fenetre.D�tail.BackColor = COULEUR_DETAIL

' On parcours le tableau contenant le nom des controles puis on affecte chaque propri�tes
For I = LBound(NomControlesATuner) To UBound(NomControlesATuner)
    Fenetre.Controls(NomControlesATuner(I)).BorderColor = CouleursBordures(I)
    Fenetre.Controls(NomControlesATuner(I)).ForeColor = CouleursTextes(I)
    Fenetre.Controls(NomControlesATuner(I)).BackColor = CouleursFonds(I)
    Fenetre.Controls(NomControlesATuner(I)).HoverForeColor = CouleursTextesSurvol(I)
    Fenetre.Controls(NomControlesATuner(I)).HoverColor = CouleursSurvol(I)
    Fenetre.Controls(NomControlesATuner(I)).PressedForeColor = couleursTextesClique(I)
    Fenetre.Controls(NomControlesATuner(I)).PressedColor = CouleursFondsCliques(I)
Next I

Exit Sub
Erreur_TuningFormulaireGlobale:
    Call GestionErreur(Err, "TuningFormulaireGlobale", "")
End Sub
