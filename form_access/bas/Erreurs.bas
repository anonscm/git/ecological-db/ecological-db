Attribute VB_Name = "Erreurs"
Option Explicit

Sub commentaire()
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************


End Sub

Function ErreurDonnees(NumeroErreur As Long) As String
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          NumeroErreur    : Num�ro de l'erreur qui a �t� d�tect�e                           *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - Chaine de caract�res contenant un message explicit pour certains types d'erreur.*
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*                                                                                            *
'*      BUT DE LA FONCTION                                                                    *
'*          - Indiquer un message clair lorsque VB ne peut le faire, notamment lorsque        *
'*            l'erreur intervient lorsque l'int�grit� des donn�es est viol�e (Doublons dans 1 *
'*            index ...). Cette fonction est syst�matiquement appel�e par la fonction         *
'*            GestionErreur qui est la fonction g�n�rale de traitement des erreurs.           *
'*          - Elle renvoie une chaine indiquant quel type d'erreur est survenu pour certains  *
'*            type d'erreur.                                                                  *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
Dim DerniereErreur As Integer

'Derni�re erreur donn�e
DerniereErreur = DBEngine.Errors.Count - 1
'Si l'application n'utilise pas de gestion de donn�es DBEngineErrors.Count - 1 renvoie
'la valeur - 1 et il n'y a pas lieu de chercher un message d'erreur DATA
If DerniereErreur <> -1 Then
    If DBEngine.Errors(DerniereErreur).Number = NumeroErreur Then
        ErreurDonnees = DBEngine.Errors(DerniereErreur).Description
    End If
End If

End Function

Sub GestionErreur(NumeroErreur As Long, NomProcedure As String, TableauParametres As Variant)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR :
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          NumeroErreur    : Num�ro de l'erreur qui a �t� d�tect�e                           *
'*          NomProcedure    : Nom de la Proc�dure ou Fonction qui a d�tect� une erreur. Il    *
'*                            peut �tre interessant de mettre le nom de module �galement      *
'*          TableauParametres   : Tableau contenant les valeurs des diff�rents param�tres de  *
'*                                la fonction ou proc�dure au moment de la d�tection d'erreur *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          Lorsqu'une erreur se produit dans une proc�dure ou fonction 2 cas de figure       *
'*          peuvent se pr�senter :                                                            *
'*              - L'erreur est trait�e en cons�quence (annulation, sortie, remise � blanc...) *
'*              - L'erreur n'est pas trait�e.                                                 *
'*          Dans ce dernier cas on utilise g�n�ralement une instruction Error$(Err) permettant*
'*          d'afficher un message clair. Plut�t que de remettre cette instruction dans toutes *
'*          les routines de gestion d'erreur il peut �tre fait appel � la fonction ci-dessus. *
'*          Cel� garantit une gestion uniforme des erreurs non trait�es et simplifie le code  *
'*          notamment si l'on souhaite afficher le nom du module et les param�tres.           *
'*          De plus l'�volution de la gestion des erreurs sera plus facile si une seule       *
'*          proc�dure contient le code.                                                       *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*          Indiquer le message et le num�ro d'une erreur qui n'est pas trait�e par le code   *
'*          d'une fonction ou proc�dure. De plus le message indique quelle est la fonction ou *
'*          proc�dure qui a fait l'objet d'une erreur et quels �taient les param�tres � ce    *
'*          moment.                                                                           *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_GestionErreur
Dim MessageDonnees As String

MessageDonnees = ErreurDonnees(NumeroErreur)

Select Case MessageDonnees
    Case ""
        MsgBox "Une erreur est survenue dans le module " + NomProcedure + " ; Erreur N� : " + Str$(NumeroErreur) + Chr$(13) + Error$(NumeroErreur) + Chr$(13) + TableauParametres

    Case Else
        MsgBox MessageDonnees + Chr$(13) + "Une erreur est survenue dans le module " + NomProcedure + " ; Erreur N� : " + Str$(NumeroErreur) + Chr$(13) + Error$(NumeroErreur)
End Select

Screen.MousePointer = 0

Exit Sub
Erreur_GestionErreur:
    MsgBox "Erreur dans la proc�dure g�n�rale de gestion d'erreur" + Chr$(13) + Error$(Err) + " Erreur N� : " + Str$(Err)
End Sub
