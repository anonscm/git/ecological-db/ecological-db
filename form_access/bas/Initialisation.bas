Attribute VB_Name = "Initialisation"
Option Explicit
Option Compare Database
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH              DERNIERE MISE A JOUR : 28/09/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'* Ce module permet l'initialisation de l''application a savoir :
'*      - La d�tection de la pr�sence ou non de la table de configuration qui doit contenir
'*        dans son nom : "formulaire_access_config". Si elle existe on r�cup�re les
'*        informations concernant le SGBD, le nom de la base de donn�es, le nom du r�le (groupe)
'*        ayant les droits en �criture, etc...
'*      - La cr�ation des requetes qui doivent �tre configur�es par le d�veloppeur en fonction
'*        du SGBD utilis�. Impl�mentation r�alis�e pour
'*              - Postgresql
'*      - L'affection de la valeur � la variable globale UserDroit qui d�termine si
'*        l'utilisateur a les droits en �criture ou pas et donc peut acc�der aux boutons
'*        ajouter, modifier, sauvegarder, annuler et supprimer
'*                                                                                            *
'*      REGLES A RESPECTER :
'*      - Les constantes d�finissant le nom des diff�rentes requ�tes doivent �tre renseign�es
'*                                                                                            *
'*      PROBLEMES EVENTUELS :
'*      - Si la requ�te "GetTailleMaxTextLong" n'existe pas et que lors de l'ajout ou d'une
'*        modification la valeur du champs depasse la longueur max autoris�e par la base de
'*        donn�es, un message d''erreur apparaitra mais indiquera une longueur max de 0
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :
'*                      Date    :
'*                      Objet   :
'**********************************************************************************************

'Droits de l''utilisateur connect�
Global DroitsUtilisateur As String

'Nom du SGBD utilis�, valeur r�cup�r�e � partir de la table de configuration
Global SysSgbd As String

'Version du SGBD utilis�, valeur r�cup�r�e � partir de la table de configuration
Global SgbdVersion As Variant

'Indique si l''application fonctionne en mode ODBC, valeur r�cup�r�e � partir de la table de configuration
Global Odbc As Boolean

'Nom de la base de donn�e utilis�e, valeur r�cup�r�e � partir de la table de configuration
Global Database As String

'Nom du r�le ayant les droits en �criture sur les tables, valeur r�cup�r�e � partir de la table de configuration
Global RoleEcriture As Variant

'Variable bool�enne globale qui indique si on se trouve en mode deconnecte, valeur r�cup�r�e � partir de la table de configuration
Public ModeDeconnecte As Boolean

'Nom de l'utilisateur connect�
Global NomUtilisateur As String

'Nom de la requ�te r�cup�rant l'utilisateur connect�
Global Const NOM_REQUETE_USER_CONNECTE As String = "GetUserConnected"

'Nom de la requ�te r�cup�rant les r�les de l'utilisateur connect�
Global Const NOM_REQUETE_ROLE_OF_USER As String = "GetRoleOfUser"

'Nom de la requ�te r�cup�rant la longueur max du champ
Global Const NOM_REQUETE_TAILLE_MAX_TEXT_LONG As String = "GetTailleMaxTextLong"

'Nom de la requ�te r�cup�rant la description du champ
Global Const NOM_REQUETE_DESCRIPTION_CHAMP As String = "GetFieldDescription"

'Message d'information affich� sur la page d'acceuil indiquant si la gestion des droits est prise en compte et/ou si le contr�le validit� sur les champs
'alphanum�rique est prise en compte
Global InformationMessage As String

'Variable donnant TAG de la base actuelle - cette variable globale permet un test au sein du code et �ventuellement un traitement sp�cifique selon le r�sultat de ce test.
Global ApplicationTag As String

'Variable donnant le num�ro de l'instance de la base actuelle. Elle est utilis�e lors du mode d�connect� et notamment lorsque plusieurs instances de la base d�connect�e
'sont aliment�es en parall�le (chaque base d�connect�e est initialis�e avec un num�ro d'instance distinct;
Global NumInstance As Integer

Sub InitialisationApplication()
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 14/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*                                                                                            *
'*      REGLES A RESPECTER :
'*          - Cf. d�claration de ce module afin que l'initialisation de l'application
'*            fonctionne
'*                                                                                            *
'*      BUT DE LA PROCEDURE :
'*          Cette proc�dure r�cup�re la configuration de l'application si elle existe, puis
'*          initialise l'application en cr�ant les requ�tes necessaires(Si le SGBD est autre
'*          que postgresql ou msaccess le d�veloppeur aura � charge de coder les requ�tes
'*          pour son SGBD ce code sera � ins�rer dans la fonction CreationRequetes en faisant
'*          un copier/coller du code concernant le SGBD "postgresql" et en l'adaptant.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_InitialisationApplication
Dim table As TableDef
Dim Source As Recordset
Dim Req As QueryDef
Dim Sql As String
Dim EstSuperUtilisateur As Boolean

'On initialise les variables
SysSgbd = ""
SgbdVersion = ""
Database = ""
RoleEcriture = ""
NomUtilisateur = ""

DroitsUtilisateur = DROIT_DEFAUT
EstSuperUtilisateur = False

For Each table In CurrentDb.TableDefs
    'On v�rifie la pr�sence ou non de la table de configuration
    If InStr(1, table.Name, "formulaire_access_config") Then
        'On r�cup�re la configuration
        Set Source = CurrentDb.OpenRecordset("select * from " & Replace(table.Name, ".", "_"))
        SysSgbd = Source.Fields("faconf_sgbd")
        If Not IsNull(Source.Fields("faconf_version")) Then
            SgbdVersion = Source.Fields("faconf_version")
        End If
        Odbc = Source.Fields("faconf_odbc")
        Database = Source.Fields("faconf_database")
        If Not IsNull(Source.Fields("faconf_role_ecriture")) Then
            RoleEcriture = Source.Fields("faconf_role_ecriture")
        End If
        ModeDeconnecte = Source.Fields("faconf_deconnecte")
        If Not IsNull(Source.Fields("faconf_tag")) Then
            ApplicationTag = Source.Fields("faconf_tag")
        End If
        If (Not Odbc) And ModeDeconnecte Then
            If Not IsNull(Source.Fields("faconf_num_instance")) Then
                NumInstance = Source.Fields("faconf_num_instance")
            End If
        End If

        'Lancement de la proc�dure de cr�ation des requ�tes ODBC direct.
        CreationRequetes SysSgbd, table.Connect

        'Si la requ�te permettant de r�cup�rer l''utilisateur connect� existe on l'execute
        If RequeteExiste(NOM_REQUETE_USER_CONNECTE) Then
            Set Source = CurrentDb.OpenRecordset(NOM_REQUETE_USER_CONNECTE)
            NomUtilisateur = CStr(Source.Fields("username"))
            EstSuperUtilisateur = CBool(Source.Fields("super_user"))
        End If
        'Initialisation de la variable DroitsUtilisateurs pour savoir si l'utilisateur b�n�ficie de l'interface limit�e � la consultation ou pas
        If EstSuperUtilisateur Then
            DroitsUtilisateur = DROIT_ECRITURE
        Else
            DroitsUtilisateur = GetUserDroits()
        End If
        Exit For
    End If
Next

'On cr�� le message d'information en fonction des requ�tes cr��es et de la pr�sence ou non de la table de configuration
CreerMessageInformation
Set Source = Nothing
Exit Sub
Erreur_InitialisationApplication:
    Call GestionErreur(Err, "InitialisationApplication", "")
End Sub

Sub CreationRequetes(SysSgbd As String, ChaineConnexion As String)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH              DERNIERE MISE A JOUR : 27/07/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - SysSgbd : Le nom du SGBD utilis�, provenant de la table de configuration.       *
'*          - ChaineConnexion : Chaine de connexion � la base de donn�e.                      *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Cf. d�claration de ce module afin que l'initialisation de l'application         *
'*            fonctionne                                                                      *
'*                                                                                            *
'*      BUT DE LA PROCEDURE :                                                                 *
'*          Cr�er les requ�tes ODBC direct n�cessaires � l'application.                       *
'*          Certaines requ�tes ne peuvent �tre ex�cut�es que par ce biais notamment lorsqu'elles
'*          font appel � du code SQL qu'Access ne supporte pas (utilisation des sch�mas et    *
'*          tables syst�mes du SGBD, SQL r�cursif ...).                                       *
'*          Initialement :                                                                    *
'*              - r�cup�ration utiliateur connect�                                            *
'*              - r�cup�ration de la taille maximale d'un champ varchar                       *
'*              -  r�cup�ration des r�les de l'utilisateur connect�                           *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  : S. GUIWARCH                                                 *
'*                      Date    : 27/07/2018                                                  *
'*                      Objet   : Ajout de la requ�te param�tr�e dont le nom est pr�cis� par  *
'*                                constante NomRequeteDescriptionChamp ("GetFieldDescription")*
'**********************************************************************************************
On Error GoTo Erreur_CreationRequetes
Dim Req As QueryDef
Dim Sql As String

Select Case LCase(SysSgbd)
    ' Pour postgresql
    Case "postgresql"
        For Each Req In CurrentDb.QueryDefs
            If Left(Req.Name, 1) <> "~" Then
                Req.Connect = ChaineConnexion
            End If
        Next Req

        'Requ�te permettant de r�cup�rer l'utilisateur connect�
        Sql = "SELECT u.usename as username, u.usesuper as super_user FROM pg_catalog.pg_user as u WHERE u.usename in (SELECT user);"
        If Not EnregistrementRequeteOdbcDirect(ChaineConnexion, NOM_REQUETE_USER_CONNECTE, Sql) Then
            MsgBox "Erreur de cr�ation de la requ�te " & NOM_REQUETE_USER_CONNECTE
        End If

        'Requ�te permettant de r�cup�rer la taille maximale d'un champ de type text (> 255)
        Sql = "SELECT character_maximum_length FROM information_schema.columns where table_name = [param1] and column_name = [param2];"
        If Not EnregistrementRequeteOdbcDirect(ChaineConnexion, NOM_REQUETE_TAILLE_MAX_TEXT_LONG, Sql) Then
            MsgBox "Erreur de cr�ation de la requ�te " & NOM_REQUETE_TAILLE_MAX_TEXT_LONG
        End If

        'Requ�te permettant de r�cup�rer les r�les de l'utilisateur connect�
        Sql = "WITH RECURSIVE extrait_oid_roles AS (SELECT oid FROM pg_roles WHERE rolname in (select user) UNION ALL SELECT m.roleid FROM extrait_oid_roles " & _
            "JOIN   pg_auth_members as m ON m.member = extrait_oid_roles.oid ) " & _
            "SELECT rolname FROM extrait_oid_roles inner join pg_roles on extrait_oid_roles.oid = pg_roles.oid where rolname not in (select user)"
        If Not EnregistrementRequeteOdbcDirect(ChaineConnexion, NOM_REQUETE_ROLE_OF_USER, Sql) Then
            MsgBox "Erreur de cr�ation de la requ�te " & NOM_REQUETE_ROLE_OF_USER
        End If

        'Requ�te permettant de r�cup�rer la description d'un champ de la base
         Sql = "SELECT description " & _
            "FROM pg_catalog.pg_description " & _
            "WHERE objoid IN (select pg_class.oid " & _
                "FROM pg_class " & _
                "INNER JOIN pg_namespace ON relnamespace = pg_namespace.oid " & _
                "WHERE pg_namespace.nspname = [param1] " & _
                "AND pg_class.relname = [param2]) " & _
            "AND classoid = 'pg_catalog.pg_class'::pg_catalog.regclass " & _
            "AND objsubid IN (SELECT ordinal_position " & _
                "FROM information_schema.columns " & _
                "WHERE table_schema != 'information' " & _
                "AND table_name = [param2] " & _
                "AND column_name = [param3])"
        If Not EnregistrementRequeteOdbcDirect(ChaineConnexion, NOM_REQUETE_DESCRIPTION_CHAMP, Sql) Then
            MsgBox "Erreur de cr�ation de la requ�te " & NOM_REQUETE_DESCRIPTION_CHAMP
        End If

    Case "access" 'On supprime les requ�tes qui ont �t� �ventuellement cr��es pour d'autres syst�mes (postgres notamment)
        
        'Si la requ�te permettant de r�cup�rer l'utilisateur connect� existe on la supprime.
        If RequeteExiste(NOM_REQUETE_USER_CONNECTE) Then
            CurrentDb.QueryDefs.Delete NOM_REQUETE_USER_CONNECTE
        End If
        
        'Si la requ�te permettant de r�cup�rer la taille maximale d'un champ de type text (> 255) existe on la supprime.
        If RequeteExiste(NOM_REQUETE_TAILLE_MAX_TEXT_LONG) Then
            CurrentDb.QueryDefs.Delete NOM_REQUETE_TAILLE_MAX_TEXT_LONG
        End If
        
        'Si la requ�te permettant de r�cup�rer les r�les de l'utilisateur connect� existe on la supprime.
        If RequeteExiste(NOM_REQUETE_ROLE_OF_USER) Then
            CurrentDb.QueryDefs.Delete NOM_REQUETE_ROLE_OF_USER
        End If
        
    Case Else
End Select

Set Req = Nothing
Exit Sub
Erreur_CreationRequetes:
    Call GestionErreur(Err, "CreationRequetes", "")
End Sub

Function RequeteExiste(RequeteName As String) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH              DERNIERE MISE A JOUR : 14/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*          - RequeteName : Le nom de la requ�te dont on veut v�rifier l'existance
'*                                                                                            *
'*      VALEUR DE RETOUR :
'*          - true si la requ�te existe sinon false
'*                                                                                            *
'*      REGLES A RESPECTER :
'*          - Cf. d�claration de ce module afin que l'initialisation de l''application
'*            fonctionne
'*                                                                                            *
'*      BUT DE LA FONCTION :
'*          Tester l''existence de la requ�te
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_RequeteExiste
Dim Requete  As QueryDef

RequeteExiste = False

'Tentative d'instancier la vbariable Requete avec la requ�te dont pn souhaite tester l'existence. Si l'instruction ne d�clenche poas d'erreur alors la requ�te existe
Set Requete = CurrentDb.QueryDefs(RequeteName)
RequeteExiste = True

Exit Function
Erreur_RequeteExiste:
    'en cas d''erreur diff�rente de celle indiquant que la requ�te n''existe pas on affiche l'erreur sinon on va � l'�tiquette fin.
    If Err.Number <> 3265 Then
        Call GestionErreur(Err, "RequeteExiste", "")
    End If
End Function

Sub CreerMessageInformation()
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 14/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*                                                                                            *
'*      REGLES A RESPECTER :
'*          - Cf. d�claration de ce module afin que l'initialisation de l''application
'*            fonctionne
'*                                                                                            *
'*      BUT DE LA PROCEDURE :
'*          Cr�er le message d''information appropri� en fonction de la pr�sence des requ�tes
'*          NomRequeteUserConnected, NomRequeteRoleOfUser et NomRequeteTailleMaxTextLong
'*
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_CreerMessageInformation

If RequeteExiste(NOM_REQUETE_USER_CONNECTE) And RequeteExiste(NOM_REQUETE_ROLE_OF_USER) Then
    InformationMessage = InformationMessage & vbCrLf & " - La gestion des droits (lecture seule ou tous les droits) est prise en compte et configur�e."
Else
    InformationMessage = InformationMessage & vbCrLf & " - La gestion des droits (lecture seule ou tous les droits) n'est pas prise en compte."
End If

If RequeteExiste(NOM_REQUETE_TAILLE_MAX_TEXT_LONG) Then
    InformationMessage = InformationMessage & vbCrLf & " - Les cont�les de validit� des champs alphanum�riques d'une longueur sup�rieure � 255 sont pris en compte."
Else
    InformationMessage = InformationMessage & vbCrLf & " - Les cont�les de validit� des champs alphanum�riques d'une longueur sup�rieure � 255 ne sont pas pris en compte."
End If

fin:
Exit Sub

Erreur_CreerMessageInformation:
    Call GestionErreur(Err, "CreerMessageInformation", "")
    Resume fin
End Sub

Function GetUserDroits() As String
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 14/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*                                                                                            *
'*      VALEUR DE RETOUR :
'*          - Constante indiquant si l'utilisateur connect� dispose de droit en ecriture ou
'*            en lecture seule (au choix DROIT_ECRITURE ou DROIT_LECTURE)
'*                                                                                            *
'*      REGLES A RESPECTER :
'*          - Cf. d�claration de ce module afin que l'initialisation de l'application
'*            fonctionne
'*          - Renseignement pr�alable de la variable globale ROLE_ECRITURE ainsi que des
'*            3 constantes DROIT_ECRITURE, DROIT_DEFAUT et DROIT_LECTURE.
'*                                                                                            *
'*      BUT DE LA FONCTION :
'*          - retourner une constante DROIT_ECRITURE ou DROIT_LECTURE selon que l'utilisateur
'*            dispose du droit d'�criture ou de lecture seule.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_GetUserDroits
Dim Source As Recordset

Select Case LCase(RoleEcriture)
    Case TOUS
        GetUserDroits = DROIT_ECRITURE
        Exit Function

    Case Else
        If Not (RequeteExiste(NOM_REQUETE_ROLE_OF_USER)) Then
                GetUserDroits = DROIT_DEFAUT
                Exit Function
        End If
        Set Source = CurrentDb.OpenRecordset(NOM_REQUETE_ROLE_OF_USER)
        While Not Source.EOF
            If LCase(Source.Fields("rolname")) = LCase(RoleEcriture) Then
                GetUserDroits = DROIT_ECRITURE
                Exit Function
            End If
            Source.MoveNext
        Wend
        GetUserDroits = DROIT_DEFAUT
End Select

Set Source = Nothing
Exit Function
Erreur_GetUserDroits:
    Call GestionErreur(Err, "GetUserDroits", "")
End Function

Function ExecuteRequeteParametre(NomRequete As String, Params As Variant, ParamsType As String) As Recordset
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. GUIWARCH               DERNIERE MISE A JOUR : 14/11/2016
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :
'*          - NomRequete : Nom de la requ�te ODBC direct ayant des param�tres
'*          - Params : Tableau contenant les valeurs des diff�rents param�tres
'*                     Array(param1Value, param2Value, etc....)
'*          - ParamsType: type de param�tres ("String" ou "Int" etc...)
'*                                                                                            *
'*      VALEUR DE RETOUR :
'*          - Un recordset contenant le r�sultat de la requ�te apr�s avoir modifi� les
'*            param�tres.
'*                                                                                            *
'*      REGLES A RESPECTER :
'*          - Cf. d�claration de ce module afin que l'initialisation de l'application
'*            fonctionne
'*                                                                                            *
'*      BUT DE LA FONCTION :
'*          Modifier les param�tres d'une requ�te (car en odbc les param�tres ne sont pas
'*          disponibles avec l'objet QueryDef), puis executer la requ�te puis la remettre � son
'*          �tat initial.
'*          Par exemple une requete odbc direct est cr�e avec la cha�ne suivente :
'*              select * from matable where colonneid = [param1]
'*          La fonction va remplacer [param1] par une valeur qu'elle re�oit dans le param�tre
'*          Params, les requ�tes odbc direct ne puvant �tre param�trable comme les autres
'*          requ�tes Access. Une fois le remplacement effectu�, la requ�te est ex�cut�e puis
'*          remise en �tat initial.
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_ExecuteRequeteParametre
Dim I As Integer
Dim Requete As QueryDef
Dim RequeteInitiale, RequeteModifiee As String

Set Requete = CurrentDb.QueryDefs(NomRequete)
RequeteInitiale = Requete.Sql
Select Case LCase(ParamsType)
    Case TYPE_PARAM_STRING
        For I = 0 To UBound(Params)
            If (I = 0) Then
                RequeteModifiee = Replace(RequeteInitiale, "[param" & CStr(I + 1) & "]", "'" & Params(I) & "'")
            Else
                RequeteModifiee = Replace(RequeteModifiee, "[param" & CStr(I + 1) & "]", "'" & Params(I) & "'")
            End If
        Next
    Case TYPE_PARAM_INT
        For I = 0 To UBound(Params)
            If (I = 0) Then
                RequeteModifiee = Replace(RequeteInitiale, "[param" & CStr(I + 1) & "]", Params(I))
            Else
                RequeteModifiee = Replace(RequeteModifiee, "[param" & CStr(I + 1) & "]", Params(I))
            End If
        Next
    Case Else
End Select

If RequeteModifiee = "" Then
    MsgBox "Le code SQL de la requ�te : " & NomRequete & " est vide."
End If
CurrentDb.QueryDefs(NomRequete).Sql = RequeteModifiee

Set ExecuteRequeteParametre = CurrentDb.OpenRecordset(NomRequete)
CurrentDb.QueryDefs(NomRequete).Sql = RequeteInitiale

Set Requete = Nothing
Exit Function
Erreur_ExecuteRequeteParametre:
    'On r�affecte son code sql initial � la requ�te.
    CurrentDb.QueryDefs(NomRequete).Sql = RequeteInitiale
    Call GestionErreur(Err, "ExecuteRequeteParametre", "")
End Function

Function EnregistrementRequeteOdbcDirect(ChaineConnexion As String, NomRequete As String, CodeSql As String) As Boolean
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    M. BENARD               DERNIERE MISE A JOUR : 28/09/2018
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - ChaineConnexion : Chaine de connexion � la base de donn�e.                      *
'*          - NomRequete : nom de la requ�te � cr�er                                          *
'*          - CodeSql : cha�ne sql de la requ�te � cr�er.                                     *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - True si le traitement abutit et false en cas d'erreur                           *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Respecter le format des param�tres.                                             *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Enregistrer une requ�te ODBC directe dans la base.                              *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_EnregistrementRequeteOdbcDirect
Dim Req As QueryDef
Dim Sql As String

'Suppression de la requ�te si elle existe
If RequeteExiste(NomRequete) Then
    CurrentDb.QueryDefs.Delete NomRequete
End If
 
'Astuce pour la cr�ation de la requ�te car si l'on affecte directement le bon code sql Access nous g�n�re une erreur de syntaxe.
Sql = "select user"
 
'Cr�ation de la requ�te avec la chaine sql factice
Set Req = CurrentDb.CreateQueryDef(NomRequete, Sql)
Req.Connect = ChaineConnexion
 
'Repositionnement du code sql de la requ�te enregistr�e avec le code sql pass� en param�tre � la fonction
Req.Sql = CodeSql
 
EnregistrementRequeteOdbcDirect = True

Exit Function
Erreur_EnregistrementRequeteOdbcDirect:
    Call GestionErreur(Err, "EnregistrementRequeteOdbcDirect", "")
    EnregistrementRequeteOdbcDirect = False
End Function
