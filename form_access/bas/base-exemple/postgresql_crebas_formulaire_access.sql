---------------------------------------------------------------------------------
-- Auteur :		Alain Benard
-- Date création 			:	5/10/2016
-- Dernière modificcation	:	10/11/2016
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour créer la structure (Tables, contraintes, index ...).
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer sur une base précedemment créée à l'aide du script suivant :
--				https://svngeodb.nancy.inra.fr/svn/scripts/trunk/serveurs%20virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des index et contraintes ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f postgresql_crebas_formulaire_access.sql -d db_formulaire_access -U sguiwarch
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 	
-- \prompt "Entrez le nom du role qui execute ce script?" executant
\set proprietaire 'formulaire_access'
\set grpecriture 'formulaire_access_ecriture'
\set grplecture 'formulaire_access_lecture'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

-- L'utilisation d'une transaction permet de créer toute la structure ou rien si une erreur survient, laissant la base de données dans un état stable
start transaction;

-- ************************************************************************
-- ***** Modification des droits par défaut lors de la création dobjet ****
-- ************************************************************************

	ALTER DEFAULT PRIVILEGES
		IN SCHEMA public
		GRANT ALL 
		ON TABLES
		TO :grpecriture;
	
	ALTER DEFAULT PRIVILEGES
		IN SCHEMA public
		GRANT SELECT
		ON TABLES
		TO :grplecture;
		
	ALTER DEFAULT PRIVILEGES
		IN SCHEMA public
		GRANT ALL
		ON SEQUENCES
		TO :proprietaire, :grpecriture;
		
-- *************************************************
-- ***** Création de la table de configuration *****
-- *********************************************-***

	--table : t_formulaire_access_configuration_faconf
	create table t_formulaire_access_configuration_faconf (
		faconf_id               serial			not null,
		faconf_sgbd             varchar(100)	not null,
		faconf_version			varchar(4)		null,
		faconf_odbc				boolean         not null,
		faconf_deconnecte		boolean         not null,
		faconf_num_instance		integer			null,
		faconf_database			varchar(100)	not null,
		faconf_role_ecriture	varchar(100)	null,
		faconf_tag				varchar(255)	null,
		constraint t_formulaire_access_configuration_faconf_pkey primary key (faconf_id)
	);

	comment on table t_formulaire_access_configuration_faconf is
	'Table contenant une partie de la configuration de l''application access.';
	comment on column t_formulaire_access_configuration_faconf.faconf_id is
	'Identifiant de la configuration.';
	comment on column t_formulaire_access_configuration_faconf.faconf_sgbd is
	'Nom du sgbd utilisé.';
	comment on column t_formulaire_access_configuration_faconf.faconf_version is
	'Version du sgbd utilisé.';
	comment on column t_formulaire_access_configuration_faconf.faconf_odbc is
	'Booléan indiquant si l''application est connectée à la base en mode connecté (tables liées).';
	comment on column t_formulaire_access_configuration_faconf.faconf_deconnecte is
	'Booléan indiquant si l''application est connectée à la base en mode déconnecté.';
	comment on column t_formulaire_access_configuration_faconf.faconf_num_instance is
	'Chiffre indiquant le numéro de l''instance, information utilisée en mode déconnecté pour la saisie à plusieurs.';
	comment on column t_formulaire_access_configuration_faconf.faconf_database is
	'Nom de la base de donnée utilsée.';
	comment on column t_formulaire_access_configuration_faconf.faconf_role_ecriture is
	'Nom du rôle ayant les droits en écriture.';
	comment on column t_formulaire_access_configuration_faconf.faconf_tag is
	'Tag permettant d''effectuer un traitement particulier selon la valeur de ce dernier.';
	
	alter table public.t_formulaire_access_configuration_faconf owner to :proprietaire;
	
	--insertion
	insert into t_formulaire_access_configuration_faconf(faconf_sgbd, faconf_version, faconf_odbc, faconf_deconnecte,faconf_num_instance, faconf_database, faconf_role_ecriture,faconf_tag) 
	values ('Postgresql', '9.5', true, false, null, 'db_formulaire_access', 'formulaire_access_ecriture', null);

-- ********************************************
-- ***** Création des tables de référence *****
-- ********************************************

	--Table : tr_organisme_org
		create table public.tr_organisme_org (
		org_id               serial               not null,
		org_code             varchar(12)          not null,
		org_nom              varchar(300)         not null,
		constraint c_pk_org primary key (org_id), 
		constraint c_uni_code_org unique(org_code)
	);

	comment on table public.tr_organisme_org is
	'Table contenant tous les organismes.';
	comment on column public.tr_organisme_org.org_id is
	'Identifiant de l''organisme.';
	comment on column public.tr_organisme_org.org_code is
	'Code de l''organisme.';
	comment on column public.tr_organisme_org.org_nom is
	'Nom de l''organisme.';
	comment on constraint c_uni_code_org on public.tr_organisme_org is
	'Unicité sur le code de l''organisme.';
	alter table public.tr_organisme_org owner to :proprietaire;
	
	--insertion
	insert into public.tr_organisme_org(org_code, org_nom) values ('INRA', 'Institut National de la Recherche Agronomique.');
	insert into public.tr_organisme_org(org_code, org_nom) values ('ONF', 'Office National des Forêt.');
	
-- ******************************************
-- ***** Création des tables de données *****
-- ******************************************

	-- Table : t_personne_per
	create table public.t_personne_per (
		per_id               serial               not null,
		per_org_id           int4                 not null,
		per_nom              varchar(32)          not null,
		per_prenom           varchar(32)          not null,
		per_mail             varchar(150)         not null,
		constraint c_pk_t_personne_per primary key (per_id),
		constraint c_uni_mail_per unique(per_mail), 
		constraint c_fk_org_per foreign key (per_org_id)
			references public.tr_organisme_org(org_id)
			on delete no action
			on update no action
			not deferrable
	);
	comment on table public.t_personne_per is
	'Table contenant les personnes.';
	comment on column public.t_personne_per.per_id is
	'Identifiant d''une personne.';
	comment on column public.t_personne_per.per_org_id is
	'Identifiant de l''organisme.';
	comment on column public.t_personne_per.per_nom is
	'Nom d''une personne.';
	comment on column public.t_personne_per.per_prenom is
	'Prénom d''une personne.';
	comment on column public.t_personne_per.per_mail is
	'Adresse mail d''une personne.';
	comment on constraint c_uni_mail_per on public.t_personne_per is
	'Unicité sur le mail de la personne.';
	comment on constraint c_fk_org_per on public.t_personne_per is
	'Rattachement d''un organisme à une personne.';
	alter table public.t_personne_per owner to :proprietaire;
	
	-- Création de l'index
	create index x_btr_org_id_per on public.t_personne_per using btree (per_org_id);
	comment on index x_btr_org_id_per is 
	'Index sur le ratachement à un organisme afin d''optimiser le temps des requètes';
	
	--insertion
	insert into public.t_personne_per(per_org_id, per_nom, per_prenom, per_mail) values (1, 'Dupond', 'Pierre', 'pierre.dupond@inra.fr');
	insert into public.t_personne_per(per_org_id, per_nom, per_prenom, per_mail) values (1, 'Benard', 'Alain', 'alain.benard@inra.fr');
	insert into public.t_personne_per(per_org_id, per_nom, per_prenom, per_mail) values (1, 'Guiwarch', 'Sébastien', 'sebastien.guiwarch@inra.fr');
	
	
	-- Table : T_PROJET_PRO
	create table public.t_projet_pro (
		pro_id               serial               not null,
		pro_nom              varchar(255)         not null,
		constraint c_pk_t_projet_pro primary key (pro_id),
		constraint c_uni_nom_pro unique (pro_nom)
	);
	comment on table public.t_projet_pro is
	'Table contenant les projets.';
	comment on column public.t_projet_pro.pro_id is
	'Identifiant du projet.';
	comment on column public.t_projet_pro.pro_nom is
	'Nom du projet.';
	comment on constraint c_uni_nom_pro on public.t_projet_pro is
	'Unicité sur le nom du projet.';
	alter table public.t_projet_pro owner to :proprietaire;
	
	--insertion
	insert into public.t_projet_pro(pro_nom) values ('Db_phyto');
	insert into public.t_projet_pro(pro_nom) values ('Db_poissons');
	
	-- Table : t_competence_comp
	create table public.t_competence_comp (
		comp_id               serial               not null,
		comp_libelle          varchar(255)         not null,
		constraint c_pk_t_competence_comp primary key (comp_id),
		constraint c_uni_libelle_comp unique (comp_libelle)
	);
	comment on table public.t_competence_comp is
	'Table contenant les compétences.';
	comment on column public.t_competence_comp.comp_id is
	'Identifiant de la compétence.';
	comment on column public.t_competence_comp.comp_libelle is
	'Libellé de" la compétence.';
	comment on constraint c_uni_libelle_comp on public.t_competence_comp is
	'Unicité sur le libellé de la compétence.';
	alter table public.t_competence_comp owner to :proprietaire;
	
	--insertion
	insert into public.t_competence_comp(comp_libelle) values ('Administrateur systèmes');
	insert into public.t_competence_comp(comp_libelle) values ('VBA');
	insert into public.t_competence_comp(comp_libelle) values ('Carrotage');
	insert into public.t_competence_comp(comp_libelle) values ('Elagage');
	
-- ******************************************
-- **** Création des tables de jointures ****
-- ******************************************

	create table public.tj_role_per_pro_rol (
		rol_per_id               int4                 not null,
		rol_pro_id      	     int4                 not null,
		rol_libelle		         varchar(255)         not null,
		constraint c_pk_tj_role_per_pro_rol primary key (rol_per_id, rol_pro_id),
		constraint c_fk_per_rol foreign key (rol_per_id)
			references public.t_personne_per(per_id)
			on delete no action
			on update no action
			not deferrable,
		constraint c_fk_pro_rol foreign key (rol_pro_id)
			references public.t_projet_pro(pro_id)
			on delete no action
			on update no action
			not deferrable
	);
	comment on table public.tj_role_per_pro_rol is
	'Table attribuant un rôle à 1 personne concernant 1 projet.';
	comment on column public.tj_role_per_pro_rol.rol_per_id is
	'Identifiant d''une personne.';
	comment on column public.tj_role_per_pro_rol.rol_pro_id is
	'Identifiant du projet.';
	comment on column public.tj_role_per_pro_rol.rol_libelle is
	'Libellé du rôle.';
	comment on constraint c_fk_per_rol on public.tj_role_per_pro_rol is
	'Association d''une personne à un rôle.';
	comment on constraint c_fk_pro_rol on public.tj_role_per_pro_rol is
	'Association d''un projet à un rôle.';
	alter table public.tj_role_per_pro_rol owner to :proprietaire;
	
	-- Création de l'index
	create index x_btr_per_id_rol on public.tj_role_per_pro_rol using btree (rol_per_id);
	comment on index x_btr_per_id_rol is 
	'Index sur le ratachement à une personne afin d''optimiser le temps des requètes';
	
	-- Création de l'index
	create index x_btr_pro_id_rol on public.tj_role_per_pro_rol using btree (rol_pro_id);
	comment on index x_btr_pro_id_rol is 
	'Index sur le ratachement à un projet afin d''optimiser le temps des requètes';
	
	--insertion
	insert into public.tj_role_per_pro_rol(rol_per_id, rol_pro_id, rol_libelle) values (1,1,'Technicien de terrain.');
	insert into public.tj_role_per_pro_rol(rol_per_id, rol_pro_id, rol_libelle) values (2,1,'Chef de projet.');
	insert into public.tj_role_per_pro_rol(rol_per_id, rol_pro_id, rol_libelle) values (3,1,'Développeur');

	create table public.tj_acompetence_per_comp_acomp (
		acomp_per_id               int4                 not null,
		acomp_comp_id      	     int4                 not null,
		constraint c_pk_tj_acompetence_per_comp_acomp primary key (acomp_per_id, acomp_comp_id),
		constraint c_fk_per_acomp foreign key (acomp_per_id)
			references public.t_personne_per(per_id)
			on delete no action
			on update no action
			not deferrable,
		constraint c_fk_comp_acomp foreign key (acomp_comp_id)
			references public.t_competence_comp(comp_id)
			on delete no action
			on update no action
			not deferrable
	);
	comment on table public.tj_acompetence_per_comp_acomp is
	'Table attribuant une compétence à 1 personne.';
	comment on column public.tj_acompetence_per_comp_acomp.acomp_per_id is
	'Identifiant d''une personne.';
	comment on column public.tj_acompetence_per_comp_acomp.acomp_comp_id is
	'Identifiant d''une compétence.';
	comment on constraint c_fk_per_acomp on public.tj_acompetence_per_comp_acomp is
	'Association d''une personne à une compétence.';
	comment on constraint c_fk_comp_acomp on public.tj_acompetence_per_comp_acomp is
	'Association d''une compétence.';
	alter table public.tj_acompetence_per_comp_acomp owner to :proprietaire;
	
	-- Création de l'index
	create index x_btr_per_id_acomp on public.tj_acompetence_per_comp_acomp using btree (acomp_per_id);
	comment on index x_btr_per_id_acomp is 
	'Index sur le ratachement à une personne afin d''optimiser le temps des requètes';
	
	-- Création de l'index
	create index x_btr_comp_id_acomp on public.tj_acompetence_per_comp_acomp using btree (acomp_comp_id);
	comment on index x_btr_comp_id_acomp is 
	'Index sur le ratachement à une compétence afin d''optimiser le temps des requètes';
	
	--insertion
	insert into public.tj_acompetence_per_comp_acomp(acomp_per_id, acomp_comp_id) values (1,3);
	insert into public.tj_acompetence_per_comp_acomp(acomp_per_id, acomp_comp_id) values (1,4);
	insert into public.tj_acompetence_per_comp_acomp(acomp_per_id, acomp_comp_id) values (2,1);
	insert into public.tj_acompetence_per_comp_acomp(acomp_per_id, acomp_comp_id) values (2,2);
	insert into public.tj_acompetence_per_comp_acomp(acomp_per_id, acomp_comp_id) values (3,2);

commit -- Enregistre la totalité du traitement si aucune erreur n''est survenue