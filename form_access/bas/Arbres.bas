Attribute VB_Name = "Arbres"
Option Explicit
Option Compare Database
Public Const SYNCHRO_SUR_TREEVIEW = 1101
Public Const SYNCHRO_PAR_NODE_INDEX = 1102
Public Const TREEVIEW_VERROUILLE = 9000
Public Const TREEVIEW_DEVERROUILLE = 9001

Public Const NOEUDS_PARENTS_COCHES = 10000
Public Const NOEUDS_ENFANTS_COCHES = 10001
Public Const TOUS_NOEUDS_COCHES = 10002

Public NoeudsModifCoch�s As Variant

Public Sub load_Treeview( _
    Fenetre As Form, _
    source As Recordset, _
    tv As MSComctlLib.TreeView, _
    colonneID As String, _
    colonneNom As String, _
    colonneIDParent As String, _
    colonneIdEnfant As String, _
    Critere As String _
)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET              DERNIERE MISE A JOUR : 07/12/2016                      *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - rsReqs : Objet de type Recordset auquel le Treeview est li�                     *
'*                                                                                            *
'*          - tv : Objet de type Treeview qui va �tre rempli                                  *
'*                                                                                            *
'*          - colonneID : Objet de type string correspondant au nom de la colonne contenant   *
'*                        l'id du sujet de la table  (colonne du rsReqs)                      *
'*                                                                                            *
'*          - colonneNom : Objet de type string correspondant au nom de la colonne contenant  *
'*                        le nom du sujet de la table (colonne du rsReqs)                     *
'*                                                                                            *
'*          - colonneIDParent : Objet de type string correspondant au nom de la colonne       *
'*                              contenant l'id parent du sujet de la table (colonne du rsReqs)*
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Pour que la fonction se r�alise de la bonne mani�re il faut que le Recordset    *
'*            pass� � la fonction ait une colonne "niveau" ou renommer ce param�tre dans      *
'*            strFind selon le nom votre colonne. Le plus haut niveau doit aussi �tre         *
'*            inititialis� � 0 ou une nouvelle fois, doit �tre modifi� dans le strFind.      *
'*                                                                                            *
'*      BUT DE LA PROCEDURE : Remplir l'arbre tv pass� en param�tre de la fonction selon les  *
'*                            donn�e pr�sente dans le Recordset rsReqs, pour cela on fait     *
'*                            appel � la proc�dure addChildren                                *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_load_Treeview
Dim strFind As String
Dim rsReqs As DAO.Recordset
Dim nodX As MSComctlLib.Node
Dim strBook As String

If source.RecordCount = 0 Then
    tv.Nodes.Clear
    Exit Sub
End If

Set rsReqs = source
rsReqs.MoveFirst
tv.Nodes.Clear
'strFind = "niveau = 0"
strFind = Critere
rsReqs.FindFirst strFind

Do While Not rsReqs.NoMatch
    Set nodX = tv.Nodes.Add(, , "<ID>" & rsReqs.Fields(colonneID) & "</ID>", rsReqs.Fields(colonneNom))
    'Set nodX = Tv.Nodes.Add(, , "<ID>" & rsReqs.Fields(colonneIDParent) & "$$" & rsReqs.Fields(colonneIdEnfant) & "</ID>", rsReqs.Fields(colonneNom))
    strBook = rsReqs.Bookmark
    addChildren Fenetre, nodX, tv, rsReqs, rsReqs.Fields(colonneID), colonneID, colonneNom, colonneIDParent, colonneIdEnfant
    rsReqs.Bookmark = strBook
    Fenetre.PersonnaliseNode rsReqs, nodX
    rsReqs.FindNext strFind
Loop

Set rsReqs = Nothing
Exit Sub

Erreur_load_Treeview:
    Set rsReqs = Nothing
    Call GestionErreur(Err, "load_Treeview", "")
End Sub

Private Sub addChildren( _
    Fenetre As Form, _
    nodParent As Node, _
    tv As MSComctlLib.TreeView, _
    rsReqs As DAO.Recordset, _
    parentID As String, _
    colonneID As String, _
    colonneNom As String, _
    colonneIDParent As String, _
    colonneIdEnfant As String _
)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET              DERNIERE MISE A JOUR : 07/12/2016                      *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - tv : Objet de type Treeview qui va �tre rempli                                  *
'*                                                                                            *
'*          - nodParent : Objet de type Node �tant le parent du noeud qui va �tre ajouter     *
'*                                                                                            *
'*          - rsReqs : Objet de type Recordset auquel le Treeview est li�                     *
'*                                                                                            *
'*          - lngParentID : Objet de type long correspondant � l'id du parent du noeud qui va *
'*                          �tre ajouter                                                      *
'*                                                                                            *
'*          - colonneID : Objet de type string correspondant au nom de la colonne contenant   *
'*                        l'id du sujet de la table                                           *
'*                                                                                            *
'*          - colonneNom : Objet de type string correspondant au nom de la colonne contenant  *
'*                        le nom du sujet de la table                                         *
'*                                                                                            *
'*          - colonneIDParent : Objet de type string correspondant au nom de la colonne       *
'*                              contenant l'id parent du sujet de la table                    *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Pour que la fonction se r�alise de la bonne mani�re il faut que le Recordset    *
'*            pass� � la fonction est une colonne "niveau" ou renommer ce param�tre dans      *
'*            strFind selon le nom votre colonne. Le plus haut niveau doit aussi �tre         *
'*            inititialis� � 0 ou une nouvelle fois, doit �tre modifier dans le strFind.      *
'*                                                                                            *
'*      BUT DE LA PROCEDURE : Remplir l'arbre tv pass� en param�tre de la fonction selon les  *
'*                            donn�e pr�sente dans le Recordset rsReqs, cette procedure va    *
'*                            ajouter les enfants au noeud passer en param�tre. Elle s'appel  *
'*                            elle-m�me de fa�on r�cursive afin d'ajouter tous les enfants de *
'*                            tous les noeuds de l'arbre. Quand le noeud � un enfant il est   *
'*                            ajouter au Treeview puis la proc�dure cherche les enfants de ce *
'*                            nouveau noeud et les ajoutes et ainsi de suite                  *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_addChildren
Dim strFind As String
Dim nodX As Node
Dim strBook As String

If rsReqs.Fields(colonneIDParent).Type = dbLong Then
    strFind = colonneIDParent & " = " & parentID
End If

If rsReqs.Fields(colonneIDParent).Type = dbMemo Then
    strFind = colonneIDParent & " = '" & parentID & "'"
End If

rsReqs.FindFirst strFind

Do While Not rsReqs.NoMatch
    Set nodX = tv.Nodes.Add(nodParent, tvwChild, "<ID>" & rsReqs.Fields(colonneID) & "</ID>", rsReqs.Fields(colonneNom))
    'Set nodX = Tv.Nodes.Add(nodParent, tvwChild, "<ID>" & rsReqs.Fields(colonneIDParent) & "$$" & rsReqs.Fields(colonneIdEnfant) & "</ID>", rsReqs.Fields(colonneNom))
    strBook = rsReqs.Bookmark
    addChildren Fenetre, nodX, tv, rsReqs, rsReqs.Fields(colonneID), colonneID, colonneNom, colonneIDParent, colonneIdEnfant
    rsReqs.Bookmark = strBook
    Fenetre.PersonnaliseNode rsReqs, nodX
    rsReqs.FindNext strFind
Loop

Exit Sub

Erreur_addChildren:
    Call GestionErreur(Err, "addChildren", "")
End Sub

Public Function getXML(strxml As String, strElement As String) As String
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET               DERNIERE MISE A JOUR : 07/12/2016                     *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - strxml : Objet de type String qui doit �tre traiter par la fonction             *
'*                                                                                            *
'*          - strElement : Objet de type String appartenant � strxml et que l'on veut traiter *
'*                         c'est cet objet qui sera supprimer par la fonction                 *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - String sans les strElement afin de ne r�cup�rer que l'id et non les balises ID  *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Ne pas toucher les procedures load_Treeview et addChildren qui initialise la    *
'*            la clef d'un noeud en string de type <ID>id_de_l_element</ID>                   *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Sortir la clef du noeud en tant qu'id en enlevant les balises <ID></ID> de la   *
'*            clef                                                                            *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_getXML
Dim intLeft As Integer
Dim intRight As Integer

intLeft = InStr(1, strxml, "<" & strElement & ">", vbTextCompare) + Len(strElement) + 2
intRight = InStr(1, strxml, "</" & strElement & ">", vbTextCompare)
getXML = Mid(strxml, intLeft, intRight - intLeft)
    
Exit Function
    
Erreur_getXML:
    Call GestionErreur(Err, "getXML", "")
End Function

Public Function getID(nodX As MSComctlLib.Node) As Variant
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET               DERNIERE MISE A JOUR : 07/12/2016                     *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - nodX : Objet de type Node duquel on veut r�cup�rer l'ID � partir de sa clef     *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - Long repr�sentant l'ID du noeud pass� en param�tre                              *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Ne pas toucher les procedures load_Treeview et addChildren qui initialise la    *
'*            la clef d'un noeud en string de type <ID>id_de_l_element</ID>                   *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Appel de la fonction getXML avec en param�tre la clef du noeud (nodX.Key) et    *
'*            "ID". Cela va permettre de r�cup�rer l'ID du noeud et de l'affecter � un Long   *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_get

getID = getXML(nodX.Key, "ID")
    
Exit Function
    
Erreur_get:
    Call GestionErreur(Err, "get - module arbres", "")
End Function

Public Sub SyncFormAndTreeview(Fenetre As Form, NomTreeview As String, NomControlsId As String, Mode As String, Optional nodeIndex As Variant)
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET               DERNIERE MISE A JOUR : 07/12/2016                     *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - strxml : Objet de type String qui doit �tre traiter par la fonction             *
'*                                                                                            *
'*          - strElement : Objet de type String appartenant � strxml et que l'on veut traiter *
'*                         c'est cet objet qui sera supprimer par la fonction                 *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - String sans les strElement afin de ne r�cup�rer que l'id et non les balises ID  *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Ne pas toucher les procedures load_Treeview et addChildren qui initialise la    *
'*            la clef d'un noeud en string de type <ID>id_de_l_element</ID>                   *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Sortir la clef du noeud en tant qu'id en enlevant les balises <ID></ID> de la   *
'*            clef                                                                            *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_SyncFormAndTreeview
Dim tv As MSComctlLib.TreeView
Dim NdIndex As Long

Set tv = Fenetre.Controls(NomTreeview).Object

Select Case Mode
    Case AUCUNE_SYNCHRO
    Case SYNCHROSURFORMULAIRE
        NdIndex = GetNodeIndexById(tv, Fenetre.Controls(NomControlsId).Value)
        With tv
            .DropHighlight = .Nodes(NdIndex)
            .Nodes(NdIndex).Expanded = True
            .SelectedItem = .Nodes(NdIndex)
            If .Checkboxes Then
                .Nodes(NdIndex).Checked = True
            End If
            OuvrirNoeudsParents .Nodes(NdIndex)
        End With
    Case SYNCHRO_SUR_TREEVIEW
        NdIndex = GetNodeIndexById(tv, getID(tv.SelectedItem))
        With tv
            .DropHighlight = .Nodes(NdIndex)
            .Nodes(NdIndex).Expanded = True
            .SelectedItem = .Nodes(NdIndex)
            If .Checkboxes Then
                .Nodes(NdIndex).Checked = True
            End If
        End With
        Fenetre.Recordset.FindFirst ("[" & NomControlsId & "]=" & getID(tv.Nodes(NdIndex)))
    Case SYNCHRO_PAR_NODE_INDEX
        If IsNull(nodeIndex) Then
            Exit Sub
        End If
        With tv
            .DropHighlight = .Nodes(nodeIndex)
            .Nodes(nodeIndex).Expanded = True
            .SelectedItem = .Nodes(nodeIndex)
            If .Checkboxes Then
                .Nodes(NdIndex).Checked = True
            End If
        End With
        Fenetre.Recordset.FindFirst ("[" & NomControlsId & "]=" & getID(tv.Nodes(nodeIndex)))
End Select
Fenetre.SyncFormAndTreeview Mode, nodeIndex

Set tv = Nothing
Exit Sub
Erreur_SyncFormAndTreeview:
    Call GestionErreur(Err, "SyncFormAndTreeview - " & Fenetre.NomFenetre, "")
End Sub

Public Function GetNodeIndexById(tv As MSComctlLib.TreeView, id As String) As Integer
'**********************************************************************************************
'*                                                                                            *
'*  AUTEUR :    A. DUCRET               DERNIERE MISE A JOUR : 07/12/2016                     *
'*                                                                                            *
'**********************************************************************************************
'*                                                                                            *
'*      LES PARAMETRES :                                                                      *
'*          - strxml : Objet de type String qui doit �tre traiter par la fonction             *
'*                                                                                            *
'*          - strElement : Objet de type String appartenant � strxml et que l'on veut traiter *
'*                         c'est cet objet qui sera supprimer par la fonction                 *
'*                                                                                            *
'*      VALEUR DE RETOUR :                                                                    *
'*          - String sans les strElement afin de ne r�cup�rer que l'id et non les balises ID  *
'*                                                                                            *
'*      REGLES A RESPECTER :                                                                  *
'*          - Ne pas toucher les procedures load_Treeview et addChildren qui initialise la    *
'*            la clef d'un noeud en string de type <ID>id_de_l_element</ID>                   *
'*                                                                                            *
'*      BUT DE LA FONCTION :                                                                  *
'*          - Sortir la clef du noeud en tant qu'id en enlevant les balises <ID></ID> de la   *
'*            clef                                                                            *
'*                                                                                            *
'*      HISTORIQUE DES MODIFICATIONS :                                                        *
'*          MODIF1 :    Auteur  :                                                             *
'*                      Date    :                                                             *
'*                      Objet   :                                                             *
'**********************************************************************************************
On Error GoTo Erreur_GetNodeIndexById
Dim i As Integer

For i = 1 To tv.Nodes.Count
    If getID(tv.Nodes(i)) = id Then
        GetNodeIndexById = i
        Exit For
    End If
Next i

Exit Function
Erreur_GetNodeIndexById:
    Call GestionErreur(Err, "GetNodeIndexById", "")
End Function

'****************************************************************************************************************
'* Fonctions utilis�es pour les treeview contenant des cases a cocher                                           *
'****************************************************************************************************************

Public Sub CocherNoeudsDansTreeview(tv As MSComctlLib.TreeView, RecordsetNoeuds As Recordset, AvecParents As Boolean, AvecEnfants As Boolean, AvecNoeudsIdentiques As Boolean)
On Error GoTo Erreur_CocherNoeudsDansTreeview
Dim IdNoeud As Variant

If RecordsetNoeuds.RecordCount = 0 Then
    Exit Sub
End If
RecordsetNoeuds.MoveFirst

Do While Not RecordsetNoeuds.EOF
    IdNoeud = GetNodeIndexById(tv, RecordsetNoeuds.Fields(0))
    If tv.Nodes(IdNoeud).Checked = False Then
        tv.Nodes(IdNoeud).Checked = True
        OuvrirNoeudsParents tv.Nodes(IdNoeud)
    End If
    
    If AvecParents Then
        CocheNoeudsParents tv.Nodes(IdNoeud)
    End If
    
    If AvecEnfants Then
        CocheNoeudsEnfants tv.Nodes(IdNoeud)
    End If
    
    If AvecNoeudsIdentiques Then
        ToggleNoeudsIdentiques tv, tv.Nodes(IdNoeud)
    End If
    RecordsetNoeuds.MoveNext
Loop

Exit Sub
Erreur_CocherNoeudsDansTreeview:
    Call GestionErreur(Err, "CocherNoeudsDansTreeview", "")
End Sub

Public Sub CocheNoeudsParents(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_CocheNoeudsParents
Dim Nd As MSComctlLib.Node

Set Nd = Noeud.Parent
If Not Nd Is Nothing Then
    If Nd.Checked = False Then
        Nd.Checked = True
        Nd.Expanded = True
    End If
    Call CocheNoeudsParents(Nd)
Else
    Noeud.Expanded = True
End If

Set Nd = Nothing
Exit Sub
Erreur_CocheNoeudsParents:
    Call GestionErreur(Err, "CocheNoeudsParents", "")
End Sub

Public Sub CocheNoeudsEnfants(Noeud As MSComctlLib.Node, Optional tv As MSComctlLib.TreeView)
On Error GoTo Erreur_CocheNoeudsEnfants
Dim Nd As MSComctlLib.Node
Dim i As Integer

Noeud.Expanded = True
OuvrirNoeudsParents Noeud
Set Nd = Noeud.Child

If Not Nd Is Nothing Then
    For i = 1 To Noeud.Children
        If Nd.Checked = False Then
            Nd.Checked = True
            If Not tv Is Nothing Then
                ToggleNoeudsIdentiques tv, Nd
            End If
        End If
        Call CocheNoeudsEnfants(Nd, tv)
        Set Nd = Nd.Next
    Next i
End If

Set Nd = Nothing
Exit Sub
Erreur_CocheNoeudsEnfants:
    Call GestionErreur(Err, "CocheNoeudsEnfants", "")
End Sub

Public Sub ToggleNoeudsIdentiques(tv As MSComctlLib.TreeView, Noeud As MSComctlLib.Node)
On Error GoTo Erreur_CocheNoeudsIdentiques
Dim i, IndexNoeudCliqu�, IndexNoeudParcouru As Integer
Dim IndexNoeudsCliqu�, IndexNoeudsParcourus As Variant

For i = 1 To tv.Nodes.Count
    IndexNoeudsParcourus = Split(getID(tv.Nodes(i)), "$$")
    IndexNoeudParcouru = IndexNoeudsParcourus(UBound(IndexNoeudsParcourus))
    
    IndexNoeudsCliqu� = Split(getID(Noeud), "$$")
    IndexNoeudCliqu� = IndexNoeudsCliqu�(UBound(IndexNoeudsCliqu�))
    If IndexNoeudCliqu� = IndexNoeudParcouru Then
        tv.Nodes(i).Checked = Noeud.Checked
        If Noeud.Checked = True Then
            OuvrirNoeudsParents tv.Nodes(i)
        End If
    End If
Next i

Exit Sub
Erreur_CocheNoeudsIdentiques:
    Call GestionErreur(Err, "CocheNoeudsIdentiques", "")
End Sub

Public Sub OuvrirNoeudsParents(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_OuvrirNoeudsParents
Dim Nd As MSComctlLib.Node

Noeud.Expanded = True
Set Nd = Noeud.Parent
If Not Nd Is Nothing Then
    If Nd.Checked = False Then
        Nd.Expanded = True
    End If
    Call OuvrirNoeudsParents(Nd)
Else
    Noeud.Expanded = True
End If

Set Nd = Nothing
Exit Sub
Erreur_OuvrirNoeudsParents:
    Call GestionErreur(Err, "OuvrirNoeudsParents", "")
End Sub

Public Sub OuvrirTousNoeuds(tv As MSComctlLib.TreeView)
On Error GoTo Erreur_OuvrirTousNoeuds
Dim Nd As MSComctlLib.Node

For Each Nd In tv.Nodes
    Nd.Expanded = True
Next Nd

Set Nd = Nothing
Exit Sub
Erreur_OuvrirTousNoeuds:
    Set Nd = Nothing
    Call GestionErreur(Err, "OuvrirTousNoeuds", "")
End Sub

Public Sub DecocheTousNoeuds(tv As MSComctlLib.TreeView)
On Error GoTo Erreur_DecocheTousNoeuds
Dim Nd As MSComctlLib.Node

For Each Nd In tv.Nodes
    Nd.Checked = False
    Nd.Expanded = False
Next Nd

Set Nd = Nothing
Exit Sub
Erreur_DecocheTousNoeuds:
    Call GestionErreur(Err, "DecocheTousNoeuds", "")
End Sub

Public Sub DecocheNoeudsParents(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_DecocheNoeudsParents
Dim Nd As MSComctlLib.Node

Set Nd = Noeud.Parent
If Not Nd Is Nothing Then
    If Nd.Checked = True Then
        Nd.Checked = False
    End If
    Call DecocheNoeudsParents(Nd)
End If

Set Nd = Nothing
Exit Sub
Erreur_DecocheNoeudsParents:
    Call GestionErreur(Err, "DecocheNoeudsParents", "")
End Sub

Public Sub DecocheNoeudsEnfants(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_DecocheNoeudsEnfants
Dim Nd As MSComctlLib.Node
Dim i As Integer

Noeud.Expanded = True
Set Nd = Noeud.Child

If Not Nd Is Nothing Then
    For i = 1 To Noeud.Children
        If Nd.Checked = True Then
            Nd.Checked = False
        End If
        Call DecocheNoeudsEnfants(Nd)
        Set Nd = Nd.Next
    Next i
End If

Set Nd = Nothing
Exit Sub
Erreur_DecocheNoeudsEnfants:
    Call GestionErreur(Err, "DecocheNoeudsEnfants", "")
End Sub

Public Function RecupererNoeudModifCoch�s(tv As MSComctlLib.TreeView, Mode As Integer) As Variant
On Error GoTo Erreur_RecupererNoeudModifCoch�s

NoeudsModifCoch�s = Array()

ConstruireListeNoeudsModifCoch�s tv, Mode
RecupererNoeudModifCoch�s = NoeudsModifCoch�s

NoeudsModifCoch�s = Array()

Exit Function
Erreur_RecupererNoeudModifCoch�s:
    Call GestionErreur(Err, "RecupererNoeudModifCoch�s", "")
End Function

Public Sub ConstruireListeNoeudsModifCoch�s(tv As MSComctlLib.TreeView, Mode As Integer)
On Error GoTo Erreur_ConstruireListeNoeudsModifCoch�s
Dim Nd As MSComctlLib.Node
Dim rang As Integer

For Each Nd In tv.Nodes
    If Nd.Checked Then
        Select Case Mode
            Case NOEUDS_ENFANTS_COCHES
                AjouterDerniersNoeudsEnfantsCoch�s Nd
            Case NOEUDS_PARENTS_COCHES
                AjouterPremiersNoeudsCoch�s Nd
            Case TOUS_NOEUDS_COCHES
                AjouterTousNoeudsCoch�s Nd
        End Select
    End If
Next Nd

Exit Sub
Erreur_ConstruireListeNoeudsModifCoch�s:
    Call GestionErreur(Err, "ConstruireListeNoeudsModifCoch�s", "")
End Sub

Public Sub AjouterDerniersNoeudsEnfantsCoch�s(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_AjouterDerniersNoeudsEnfantsCoch�s
Dim Nd As MSComctlLib.Node
Dim i, rang, RangPrecedent As Integer

Set Nd = Noeud.Child
RangPrecedent = UBound(NoeudsModifCoch�s)

If Not Nd Is Nothing Then
    For i = 1 To Noeud.Children
        If Nd.Checked Then
            AjouterDerniersNoeudsEnfantsCoch�s Nd
        End If
        
        Set Nd = Nd.Next
    Next i
Else
    If Not IsInArray(NoeudsModifCoch�s, getID(Noeud)) Then
        rang = UBound(NoeudsModifCoch�s) + 1
        ReDim Preserve NoeudsModifCoch�s(rang)
        NoeudsModifCoch�s(rang) = getID(Noeud)
    End If
End If

If RangPrecedent = UBound(NoeudsModifCoch�s) Then
    If Not IsInArray(NoeudsModifCoch�s, getID(Noeud)) Then
        rang = UBound(NoeudsModifCoch�s) + 1
        ReDim Preserve NoeudsModifCoch�s(rang)
        NoeudsModifCoch�s(rang) = getID(Noeud)
    End If
End If

Exit Sub
Erreur_AjouterDerniersNoeudsEnfantsCoch�s:
    Call GestionErreur(Err, "AjouterDerniersNoeudsEnfantsCoch�s", "")
End Sub

Public Sub AjouterPremiersNoeudsCoch�s(Noeud As MSComctlLib.Node)
On Error GoTo Erreur_AjouterPremiersNoeudsCoch�s
Dim rang As Integer

If Noeud.Checked = True Then
    If Not Noeud.Parent Is Nothing Then
        If Noeud.Parent.Checked = True Then
            AjouterPremiersNoeudsCoch�s Noeud.Parent
            Exit Sub
        Else
            If Not IsInArray(NoeudsModifCoch�s, getID(Noeud)) Then
                rang = UBound(NoeudsModifCoch�s) + 1
                ReDim Preserve NoeudsModifCoch�s(rang)
                NoeudsModifCoch�s(rang) = getID(Noeud)
            End If
            Exit Sub
        End If
    Else
        If Not IsInArray(NoeudsModifCoch�s, getID(Noeud)) Then
            rang = UBound(NoeudsModifCoch�s) + 1
            ReDim Preserve NoeudsModifCoch�s(rang)
            NoeudsModifCoch�s(rang) = getID(Noeud)
        End If
    End If
End If

Exit Sub
Erreur_AjouterPremiersNoeudsCoch�s:
    Call GestionErreur(Err, "AjouterPremiersNoeudsCoch�s", "")
End Sub

Public Sub AjouterTousNoeudsCoch�s(Node As MSComctlLib.Node)
On Error GoTo Erreur_AjouterTousNoeudsCoch�s
Dim rang As Integer

If Node.Checked = True Then
    rang = UBound(NoeudsModifCoch�s) + 1
    ReDim Preserve NoeudsModifCoch�s(rang)
    NoeudsModifCoch�s(rang) = getID(Node)
End If

Exit Sub
Erreur_AjouterTousNoeudsCoch�s:
    Call GestionErreur(Err, "AjouterTousNoeudsCoch�s", "")
End Sub
