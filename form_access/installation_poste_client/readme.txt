 - Site de t�l�chargement des pilotes ODBC : � partie du site https://odbc.postgresql.org/ on acc�de facilement au site officiel de t�l�chargement des pilotes ODBC pour Postgresql :
	https://www.postgresql.org/ftp/odbc/versions/msi/
Il conviendra de choisir le bon pilote ODBC (g�n�ralement le plus r�cent ou celui adapt� � la version du serveur Postgresql et dans tous les cas en version 32 ou 64 bits conforme � votre syst�me d'exploitation).

 - Runtime Access 2013 t�l�charg� � partir du site officel : 
 https://www.microsoft.com/fr-fr/download/details.aspx?id=39358 (dans tous les cas en version 32 ou 64 bits conforme � votre syst�me d'exploitation).
 Version anglaise du runtime : https://www.microsoft.com/en-nz/download/details.aspx?id=39358